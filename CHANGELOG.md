# v2023.07 (2023-07-18)

## Changes

* examples: Add an example to use NetX Duo's Azure IoT addon to connect to Azure IoT Hub, download an update from the Azure Device
  Update service and install the update using the PSA Firmware Update API provided by TrustedFirmware-M (TF-M).
* cmake: Fix conditional source inclusion for CMake targets in `utils` directory.
* templates: Set linker flags explicitly.
* pigweed: Do not use mbed_sdk_init when testing.
* cmake: Use updated mcu-driver-hal & ref implementation.
* components: Add CMSIS-Pack-Utils CMake module PoC
  utils: Use CMSIS-Driver instead of the MDH API to retarget serial input/output
  pigweed: Use CMSIS-Driver in pigweed component for platform drivers
  lwip: Use CMSIS Ethernet Driver instead of MDH Emac Driver
  cmsis: Replace using MCU-Driver-Hal with CMSIS Driver Packs through the Open IoT SDK
* doc: Fix workflow.md's link to coding_style.md.
* ci: Move template-specific build steps to hooks called at specific times in the CI build process
* examples: Fix Bluetooth instructions
* arm-2d: Add documentation for component
* nimble: Add pigweed logging. We set the log level at info by default
* docs: Replace the overview page of RTOS configuration with more detailed pages for each of RTX5, ThreadX and FreeRTOS.
* lwip: Pass IP address to network_state_callback when network comes up
* examples: Remove unused nimble from pigweed-sys-io.
* NimBLE: Make NimBLE configurable at build time. This require the user to add a config.h file and link it against the NPL.
* ci: Build docs website for preview in merge requests.
* cmake: Fix misleading message "ERRORUnsupported compiler" when armclang is used.
* mkdocs: Introduce a plugin to convert file links to GitLab links.
* mkdocs: Extend our plugin to support links in inlined pages.
* pigweed: Fix broken links in pw_iot_sdk_config docs.
* mkdocs: Use the default palette of Material for MkDocs.
* docs: Refactor pages to use admonitions.
* mkdocs: Enable support for admonitions.
* components: Add coreMQTT Agent and use in the AWS Client example
* mbedtls: Update to MbedTLS v2.28.3 LTS
* docs: Consolidate repository README.md and docs/index.md.
* nimble: Add the BLE example to vscode task list so that one can build and run it like the other examples. Running it still require to set up babblesim and the other tools, as per the readme.
* components: Remove unused TinyUSB.
* components: Remove passing of CMAKE_BUILD_TYPE to ExternalProject.
* docs: Consolidate info for testing in a dedicated page.
* docs: Add a guide for adding a new component.
* dockerfile: Adding cmsis-pack-utils package prerequisites to the SDK's DevContainer Docker image.
* components: Print ExternalProject configure and build progresses in real time.
* cmake: Enable progress of component fetching.
* trusted-firmware-m: Simplify configuration options.
* components: No need to set DOWNLOAD_COMMAND for ExternalProject.
* docs: Removing CODEOWNERS file as it will be resolved by Gitlab groups
* templates: Use updated toolchain files.
* azure-iot-sdk-c: Fix TLS connection timeout
* aws-iot-device-sdk-embedded-c: Fix coreMQTT packets reception
* ml-embedded-evaluation-kit: Add documentation.
* Fix gcc linker template. Previous version did not check if global/static variables' init value can fit into the FLASH.
* netxduo: Add README.md for the component.
* cmsis-dsp: Remove component as it has no standalone use in the Open IoT SDK.
* cmake: Enable ExternalProject incremental rebuild so that any code changes in custom projects get picked up during rebuild.
* littlefs: Add documentation for component.
* Update threadx-cdi-port to losen message queue requirements.
* templates: Update toolchain files.
* examples: Add example controlling device sockets via pw_rpc & pyedmgr
* docs: Add README for pyedmgr
* docs: Add doc for integration tests with pyedmgr/pw_rpc
* lwip: Add missing include
* components: Provide Mbed Atomic as a standalone.
* components: Provide Mbed Critical as a standalone.
* azure: Make ADU import manifest generator available independently.
* coremqtt-agent: Fix broken link in coreMQTT Agent component README
* components: Remove Matter component
* ci: Update developer-tools
* ci: Enable the Black Duck scan as part of quality-check.
* docs: Update LICENSE.md change security risk for embedded-c to low.
* pre-commit: Skip patch files.
* freertos: Remove unused ThreadX wrapper.
* aws-iot-device-sdk-embedded-c: Improve glue code logic
* aws-iot-device-sdk-embedded-c: Add Unit Tests for Glue Code
* cmsis-5: Fix CMake mock library to accuratly reflect what is mocked
* cmsis-5: Fix datatype in mock
* rpc-socket: Remove empty variable from CMakeLists.txt
* utils: Make serial retarget independent of cmsis-pack-platform.
* aws-network-manager: Add check to ensure all parameters are valid when connecting to the network


# v2023.04 (2023-04-13)

## Changes

* components: Add AVH socket implementation of the iot_socket API.
* documentation: Update to Prerequisits documentation to include information
  regarding troubleshooting installation of the required software for the sdk
* documentation: Update to README.md to include information on building and running the unit tests
  that are included with the sdk.
* NimBLE: Add bluetooth compoments.
* mbedtls: Add unit test for threading alternative implementation
* mbedos-socket: Add unit test for conversion modules
* mbedos-netsocket: Add unit test for IotSocketToMbed
* mbedos_netsocket: Add unit test cases to remaining UDPSocket methods
* aws-client: Remove example build warnings
* contributing: Add description for security risk column
* license: CMSIS_5 category should be 2 not 1
* mdh: Update to most recent version

  Update to the sha for MCU Reference Platforms For Arm to incorporate the
  refactor of delay_loop in mbed-wait that has been done within MCU Driver HAL
* netxduo: Re-organise unit test mocks so they can be re-used
* lwip: Removal of duplicated data type definitions
* docs: Update code owners list.
* cmsis_lwip: Remove duplication of SystemCoreClock from cmsis-lwip example.
* azure-sdk: Remove build warnings from the example
* freertos-api: Fix example build warnings.
* cmsis-rtos-api: Remove example build warnings
* azure-netxduo-iot: Fix build warnings for example
* license: Update license details for the Open IoT SDK

  - Add Azure Netx Duo license details
  - Update component paths for the Open IoT SDK components
  - Remove Googletest license details as this is now included in fff
* license: Add Arm Copyright to Microsoft Third Party IP
* lwip: Use malloc instead of memory pools
* azure: Fixed NTP client task to be more reliable
* mbedtls: Remove build warnings related to the Open IoT SDK for the example
* mbed-os-netsocket: Remove build warnings for example
* codeowners: Fix mistake in username
* ci: Enable possible tpip violation warning messages in Merge Requests.
* pigweed-chrono: Remove build warnings for example
* pigweed-log: Remove build warnings for example
* pigweed-sys-io: Fix build warnings for example
* pigweed-thread: Fix build warnings for example
* tf-m: Remove build warnings for example
* pigweed-rpc: Fix build warnings for example
* Reference generic MDH ARM CMake target
* arm-2d: Remove build warnings for code in the sdk for the test
* cmsis-sockets: Remove build warnings for test for code related to the sdk
* pigweed: Remove build warnings for test
* tf-m: Remove build warnings for code in sdk for tf-m test
* templates: Remove AN552-specific macros and checks.
* NimBLE: Add example using babblesim and AVH socket implementation of HCI drivers
* threadx-cdi-port: Update to include new macros for converting ThreadX priorities to CMSIS-RTOS priorities and vice-versa
* trusted-firmware-m: Add RTOS integration for native ThreadX.
* lwip: Fix receptions issues due to faulty HAL driver and faulty EMAC bridge.
* aws-iot-device-sdk-embedded-c: Update to version that uses MQTT 3.1.1
* azure-iot-sdk: Ensure garbage is filtered out from calls to pool.ntp.org
* asan: Implementation of AddressSanitizer with example
* templates: Fixing typo in VHT command for Corstone-300
* examples: Unify the example target's name with its directory
* tf-m: Improve iotsdk_tf_m_sign_image function
* vscode: Add VScode devcontainer environment
* cmake: Fix PATCH_COMMAND for pigweed to be cross-platform.
* cmake: Fix `static_assert` in pw_iot_sdk_config by depending on `c_static_assert` compile feature.
* Fix release function for the TX path in pigweed sysio.
* docs: Adding table of contents to README and Prerequisites files
* lwip: Add configuration option for static addresses
* tests: Add integration tests for iot socket
* docs: Fix typos
* docs: Clarify prerequisites
* docs: Add directory structure table
* docs: Add a guide for build concepts.
* docs: Add an overview of all Total Solutions examples.
* docs: Remove Keil MDK as an option
* docs: Update the description of directory strucure
* templates: Define MDH_PLATFORM for every template.
* docs: Remove the information about NimBLE and TinyUSB
* docs: Adding missing information about running the software
* docs: Add a guide for creating an application and using the SDK.
* docs: Correcting grammar and naming convention
* docs: Rewording description for Greentea dependencies
* components: Add Matter component.
* components: Add Matter lock-app example.
* developer-tools: Refactor to simplify the pipeline scripts.
  ci: Use single docker image for all jobs
* sanitizers: Add ODR checker
* sanitizers: Add address sanitizer
* sanitizers: Add undefined behaviour sanitizer
* azure-iot-sdk-c: Fixed memory issues in glue code
* pwrpc: Add documentation for example
* Generate website for Open IoT SDK
* docs: Add guide on setting up Arm Virtual Hardware and update example documentation to include example commands for running on both Arm Virtual Hardware and Fixed Virtual Platforms
* iot-ntp-client: Improve memory allocation method
* azure-sdk: Add clang-tidy exceptions for memory management in azure-sdk where memory is allocated or freed in different functions


# v2023.01 (2023-01-19)

## Changes

* ci: Run cppcheck using CMake-generated `compile_commands.json` which contains a list of files and compiler flags. This provides cppcheck with more complete information of which files belong to one build and how each file should be processed, so that cppcheck can provide better code analysis and catch issues that have been missed before.
* ci: Tidy up to enable in public GitLab instance
* autobot: Fix automatic merge by self approving the MRs
* components: Add CMSIS-DSP as a component that can be fetched by having `cmsis-dsp` in the `IOTSDK_FETCH_LIST` variable.
* components: Add Arm-2D as a component that can be fetched by having `arm-2d` in the `IOTSDK_FETCH_LIST` variable.
* tests: Create a test application to verify usability of Arm-2D and CMSIS-DSP with the Open IoT SDK.
* contributing: License file requires to list all components
* cmsis-dsp: Update CMSIS-DSP to the latest and remove the workarounds for CMake entry point and vector conversions.
* ci: Add public sync from public Gitlab to internal GitLab
* cmake: Fix the issue that `lwip-cmsis-sys` always links against `cmsis-rtx` and ensure linking order is correct for callers and implementations of `cmsis-rtos-api`.
* freertos: Convert FreeRTOS-related libraries from interface to static, to ensure the code is built only once rather than inherited as source files by every library or executable that uses FreeRTOS.
* license: Add components information

  LICENSE.md does not only list name of components and their
  licenses but also more detailed information like version,
  url-origin.
* components: Add NetX Duo as a component that can be fetched by having `netxduo` in the `IOTSDK_FETCH_LIST` variable.
* netxduo: Add IP link driver callback for NetX Duo, using EMAC and Network Stack Memory Manager APIs from MCU-Driver-HAL.
* netxduo: Add an example application that demonstrates usage of NetX Duo with the IP link driver callback based on MCU-Driver-HAL.
* netxduo: Add NetX Duo example that talks to an instance of Azure IoT Hub.
* ci: Test the NetX Duo Azure IoT example.
* clang-format: Fix issues with sorting includes

  Sorting is now enabled with the recent developer-tools version.
* netxduo-iot: Fix example Azure IoTHub init error handling.
* Pigweed: Update version.

  - A patch is applied to pass Cpp check and deal with other specificities of the Open IoT SDK.
  - Linkage between backends and modules has been improved.
* CI: Filter out files originating from dependencies when cppcheck is run.
* Testing: Add pyedmgr as an Open IoT SDK dependency. Pyedmgr is a python package that can discover, flash and communicate with multiple boards/FVP. Paired with pytest it can be used to write test which run on many boards.
* Pigweed: Add MDH based backend for pw_sys_io. This change makes other Pigweed modules based on pw_sys_io available to Open IoT SDK users.
* Pigweed: Add CMSIS RTOSv2 based backend for pw_sync and pw_thread modules. This allows users of the Open IoT SDK to get access to these modules and other modules depending on them.
* Pigweed: Add chrono backend.
* Testing: Enable Pigweed unit test suite. This test suite is used to validate the multiple backends implemented in Open IoT SDK.
* Example: Add pigweed RPC example. It demonstrates how to use pigweed RPC with pytest and pyedmgr to build tests that interact with the device through RPCs.
* ci: Create IoT devices in cloud dynamically
* cmsis-freertos: Update v10.5.1
* tf-m: Update to version 1.7.0
* netxduo: Remove fallback to THREADX_TOOLCHAIN=gnu and THREADX_ARCH=cortex_m0, because the latest NetX Duo now comes with `gnu` and `ac6` ports for Cortex-M55 and Cortex-M85 with CMake support.
* connectivity: Update iot socket repository to use the public gitlab repo which is used to extend the iot socket API.
* tls: Update Mbed TLS git URL. Point to Mbed TLS main repository on Github instead of its old mirror.
* changelog: Add towncrier news fragments and configuration


This changelog should be read in conjunction with any release notes provided
for a specific release version.
