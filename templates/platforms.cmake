# Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

macro(get_target_platform_variables platform)
    if("${${platform}-STRING}" STREQUAL "")
        message(FATAL_ERROR "get_target_platform_variables called with invalid platform=${platform}")
    endif()

    set(PLATFORM_STRING ${${platform}-STRING})
    set(PLATFORM_VHT ${${platform}-VHT})
    set(PLATFORM_FVP ${${platform}-FVP})
    set(CMAKE_SYSTEM_PROCESSOR ${${platform}-CMAKE_SYSTEM_PROCESSOR})
    set(CMSIS_device_header ${${platform}-CMSIS_device_header})
    set(CMSIS_PACK_PLATFORM_DEVICE_NAME  ${${platform}-CMSIS_PACK_PLATFORM_DEVICE_NAME})
    # This transforms the list in the generated file into multiline format, instead of the ';'-separated string.
    string(REPLACE ";" "\"\n\"" PLATFORM_CMSIS_PACKS "${${platform}-CMSIS_PACKS}")
    string(REPLACE ";" "\"\n\"" PLATFORM_CMSIS_PACK_COMPONENTS "${${platform}-CMSIS_PACK_COMPONENTS}")
    set(ROM_BASE ${${platform}-ROM_BASE})
    set(ROM_SIZE ${${platform}-ROM_SIZE})
    set(RAM_BASE ${${platform}-RAM_BASE})
    set(RAM_SIZE ${${platform}-RAM_SIZE})
    set(STACK_SIZE ${${platform}-STACK_SIZE})
    set(HEAP_SIZE ${${platform}-HEAP_SIZE})
    set(HTRUN_IMG_FORMAT ${${platform}-HTRUN_IMG_FORMAT})
    set(HTRUN_ARGS ${${platform}-HTRUN_ARGS})
    set(PLATFORM_FLASH ${${platform}-FLASH})
    set(MDH_PLATFORM ${${platform}-MDH_PLATFORM})
endmacro()

macro(get_tfm_target_platform_variables platform)
    if("${${platform}-TFM_PLATFORM}" STREQUAL "")
        message(FATAL_ERROR "get_tfm_target_platform_variables called with invalid platform=${platform}")
    endif()

    get_target_platform_variables(${platform})
    set(ROM_BASE ${${platform}-TFM_NS_ROM_BASE})
    set(ROM_SIZE ${${platform}-TFM_NS_ROM_SIZE})
    set(RAM_BASE ${${platform}-TFM_NS_RAM_BASE})
    set(RAM_SIZE ${${platform}-TFM_NS_RAM_SIZE})
    set(PLATFORM_TFM_PLATFORM ${${platform}-TFM_PLATFORM})
    set(PLATFORM_TFM_ADDRESSES ${${platform}-TFM_ADDRESSES})
endmacro()

# Corstone-300
set(corstone-300-STRING "Corstone-300")
set(corstone-300-VHT "VHT_Corstone_SSE-300_Ethos-U55")
set(corstone-300-FVP "FVP_Corstone_SSE-300_Ethos-U55")
set(corstone-300-CMAKE_SYSTEM_PROCESSOR cortex-m55)
set(corstone-300-CMSIS_device_header SSE300MPS3.h)
set(corstone-300-CMSIS_PACK_PLATFORM_DEVICE_NAME  SSE-300-MPS3)
set(corstone-300-CMSIS_PACKS
    "ARM::V2M_MPS3_SSE_300_BSP@1.3.0"
    "ARM::CMSIS@5.9.0"
    "ARM::CMSIS-Driver@2.7.2")
set(corstone-300-CMSIS_PACK_COMPONENTS
    "ARM::Device:Definition@1.2.0"
    "ARM::Device:Startup&Baremetal@1.2.0"
    "ARM::Native Driver:Timeout@1.0.0"
    "ARM::Native Driver:SysCounter@1.1.0"
    "ARM::Native Driver:SysTimer@1.1.0"
    "ARM::Native Driver:Flash@1.0.0"
    "ARM::CMSIS Driver:Flash:SRAM@1.1.0"
    "ARM::CMSIS Driver:USART@1.0.0"
    "ARM::Native Driver:UART@1.1.0"
    "CMSIS Driver:Ethernet:ETH_LAN91C111")
set(corstone-300-ROM_BASE 0x00000000)
set(corstone-300-ROM_SIZE 0x00080000)
set(corstone-300-RAM_BASE 0x21000000)
set(corstone-300-RAM_SIZE 0x00100000)
set(corstone-300-STACK_SIZE 0x00001000)
set(corstone-300-HEAP_SIZE 0x00001000)
set(corstone-300-HTRUN_IMG_FORMAT .elf)
set(corstone-300-HTRUN_ARGS "--micro=FVP_CS300_U55 --fm=MPS3")
set(corstone-300-FLASH ${CMAKE_CURRENT_LIST_DIR}/common/mps3/flash.c)
set(corstone-300-MDH_PLATFORM "ARM_AN552_MPS3")

# Corstone-300 TF-M support
# NS ROM base = NS image base + header size
# NS ROM size = NS image size - header size - trailer size
set(corstone-300-TFM_PLATFORM arm/mps3/an552)
set(corstone-300-TFM_NS_ROM_BASE "(0x01060000 + 0x400)")
set(corstone-300-TFM_NS_ROM_SIZE "(0x60000 - 0x400 - 0x800)")
set(corstone-300-TFM_NS_RAM_BASE 0x21000000)
set(corstone-300-TFM_NS_RAM_SIZE 0x00100000)
set(corstone-300-TFM_ADDRESSES "0x10000000 0x11000000 0x01060000")

# Corstone-310
set(corstone-310-STRING "Corstone-310")
set(corstone-310-CMAKE_SYSTEM_PROCESSOR cortex-m85)
set(corstone-310-CMSIS_device_header SSE310MPS3.h)
set(corstone-310-CMSIS_PACK_PLATFORM_DEVICE_NAME  SSE-310-MPS3_FVP)
set(corstone-310-CMSIS_PACKS
    "ARM::V2M_MPS3_SSE_310_BSP@1.2.0"
    "ARM::CMSIS@5.9.0"
    "ARM::CMSIS-Driver@2.7.2")
set(corstone-310-CMSIS_PACK_COMPONENTS
    "ARM::Device:Definition@1.2.0"
    "ARM::Device:Startup&Baremetal@1.2.0"
    "ARM::Native Driver:Timeout@1.0.1"
    "ARM::Native Driver:SysCounter@1.0.1"
    "ARM::Native Driver:SysTimer@1.0.0"
    "ARM::Native Driver:Flash@1.1.0"
    "ARM::CMSIS Driver:Flash:SRAM@1.0.0"
    "ARM::CMSIS Driver:USART@1.1.0"
    "ARM::Native Driver:UART@1.1.0"
    "CMSIS Driver:Ethernet:ETH_LAN91C111")
set(corstone-310-ROM_BASE 0x01000000)
set(corstone-310-ROM_SIZE 0x00200000)
set(corstone-310-RAM_BASE 0x21000000)
set(corstone-310-RAM_SIZE 0x00200000)
set(corstone-310-STACK_SIZE 0x00001000)
set(corstone-310-HEAP_SIZE 0x00001000)
set(corstone-310-HTRUN_IMG_FORMAT .elf)
set(corstone-310-VHT "VHT_Corstone_SSE-310")
set(corstone-310-FVP "FVP_Corstone_SSE-310")
set(corstone-310-HTRUN_ARGS "--micro=FVP_CS310 --fm=MPS3")
set(corstone-310-FLASH ${CMAKE_CURRENT_LIST_DIR}/common/mps3/flash.c)
set(corstone-310-MDH_PLATFORM "ARM_AN555_MPS3")

# Corstone-310 TF-M support
# NS ROM base = NS image base + header size
# NS ROM size = NS image size - header size - trailer size
set(corstone-310-TFM_PLATFORM arm/mps3/corstone310/fvp)
set(corstone-310-TFM_NS_ROM_BASE "(0x28080000 + 0x400)")
set(corstone-310-TFM_NS_ROM_SIZE "(0x300000 - 0x400 - 0x800)")
set(corstone-310-TFM_NS_RAM_BASE 0x21200000)
set(corstone-310-TFM_NS_RAM_SIZE 0x00200000)
set(corstone-310-TFM_ADDRESSES "0x11000000 0x38000000 0x28080000")
