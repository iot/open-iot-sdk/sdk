// Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
// SPDX-License-Identifier: Apache-2.0
#ifndef RPC_SOCKET_SERVER_H
#define RPC_SOCKET_SERVER_H

#include "lwip/ip_addr.h"

#include "iotsdk/ip_network_api.h"

/**
 * Set the local IP address
 * @param ip
 */
void set_ip_address(const ip_address_t *ip);

/**
 * Start the RPC server.
 */
void start_rpc_app();

#endif // ! RPC_SOCKET_SERVER_H
