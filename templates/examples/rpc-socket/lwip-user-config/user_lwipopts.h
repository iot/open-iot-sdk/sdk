/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

/* Copy this file to your application and put user overrides of lwip options here.
 * The path to your file must be added to the lwipopts library. */

// disable loopback so that we have only one interface
#define LWIP_NETIF_LOOPBACK 0
#define LWIP_HAVE_LOOPIF    0

// Auto-configure IPv6 address
#define IP_VERSION_PREF                   PREF_IPV6
#define LWIP_SET_DEFAULT_NETIF_HW_ADDRESS 0
#define LWIP_DHCP                         0

// only one netif so we can ignore scope zone checking
#define LWIP_IPV6_SCOPES 0
// we only have a self assigned local address, we want to use it immediately
// having this enabled would delay accepting packets on our address
#define LWIP_IPV6_DUP_DETECT_ATTEMPTS 0

#define LWIP_DEBUG 0
// #define TCP_DEBUG    LWIP_DBG_ON
// #define ETHARP_DEBUG LWIP_DBG_ON
// #define IP_DEBUG     LWIP_DBG_ON
// #define TCPIP_DEBUG  LWIP_DBG_ON
