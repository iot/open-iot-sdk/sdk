/*
 * Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "Driver_USART.h"

#include "cmsis_os2.h"

#include <inttypes.h>

#define PW_LOG_MODULE_NAME "main"
#include "pw_log/log.h"

static ARM_DRIVER_USART *my_serial = NULL;

extern ARM_DRIVER_USART *get_example_serial();

void serial_setup()
{
    my_serial = get_example_serial();
    if ((my_serial->Initialize(NULL) != ARM_DRIVER_OK) || (my_serial->PowerControl(ARM_POWER_FULL) != ARM_DRIVER_OK)
        || (my_serial->Control(ARM_USART_MODE_ASYNCHRONOUS, 115200) != ARM_DRIVER_OK)) {
        return;
    }
    /* Some drivers have TX and RX enabled by default and lacks option to enable/disable them. */
    int ret = my_serial->Control(ARM_USART_CONTROL_TX, 1);
    if (ret != ARM_DRIVER_OK && ret != ARM_DRIVER_ERROR_UNSUPPORTED) {
        return;
    }
    ret = my_serial->Control(ARM_USART_CONTROL_RX, 1);
    if (ret != ARM_DRIVER_OK && ret != ARM_DRIVER_ERROR_UNSUPPORTED) {
        return;
    }
}

static void thread_A(void *argument)
{
    (void)argument;
    PW_LOG_DEBUG("Debug log: %s", "Details about the execution");
    PW_LOG_INFO("Info log: %s", "What is going on");
    PW_LOG_WARN("Warning log: %s", "When something unexpected happen");
    PW_LOG_ERROR("Error log: %s", "In case of serious error");
}

int main()
{
    serial_setup();

    osStatus_t ret = osKernelInitialize();
    if (ret != osOK) {
        return -1;
    }
    // Note: initialization of the logging library is done after the kernel initialization
    // as it requires a mutex.
    pw_log_cmsis_driver_init(my_serial);

    osThreadId_t tid_A = osThreadNew(thread_A, NULL, NULL);
    if (tid_A != NULL) {
        PW_LOG_INFO("Thread A: ID = 0x%p", tid_A);
    } else {
        PW_LOG_ERROR("Failed to create thread");
        return -1;
    }

    osKernelState_t state = osKernelGetState();
    if (state == osKernelReady) {
        PW_LOG_INFO("Starting kernel");
        ret = osKernelStart();
        if (ret != osOK) {
            PW_LOG_ERROR("Failed to start kernel: %d", ret);
            return -1;
        }
    } else {
        PW_LOG_ERROR("Kernel not ready: %d", state);
        return -1;
    }

    return 0;
}
