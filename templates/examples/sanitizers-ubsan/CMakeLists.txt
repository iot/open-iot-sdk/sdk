# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

# UndefinedBehaviorSanitizer example

set(example sanitizers-ubsan)
set(supported_platforms corstone-300 corstone-310)

foreach(platform IN LISTS supported_platforms)
    get_target_platform_variables(${platform})
    set(EXAMPLE_TITLE "Example for UndefinedBehaviorSanitizer")
    file(STRINGS intro.md EXAMPLE_INTRO NEWLINE_CONSUME)
    set(example_dir ${EXAMPLES_OUTPUT_DIR}/${example}/${platform})
    set(EXAMPLE_BUILD_EXECUTABLE "__build/iotsdk-example-sanitizers-ubsan.elf")
    set(EXAMPLE_RUN_COMMAND_VHT "```sh\r\n${PLATFORM_VHT} -a ${EXAMPLE_BUILD_EXECUTABLE}\r\n```")
    if(platform STREQUAL corstone-300)
        set(EXAMPLE_RUN_COMMAND_FVP "```sh\r\n${PLATFORM_FVP} -a ${EXAMPLE_BUILD_EXECUTABLE}\r\n```")
    elseif(platform STREQUAL corstone-310)
        set(EXAMPLE_RUN_COMMAND_FVP "**Note: A standalone Arm Ecosystem FVP is not available for Corstone-310, only the version included in the Keil MDK**\r\n\n```sh\r\n${PLATFORM_FVP} -a ${EXAMPLE_BUILD_EXECUTABLE}\r\n```")
    else()
        set(EXAMPLE_RUN_COMMAND_FVP "A Fixed Virtual Platform is not available for ${platform}")
    endif()
    configure_file(${LINKER_SCRIPT_GCC} ${example_dir}/gcc.ld @ONLY)
    configure_file(${LINKER_SCRIPT_ARM} ${example_dir}/armclang.sct @ONLY)

    configure_file(CMakeLists.txt.in ${example_dir}/CMakeLists.txt @ONLY)
    configure_file(${CMSIS_PACK_PLATFORM_CONFIG} ${example_dir}/cmsis-pack-platform.cmake @ONLY)
    configure_file(${README_TEMPLATE_FILE} ${example_dir}/README.md @ONLY)

    file(COPY
        main.c
        test.c
        test.log
        sanitizers-config
        DESTINATION ${example_dir}
    )
endforeach()
