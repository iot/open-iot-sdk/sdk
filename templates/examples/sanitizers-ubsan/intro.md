## Overview

This example demonstrates using the Open IoT SDK UndefinedBehaviorSanitizer.

In this example the following undefined behaviours are triggered and caught:

* Passing `NULL` to a `non-null` argument
* Underflowing a signed integer by subtraction, addition, multiplication and division
* Dividing an integer by zero

Please see [the Sanitizers read-me](../../../components/sanitizers/ubsan/ReadMe.md) for further details.
