# Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

# AWS Client example

set(example aws-client)
set(supported_platforms corstone-300 corstone-310)
set(supported_rtoses cmsis-rtx cmsis-freertos)

file(STRINGS intro.md EXAMPLE_INTRO NEWLINE_CONSUME)

foreach(platform IN LISTS supported_platforms)
    get_target_platform_variables(${platform})

    foreach(rtos IN LISTS supported_rtoses)
        get_target_rtos_variables(${rtos})

        set(EXAMPLE_TITLE "Example for AWS Client")

        set(EXAMPLE_BUILD_EXECUTABLE "__build/iotsdk-example-aws-client.elf")
        set(EXAMPLE_RUN_COMMAND_VHT "```sh\r\n${PLATFORM_VHT} -a ${EXAMPLE_BUILD_EXECUTABLE} -C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1\r\n```")
        if(platform STREQUAL corstone-300)
            set(EXAMPLE_RUN_COMMAND_FVP "```sh\r\n${PLATFORM_FVP} -a ${EXAMPLE_BUILD_EXECUTABLE} -C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1\r\n```")
        elseif(platform STREQUAL corstone-310)
            set(EXAMPLE_RUN_COMMAND_FVP "**Note: A standalone Arm Ecosystem FVP is not available for Corstone-310, only the version included in the Keil MDK**\r\n\n```sh\r\n${PLATFORM_FVP} -a ${EXAMPLE_BUILD_EXECUTABLE} -C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1\r\n```")
        else()
            set(EXAMPLE_RUN_COMMAND_FVP "A Fixed Virtual Platform is not available for ${platform}")
        endif()
        set(example_dir ${EXAMPLES_OUTPUT_DIR}/${example}/${rtos}/${platform})

        configure_file(CMakeLists.txt.in ${example_dir}/CMakeLists.txt @ONLY)
        configure_file(configs/CMakeLists.txt.in ${example_dir}/configs/CMakeLists.txt @ONLY)
        configure_file(${CMSIS_PACK_PLATFORM_CONFIG} ${example_dir}/cmsis-pack-platform.cmake @ONLY)
        configure_file(${README_TEMPLATE_FILE} ${example_dir}/README.md @ONLY)
        # Override platform-default heap size as Mbed TLS makes large allocations
        set(HEAP_SIZE 0x00010000)
        configure_file(${LINKER_SCRIPT_GCC} ${example_dir}/gcc.ld @ONLY)
        configure_file(${LINKER_SCRIPT_ARM} ${example_dir}/armclang.sct @ONLY)

        file(COPY
            main.c
            aws-credentials
            test.log
            .ci_hooks
            DESTINATION ${example_dir}
        )
        file(COPY
            configs/aws-configs
            configs/lwip-user-config
            configs/mbedtls-user-config
            DESTINATION ${example_dir}/configs
        )
        if(rtos STREQUAL "cmsis-freertos")
            file(COPY configs/freertos-config DESTINATION ${example_dir}/configs)
        endif()
    endforeach()
endforeach()
