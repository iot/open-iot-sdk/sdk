/*
 * Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
#include "Driver_USART.h"
#include "cmsis_os2.h"

#include <cinttypes>
#include <cstdarg>
#include <cstdio>

#define PW_LOG_MODULE_NAME "main"

#include "pw_assert/check.h"
#include "pw_log/log.h"
#include "pw_log_cmsis_driver/backend.h"
#include "pw_thread/id.h"
#include "pw_thread/thread.h"
#include "pw_thread/thread_core.h"
#include "pw_thread/yield.h"
#include "pw_thread_cmsis_rtos/options.h"

using pw::thread::Thread;
using pw::thread::ThreadCore;
using pw::thread::cmsis_rtos::Options;
namespace this_thread = pw::this_thread;

extern "C" ARM_DRIVER_USART *get_example_serial();
static ARM_DRIVER_USART *my_serial;

static void serial_setup()
{
    my_serial = get_example_serial();
    if ((my_serial->Initialize(NULL) != ARM_DRIVER_OK) || (my_serial->PowerControl(ARM_POWER_FULL) != ARM_DRIVER_OK)
        || (my_serial->Control(ARM_USART_MODE_ASYNCHRONOUS, 115200) != ARM_DRIVER_OK)) {
        return;
    }
    /* Some drivers have TX and RX enabled by default and lacks option to enable/disable them. */
    int ret = my_serial->Control(ARM_USART_CONTROL_TX, 1);
    if (ret != ARM_DRIVER_OK && ret != ARM_DRIVER_ERROR_UNSUPPORTED) {
        return;
    }
    ret = my_serial->Control(ARM_USART_CONTROL_RX, 1);
    if (ret != ARM_DRIVER_OK && ret != ARM_DRIVER_ERROR_UNSUPPORTED) {
        return;
    }
}

struct ThreadCoreImpl : ThreadCore {
    void Run() override
    {
        osThreadId_t id = reinterpret_cast<osThreadId_t>(this_thread::get_id());
        PW_CHECK_NOTNULL(id, "this_thread::get_id failed");

        const char *name = osThreadGetName(id);
        PW_CHECK_NOTNULL(name, "osThreadGetName failed");

        PW_LOG_INFO("%s ran to completion", name);
    }
};

static void main_task_cb(void *ignored)
{
    PW_LOG_INFO("Main task started");

    ThreadCoreImpl thread_cb1, thread_cb2, thread_cb3;

    Options options1, options2, options3;
    options1.set_name("thread1");
    options2.set_name("thread2");
    options3.set_name("thread3");

    Thread thread1(options1, thread_cb1);
    Thread thread2(options2, thread_cb2);
    Thread thread3(options3, thread_cb3);

    this_thread::yield();

    thread1.detach();
    PW_LOG_INFO("thread1 detached");
    thread2.join();
    PW_LOG_INFO("thread2 complete");
    thread3.join();
    PW_LOG_INFO("thread3 complete");

    PW_LOG_INFO("Main task complete");

    (void)ignored;
}

int main(void)
{
    serial_setup();

    int32_t res = osKernelInitialize();
    if (res != osOK) {
        printf("osKernelInitialize failed: %" PRId32 "\n", res);
        return -1;
    }

    puts("Kernel initialised");
    pw_log_cmsis_driver_init(my_serial);

    // NB: pw::thread_cmsis_rtos requires the kernel to be started before initialising the first thread, so the first
    // thread should be created via CMSIS RTOS API directly.
    osThreadId_t main_thread = osThreadNew(main_task_cb, nullptr, nullptr);
    PW_CHECK_NOTNULL(main_thread, "osThreadNew failed");

    auto state = osKernelGetState();
    PW_CHECK_INT_EQ(state, osKernelReady, "kernel is not ready");

    PW_LOG_INFO("Starting kernel");

    res = osKernelStart();
    PW_CHECK_INT_EQ(res, osOK, "osKernelStart failed");

    PW_LOG_CRITICAL("osKernelStart should not have returned but did");
    PW_UNREACHABLE;
}
