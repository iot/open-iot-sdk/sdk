Flash an RPC server binary to Arm Virtual Hardware and communicate with it from a Python test using Pigweed RPC.

## Pre-build setup

Source `example/bootstrap.sh` to set up the environment for building the example. This creates and activates a Python virtual environment (if required) and installs the required packages for the Pytest script, and sets an environment variable needed for building with Pigweed RPC.

When this is done build with Cmake as usual:

```sh
source `example/bootstrap.sh`
cmake -S. -B__build -GNinja --toolchain=toolchains/toolchain-arm-none-eabi-gcc.cmake
```

## Pre-run setup

The `example/run-example` script handles most of the setup but there is an additional step for `pyedmgr` (Python library which handles flashing and communicating with the FVP). It uses `Iris` to communicate with the FVP, so the `IRIS_INSTALL_DIR` environment variable needs to be set (unless the FVP binary's location is in `PATH` in which case `pyedmgr` should find it automatically).

For example, if the FVP is installed to `/opt/FVP_Corstone_SSE-300` then `IRIS_INSTALL_DIR` is `/opt/FVP_Corstone_SSE-300/Iris/Python` so we can set `IRIS_INSTALL_DIR` as follows:

```sh
export IRIS_INSTALL_DIR=/opt/FVP_Corstone_SSE-300/Iris/Python
```
