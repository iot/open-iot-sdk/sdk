/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef NIMBLE_USER_CFG_H_
#define NIMBLE_USER_CFG_H_

/**
 * Look into NimBLE's source for syscfg.yml files for more info on what #Defines are supported
 * Look into components/bluetooth/cmsis-nimble-port/README.md for more info on how to find the descriptions
 */

#define MYNEWT_VAL_LOG_LEVEL (1) // INFO

#endif // NIMBLE_USER_CFG_H_
