#!/bin/bash
# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0
set -x


bs_2G4_phy_v1 -s=hci-bridge-test -D=3 &
bs_device_handbrake -s=hci-bridge-test -d=0 -r=1 &
bs_nrf52_bsim_samples_hci_uart -s=hci-bridge-test -d=1 &
# need to capture signal
bs_nrf52_bsim_samples_hci_uart -s=hci-bridge-test -d=2
sleep 1

echo "Stopping other processes"
stop_bsim.sh
