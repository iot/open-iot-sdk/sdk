// Copyright (c) 2021-2023, Arm Limited and Contributors. All rights reserved.
// SPDX-License-Identifier: Apache-2.0

extern "C" {
#include "Driver_USART.h"

ARM_DRIVER_USART *get_example_serial();
void pw_sys_io_init(ARM_DRIVER_USART *);
}

void serial_setup()
{
    ARM_DRIVER_USART *serial = get_example_serial();
    if ((serial->Initialize(NULL) != ARM_DRIVER_OK) || (serial->PowerControl(ARM_POWER_FULL) != ARM_DRIVER_OK)
        || (serial->Control(ARM_USART_MODE_ASYNCHRONOUS, 115200) != ARM_DRIVER_OK)) {
        return;
    }
    /* Some drivers have TX and RX enabled by default and lacks option to enable/disable them. */
    int ret = serial->Control(ARM_USART_CONTROL_TX, 1);
    if (ret != ARM_DRIVER_OK && ret != ARM_DRIVER_ERROR_UNSUPPORTED) {
        return;
    }
    ret = serial->Control(ARM_USART_CONTROL_RX, 1);
    if (ret != ARM_DRIVER_OK && ret != ARM_DRIVER_ERROR_UNSUPPORTED) {
        return;
    }

    pw_sys_io_init(serial);
}
