## Overview

This example demonstrates using the Open IoT SDK AddressSanitizer.

Currently only the `arm-none-eabi-gcc` toolchain is supported.

Please see [the AddressSanitizer (asan) read-me](https://gitlab.arm.com/iot/open-iot-sdk/sdk/-/blob/main/components/sanitizers/asan/ReadMe.md) for details.
