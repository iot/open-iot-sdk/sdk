/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef IOTSDK_ASAN_CONFIG_H
#define IOTSDK_ASAN_CONFIG_H

#define IOTSDK_ASAN_SYM_HEAP_START __end__
#define IOTSDK_ASAN_SYM_HEAP_END   __HeapLimit

#define IOTSDK_ASAN_SYM_STACK_START __StackLimit
#define IOTSDK_ASAN_SYM_STACK_END   __StackTop

#define IOTSDK_ASAN_SHADOW_START __shadow_start__
#define IOTSDK_ASAN_SHADOW_END   __shadow_end__

#include <pw_log/log.h>
#define iotsdk_sanitizers_error_cb(...) PW_LOG_ERROR(__VA_ARGS__)

#endif /* ! IOTSDK_ASAN_CONFIG_H */
