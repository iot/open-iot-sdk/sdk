/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

/* Copy this file to your application and put user overrides of lwip options here.
 * The path to your file must be added to the lwipopts library. */

#define LWIP_IPV4_STATIC_ADDRESS_IP_ADDRESS 0x0AC8012A /*   10.200.1.42 */
#define LWIP_IPV4_STATIC_ADDRESS_IP_NETMASK 0xFFFFFF00 /* 255.255.255.0 */
#define LWIP_IPV4_DEFAULT_GATEWAY           0x0AC80102 /*    10.200.1.2 */

// disable loopback so that we have only one interface
#define LWIP_NETIF_LOOPBACK 0
#define LWIP_HAVE_LOOPIF    0

// if we set the HW address we can control the link local ipv6 address of the default netif
#define LWIP_SET_DEFAULT_NETIF_HW_ADDRESS 1
// link local ipv6 address is generated based on the HW address, this results in ipv6 fe80::223:c1ff:fede:0001
#define LWIP_DEFAULT_NETIF_HW_ADDRESS 0x0023C1DE0001

// we set the address statically
#define LWIP_DHCP 0
// only one netif so we can ignore scope zone checking
#define LWIP_IPV6_SCOPES 0
// we only have a self assigned local address, we want to use it immediately
// having this enabled would delay accepting packets on our address
#define LWIP_IPV6_DUP_DETECT_ATTEMPTS 0
