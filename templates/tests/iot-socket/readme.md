# IOT socket integration test using RPC service

## Overview

The RPC server running on the board provides an RPC API which wraps the CMSIS IoT Socket API.

A summary of the IoT Socket API can be found
[here](https:/mdk-packs.github.io/IoT_Socket/html/group__iotSocketAPI.html#ga22311af9784ef6317ce092ccf392d049).

Each IoT Socket API function is wrapped by one endpoint in the RPC API.
The caller places the API input parameters in a protobuf request message and the protobuf API calls the IoT Socket API.
It places the status and any output parameter values into the response message and returns a Pigweed status code.
The Pigweed status should be checked first.

### Limitations

* Messages have size limitations not present in the API, check `src/iot-socket.options` for maximum sizes of fields
* Protobuf does not have 16-bit integers, so ports are transmitted as uint32 instead

## Prerequisites

* FVP for chosen platform - presumed pre-installed and in $PATH
* Iris - presumed to exist at `$(dirname $(which FVP_Corstone_SSE-300_Ethos-U55))/../../Iris/Python`
* Protocol Buffers - the binary needs to be in $PATH
* Build with the `PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python` environment variable set
```sh
export PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION="python"

```

The last two requirements can be fulfilled by sourcing `test/bootstrap.sh` from the repository root.
The script will download the `protoc` compiler release, unpack it and add it to the $PATH
and set the `PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION`.

## Build & run

Build like any other example and run with `ctest`.
Test will require root privileges to setup the network environment.
