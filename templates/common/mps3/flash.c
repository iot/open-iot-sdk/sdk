/*
 * Copyright (c) 2022, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "Driver_Flash.h"

extern ARM_DRIVER_FLASH Driver_FLASH0;

ARM_DRIVER_FLASH *get_example_flash()
{
    return &Driver_FLASH0;
}
