# Running Applications on Arm Virtual Hardware (AVH)

## What is AVH

[AVH] provides a platform for developers to use the Open IoT SDK with accurate models of Arm hardware. Currently the Open IoT SDK supports the following hardware through AVH:

* [Corstone-300] with Ethos-U55
  * AVH executable: `VHT_Corstone_SSE-300_Ethos-U55`
* [Corstone-310] with Ethos-U55
  * AVH executable: `VHT_Corstone_SSE-310`

All these hardware configurations are provided as [Fast Models] and run through the console of the Amazon Machine Image (AMI). AVH does provide models for other platforms, however these are not currently supported by the Open IoT SDK.

## Setup

For AVH setup instructions, follow the ["Launch Arm Virtual Hardware system"][Setting Up AVH] instructions using the method of your choice.

For building the examples in the Open IoT SDK, please refer to the example specific `README.md` for instructions, found in:
* `__codegen/examples/<example_name>` for generated examples within the Open IoT SDK repository.
* `examples/<example_name>` if you are using the [sdk-examples] repository.

## Running an application on AVH
Once you have built the example, the `README` will include an example command for running the application on the created AMI. However, you should update the build directory in the command if you are using a build directory other than the default `__build` directory. All optional arguments required to run the specific application on AVH will be provided in the `README`.

### Command breakdown
```
VHT_Corstone_SSE-300_Ethos-U55 -a __build/iotsdk-example-azure-sdk.elf -C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1
```
Taking the command for running the `azure-sdk` example on the Corstone-300 platform, it can be broken down to:
* `VHT_Corstone_SSE-300_Ethos-U55` - The fast model executable used to run the application on.
* `-a __build/iotsdk-example-azure-sdk.elf` - Specification of the path to the application executable.
* `-C mps3_board.smsc_91c111.enabled=1` - Model argument required to enable the ethernet controller chip.
* `-C mps3_board.hostbridge.userNetworking=1` - Model argument required to enable user networking.


[Setting Up AVH]: https://gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions#launch-arm-virtual-hardware-instance
[AVH]:https://avh.arm.com
[Corstone-300]: https://developer.arm.com/Processors/Corstone-300
[Corstone-310]: https://developer.arm.com/Processors/Corstone-310
[Fast Models]: https://developer.arm.com/Tools%20and%20Software/Fast%20Models
[sdk-examples]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/
