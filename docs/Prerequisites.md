# Prerequisites

## Supported development environments

We support the [VSCode development environment](vscode_dev_environment.md) for a quick setup on Linux, macOS and Windows. The native development environment is only supported on Linux and detailed in the sections below.

!!! tip

    If you are using the [VSCode development environment](vscode_dev_environment.md), please *skip* the sections below.

## Git

[Git](https://git-scm.com/downloads) is required for acquiring the source code.

## Developer-tools

Our tools configuration are part of [developer tools](https://gitlab.arm.com/iot/open-iot-sdk/tools/developer-tools).

Use `git clone --recurse-submodules` to fetch also submodules like developer-tools.

## Toolchains

A toolchain is required for building the examples, tests, and other code. There are two choices for the toolchain.

| Toolchain | Download | C Compiler |
|---------|----------|----------------|
| Arm Compiler for Embedded 6.18 | [standalone](https://developer.arm.com/downloads/-/arm-compiler-for-embedded) | armclang |
| GNU Arm Embedded version 10.3 (10.3-2021.10) | [standalone](https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads) | arm-none-eabi-gcc |

!!! note

    Please make sure you use the specified versions of the toolchains as the Open IoT SDK has been validated to work with these exact versions.

The open source GNU Arm Embedded Toolchain is recommended for quick start.

Whichever toolchain is chosen, it should be verified that the toolchain is in the system path to allow it to be located. For the Arm Compiler, `armclang --version` or with the GNU Arm Embedded Toolchain, `arm-none-eabi-gcc --version` can be used to verify this.

## CMake

Use [CMake](https://cmake.org/download/) v3.21.0 or newer for the build process.

## Build system

For this project, either *GNU Make* or *Ninja* is required as the CMake build generator. GNU Make is usually present on Linux distributions, but [Ninja](https://ninja-build.org/) is strongly preferred for its fast build speed.

Whichever build generator is chosen, it should be verified that the build generator is in the system path to allow it to be located. For Ninja, `ninja --version` or with GNU Make, `make --version` can be used to verify this.

If you are using a generator that is not the default for your platform, CMake must be informed either using the `CMAKE_GENERATOR` environment variable or by passing `-G <generator-name>` in the CMake command line. See [CMake options](https://cmake.org/cmake/help/latest/manual/cmake.1.html) and [CMake generators](https://cmake.org/cmake/help/latest/manual/cmake-generators.7.html) for more information.

!!! example

    If you want to use Ninja for a build, pass `-GNinja` when configuring the build. To avoid needing `-GNinja` every time and make it the default, you can add `export CMAKE_GENERATOR=Ninja` to your shell's profile.

!!! tip

    For additional build options please refer to the [official CMake documentation](https://cmake.org/cmake/help/latest/manual/cmake.1.html).

## Arm Corstone-300 MPS3 Fixed Virtual Platform (FVP)

### Installation

1. Obtain and install the FVP model for the Corstone-300 MPS3 based platform. Installers for both, Linux and Windows, are available [here](https://developer.arm.com/tools-and-software/open-source-software/arm-platforms-software/arm-ecosystem-fvps). FVP_Corstone_SSE-300_11.16_26, which simulates AN552, is recommended.

1. It is recommended to add the FVP executable to your PATH environment variable. The binaries are located
    - in `<installation_directory>/models/Linux64_GCC-6.4/`, for Linux,
    - in `<installation_directory>\models\Win64_VC2017\`, for Windows.

You can verify that the FVP is correctly added to the `PATH` environment variable by running `FVP_Corstone_SSE-300_Ethos-U55 --version`.

To enable `ctest` to run tests on the FVP, install mbed-fastmodel-agent Python module. Note that it is required to set the `<path_to>` placeholders with the path to the FVP binary before executing the final install command.

```sh
$ git clone https://github.com/ARMmbed/mbed-fastmodel-agent.git
$ cd mbed-fastmodel-agent
$ git checkout v2.0.3
$ cat <<EOF >fm_agent/settings.json
{
    "COMMON": {
        "IRIS_path": "<path_to>/FVP_Corstone_SSE-300/Iris/Python/"
    },
    "FVP_CS300_U55": {
        "model_binary": "<path_to>/FVP_Corstone_SSE-300/models/Linux64_GCC-6.4/FVP_Corstone_SSE-300_Ethos-U55",
        "model_options": [
            "--quantum=10"
        ],
        "terminal_component": "component.FVP_MPS3_Corstone_SSE_300.mps3_board.telnetterminal0",
        "configs": {
            "MPS3": "MPS3.conf",
            "MPS3-tap-network": "MPS3-tap-network.conf"
        }
    }
}
EOF
$ sudo python3 setup.py install
```

### Running an Application on the FVP
For guidance on running applications on the Corstone-300 FVP, please see the `README.md` for the examples which are included within the Open IoT SDK. The following parameters, `-C mps3_board.smsc_91c111.enabled=1 -C mps3_board.hostbridge.userNetworking=1` enable Ethernet on the FVP which is required for the Open IoT SDK and should be included in the command to run the FVP.

For further information on the Ecosystem FVP, please see [here](https://developer.arm.com/documentation/100966/1118/Arm--Corstone-SSE-300-FVP).

## Greentea tests

There are some additional prerequisites to run Greentea tests for the included examples.

### Python

[Python](https://www.python.org/downloads/) v3.8+ is recommended. Pip (included with all recent versions of Python) is required to install packages.

### htrun
Tests require the `htrun` command provided by the
[greentea-host](https://pypi.org/project/greentea-host/) python package.

Once Python is installed, it can be installed with pip with this command:

```sh
pip3 install greentea-host --upgrade
```

It should be verified that `htrun` is in the system path. This can be done by running `htrun --version`.

For testing on Corstone-300 FVP, the FastModel agent https://github.com/ARMmbed/mbed-fastmodel-agent.git needs to be fetched, with fm_agent/settings.json edited to point to your local installation of the FVP for the model binary as well as the path to Iris. Alternatively, you can keep the default values in the file and symlink your installation to there. Then do `pip3 install <path/to/mbed-fastmodel-agent>` to install it.

## Trusted Firmware-M (TF-M)

### Python packages

To build TF-M, a few Python packages need to be installed after configuration (i.e. the step with `--toolchain`) and before building (i.e. the step with `--build`):

```sh
pip3 install -r https://git.trustedfirmware.org/TF-M/trusted-firmware-m.git/plain/tools/requirements.txt?h=TF-Mv1.6.0
```

This only needs to be done once unless the revision of TF-M has changed.

### Objcopy and SRecord

To convert and combine signed TF-M images to the ELF format required by the FVP, you need

* *either* `arm-none-eabi-objcopy` from the GNU Arm Embedded Toolchain (described above) *or* `objcopy` from GNU Binutils
* `srec_cat` from SRecord

If you want to use the Arm Compiler without setting up the GNU Arm Embedded Toolchain, you can get the `objcopy` utility by installing the `binutils` package provided by your operating system's package manager, and add the extracted `bin` subdirectory to the `PATH` environment variable.

You can get `srec_cat` by installing the `srecord` package provided by your operating system's package manager.

## Toubleshooting
If the applications or Python modules that you are trying to use are not discoverable in the shell, add their respective installation paths to the appropriate OS environment variable.
