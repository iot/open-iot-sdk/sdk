# CI Build Hooks

The CI build process has been updated to offload template-specific build steps to the templates themselves

This is done by having templates define optional build hooks which get executed at specific times by the CI runner.

The available hooks are:

* `pre_cmake_configure` - executed before Cmake configure. Use for setting up build environment e.g. installing Python packages.
* `pre_cmake_build` - executed after Cmake configure but before Cmake build
* `post_cmake_build` - executed after Cmake build and before cppcheck
* `post_cppcheck` - executed after cppcheck, at the end of the job

To use a hook, create an executable file with the name of the hook in a directory called `.ci_hooks` in the template's base directory.

For example, to create a `pre_cmake_configure` hook for the `aws-client` example:

```sh
mkdir -p templates/examples/aws-client/.ci_jobs
$EDITOR templates/examples/aws-client/.ci_jobs/pre_cmake_configure
```

Further information:

* The hooks are sourced which means any variables set inside a hook are brought into the parent scope. Best practice is to prefix variables e.g. with `__hook` and `unset` them when finished
* The working directory for hooks is the project root directory
* Output (stdout & stderr) from a hook will be echoed to the terminal
* The build process will fail if the script exits with non-zero status
* Some common cases are handled by functions in the file [`.gitlab/ci/pipeline/ci_hooks_common`](../.gitlab/ci/pipeline/ci_hooks_common), these functions are available to be called from hooks (i.e. you do not need to source anything)
