# Total Solutions applications

## Contents

- [Introduction](#introduction)
- [Overview](#overview)
- [Blinky, Keyword and Speech](#blinky-keyword-and-speech)
- [Arm-2D Benchmark and Watch Panel](#arm-2d-benchmark-and-watch-panel)
- [Blinky Example](#blinky-example)

## Introduction

[Arm IoT Total Solutions][total-solutions] provides a complete solution designed for specific use-cases, leaving developers to focus on what really matters - innovation and differentiation across diverse and varied use cases. It has everything needed to simplify the design process and streamline product development, including:
* Hardware IP
* Software
* Real-time OS support
* Machine learning (ML) models
* Advanced tools such as the new Arm Virtual Hardware
* Application specific reference code
* Support from the world's largest IoT ecosystem.

It provides applications running in secure and non-secure partitions controlled with [`TrustedFirmware-M`][trusted-firmware-m] (TF-M).

The applications are based on Arm's Corstone SSE-300 and Corstone SSE-310 reference platforms which hardware abstraction layers (HALs) are provided with an implementation of the CMSIS-Driver API in [CMSIS-Pack][cmsis-pack].

Total Solutions applications have been validated to run on emulations known as Fast Models Fixed Virtual Platforms (FVP) and Arm Virtual Hardware (AVH):
* [`Corstone SSE-300 FVP`][cs300-fvp-docs]
* [`Corstone SSE-310 FVP`][cs310-fvp-docs]
* [`Arm Virtual Hardware`][avh-docs]

TF-M and MDH-ARM are made available to applications through The [`Open IoT SDK`][open-iot-sdk] which refers to them as components. The `Open IoT SDK` is a one-stop project from which all necessary components to build a fully fledged IoT application (from device drivers to commercial cloud platforms connection) are made available. It also provides libraries to seamlessly integrate these components with end users applications. Component selection is done by listing the component to fetch in the [`IOTSDK_FETCH_LIST`][iotsdk-component-selection] CMake variable.

## Overview

The following Total Solutions applications demonstrate the latest Arm technlogies:

* [`Blinky`][blinky] - periodically blinks a set of LEDs
* [`Keyword`][keyword] - takes in an audio clip and uses a machine learning model to spot keywords and use them to control the state of LEDs
* [`Speech`][speech] - takes in an audio clip and uses a machine learning model to output a transcript of spoken sentences
* [`Benchmark`][ts-pixelgraphics-benchmark] - draws on the screen to demonstrate numerous features of [`Arm-2D`][arm-2d] graphic library
* [`Watch Panel`][ts-pixelgraphics-watchpanel] - uses the `Arm-2D` graphic library to draw a clock

All of the applications above have TrustedFirmware-M enabled in their systems. They come with support for building them with [`Arm Compiler for Embedded`][arm-compiler-for-embedded] using [`CMake`][cmake]. Though TS-PixelGraphics also comes with [`GNU Arm Embedded Toolchain`][gnu-arm-embedded-toolchain] support.

The following Total Solutions application demonstrates the basic use of the Open IoT SDK *without* the latest technologies (such as `TrustedFirmware-M`) enabled:

* [`Blinky Example`][blinky-example] - periodically blinks an LED

Building `Blinky Example` is only supported with [`GNU Arm Embedded Toolchain`][gnu-arm-embedded-toolchain].

As memory region organization is intrinsic to the application, linker scripts are not provided by the `Open IoT SDK`.
`Blinky`, `Keyword` and `Speech` share the same linker scripts (also known as scatter files for Arm Compiler for Embedded) whether building for [`Corstone SSE-300`][linker-cs300-ts] or [`Corstone SSE-310`][linker-cs310-ts]. `Benchmark` and `Watch Panel` contain linker scripts for both platforms in their respective subdirectories. `Blinky Example` contains linker scripts for `Cortex-M4 SMM` and `Corstone SSE-300`.

## Keyword and Speech

These applications run on top of a Real-Time Operating System (RTOS) to orchestrate the different threads. `CMSIS-RTOS RTX`, `FreeRTOS`, and `Azure RTOS ThreadX` are currenty supported.

The `Keyword` and `Speech` applications use Machine Learning (ML) inference models provided by [`Arm ML embedded evaluation kit`][arm-ml-embedded-evaluation-kit]. The Arm ML embedded evaluation kit targets `Arm Cortex-M` CPUs and `Arm Ethos-U` NPUs. The applications can connect to Cloud Service Provider (CSP) platforms to broadcast the ML inference results to the cloud in an MQTT topic. The applications can also process Over-The-Air (OTA) update requests from CSP platforms and securely apply updates using the [PSA Firmware Update API](https://github.com/arm-software/psa-api).

The CSP platforms currently supported are:
* [Amazon Web Services (AWS)][aws]
* [Microsoft Azure][azure]

As connection to the CSP platforms (also know as endpoint) uses different networking stacks, the applications include specific source files depending on whether building to connect to AWS or Azure. The rest of this section will have hyperlinks for the `Keyword` application, however, the same is also true for the `Speech` application.
The [application source files][keyword-source-files] that are specific to an endpoint are prefixed with either `aws_*` or `azure_*` and are [selected in CMake][keyword-cmake-endpoint-selection] depending on the endpoint select when running the build script. The [`main()` function][keyword-main-function] is the same regardless of the endpoint.

For more infomation including RTOS and endpoint compatibility, read the [build instructions][total-solutions-build-instructions].

## Arm-2D Benchmark and Watch Panel

The [Arm-2D][arm-2d] library provides low level 2D image processing services mainly used in deeply embedded display systems. The rendering performance is accelerated by the latest features of the `Arm Cortex-M` processors, most notably [`Arm Helium`][arm-helium] (also known as the `M-Profile Vector Extension`, `MVE`). `Arm Helium` is part of the latest [`Armv8.1-M`][armv81m] architecture implemeneted by the [`Cortex-M55`][cortex-m55] processor (available on `Corstone SSE-300`) and the [`Cortex-M85`][cortex-m85] processor (available on `Corstone SSE-310`), although earlier generations of `Cortex-M` processors without Helium are also supported by Arm-2D.

The [`Benchmark`][ts-pixelgraphics-benchmark] and [`Watch Panel`][ts-pixelgraphics-watchpanel]  applications use the Arm-2D API to draw complex animation frames and display the frames on the LCD panels of `Corstone SSE-300` and `Corstone SSE-310` Fast Model Fixed Virtual Platforms.


[arm-2d]: https://github.com/ARM-software/Arm-2D
[arm-compiler-for-embedded]: https://developer.arm.com/downloads/-/arm-compiler-for-embedded
[arm-helium]: https://www.arm.com/technologies/helium
[arm-ml-embedded-evaluation-kit]: https://review.mlplatform.org/plugins/gitiles/ml/ethos-u/ml-embedded-evaluation-kit/
[avh-docs]: https://www.arm.com/products/development-tools/simulation/virtual-hardware
[armv81m]: https://www.arm.com/architecture/cpu/m-profile
[aws]: https://aws.amazon.com/?nc2=h_lg
[azure]: https://azure.microsoft.com/en-gb/free/search/?&ef_id=EAIaIQobChMIu4uu-dfR_QIVNhPUAR21ewqMEAAYASAAEgJxBvD_BwE:G:s&OCID=AIDcmm3bvqzxp1_SEM_EAIaIQobChMIu4uu-dfR_QIVNhPUAR21ewqMEAAYASAAEgJxBvD_BwE:G:s&gclid=EAIaIQobChMIu4uu-dfR_QIVNhPUAR21ewqMEAAYASAAEgJxBvD_BwE
[blinky]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/tree/main/examples/blinky
[cmake]: https://cmake.org/
[cortex-m55]: https://developer.arm.com/processors/cortex-m55
[cortex-m85]: https://developer.arm.com/processors/cortex-m85
[cs300-fvp-docs]: https://developer.arm.com/documentation/100966/1118/Arm--Corstone-SSE-300-FVP
[cs310-fvp-docs]: https://developer.arm.com/documentation/100966/1118/Arm--Corstone-SSE-310-FVP
[gnu-arm-embedded-toolchain]: https://developer.arm.com/downloads/-/gnu-rm
[iotsdk-component-selection]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/blob/32b64f6f32a50764587025a2a1d5cd46f124d0e9/CMakeLists.txt#L62
[keyword]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/tree/main/examples/keyword
[keyword-cmake-endpoint-selection]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/blob/c114dd4985ee0851565f391b4c96c53d52a50e77/examples/keyword/CMakeLists.txt#L63
[keyword-main-function]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/blob/c114dd4985ee0851565f391b4c96c53d52a50e77/examples/keyword/source/main_ns.c#L99
[keyword-source-files]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/tree/main/examples/keyword/source
[linker-cs300-ts]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/blob/main/bsp/an547_ns.sct
[linker-cs310-ts]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/blob/main/bsp/an555_ns.sct
[open-iot-sdk]: https://git.gitlab.arm.com/iot/open-iot-sdk/sdk
[speech]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/tree/main/examples/speech
[total-solutions]: https://www.arm.com/markets/iot/total-solutions-iot
[total-solutions-build-instructions]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions/-/tree/main/#build
[trusted-firmware-m]: https://www.trustedfirmware.org/projects/tf-m/
[ts-pixelgraphics-benchmark]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics/-/tree/totalsolutions/examples#11-benchmark
[ts-pixelgraphics-watchpanel]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics/-/tree/totalsolutions/examples#12-watch-panel
[cmsis-pack]:https://www.open-cmsis-pack.org/
