# Advanced tips on building projects

## Recap: Building a CMake project

By the time you read this page, you have probably already built a CMake project such as one of the [Open IoT SDK examples][sdk-examples]. As a quick recap, when you configure and build inside a CMake project's root directory, the bare minimum you need to pass are `-B` the build directory, `-G` the generator (`Ninja` is recommended) and `--toolchain` the toolchain file for cross-compilation.

!!! example

    ```sh
    # Configure
    cmake -B __build -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake

    # Build
    cmake --build __build
    ```

    !!! tip

        The example above selects GNU Arm Embedded Toolchain. You can pass `--toolchain toolchains/toolchain-armclang.cmake` to use Arm Compiler for Embedded instead.

The sections below offer some further tips on building CMake projects.

!!! note

    `__build` is the assumed build directory containing the artifacts. If you have configured your build in a different directory, replace `__build` with the actual directory as needed.

## Finding build artifacts

The structure of the build directory mirrors the source.

A repository that is fetched as a dependency is built inside `__build/_deps/<repository>-build`.

!!! example

    In [Total Solutions][total-solutions], the keyword application is located in `examples/keyword` which contains a `CMakeLists.txt` script that calls `add_executable(keyword ...)`. Once you have built the project, you can find the `keyword` executable image at `__build/examples/keyword/keyword.axf`, where

    * `__build` is the root of your build directory
    * `examples/keyword` mirrors the source location where `add_executable()` is called
    * `keyword` is the name passed to `add_executable()`
    * `.axf` is the suffix added by CMake when linking the executable.

        !!! note

            While the Total Solutions project uses `.axf`, other projects commonly use `.elf`

    The [Total Solutions][total-solutions] project fetches and builds `trusted-firmware-m` as a dependency whose build artifacts can be found in `__build/_deps/trusted-firmware-m-build`.

## Building only what you need

Every executable (i.e. application) or library is a *target* in CMake, defined by `add_executable()` and `add_library()` respectively. Targets can depend on one another. For example, an executable depends on libraries via `target_link_libraries()`, and a library may depend on more libraries.

Targets come from your project as well as repositories that are fetched as build dependencies. A large project such as [Total Solutions][total-solutions] uses many targets from multiple repositories.

By default, when you build a project, all targets get built (except for those marked with `EXCLUDE_FROM_ALL` in CMake scripts). You can specify what you want to build using `--target`.

!!! example "Example: Total Solutions"

    [Total Solutions][total-solutions] includes multiple examples, and if you only want to build the keyword example you can run:

    ```sh
    # Assuming that you have previously configured the project
    cmake --build __build --target keyword
    ```

    This builds the `keyword` application as well as libraries it depends on, such as the RTOS and the machine learning libraries. It skips everything that is not needed by `keyword`, such as the `blinky` and `speech` applications.

## Cleaning your build

You can simply delete the whole `__build` directory if you don't need to keep anything from the existing build. Keep in mind that this also deletes any repositories that have been fetched as dependencies, some of which take a long time to fetch.

To clean your build, while keeping all fetched repositories as well as the current project configuration, you can run:

```sh
cmake --build __build -- clean
```

This deletes compilation artifacts such as object files, libraries and executables from the build directory, so that you can do a clean rebuild. You can clean the whole project, but there is no way to clean only a single target.

## Locating fetched repositories

A CMake project fetches dependencies such as other CMake projects using the [FetchContent][fetchcontent] module. Dependencies can fetch further dependencies. Compared to git submodules which you may be familiar with, FetchContent is much more powerful. For example, a project can be scripted to fetch only repositories it needs (rather than all repositories), apply patches to repositories, and so on.

By default, each repository is fetched into `__build/_deps/<repository>-src`.

!!! example

    `mbedtls` is cloned into `__build/_deps/mbedtls-src`.

A repository fetched by another repository is also placed directly in `__build/_deps/<repository>-src` without any nesting, meaning that you can find all repositories by listing `__build/_deps/*`.

## Skipping updates of repositories

Every time you build the project, CMake ensures each repository is checked out at the version required by your project. But sometimes you may want to skip automatic updates, for example when your network connection is limited, or when you have modified the code in one of the repositories and do not want your changes to be discarded.

Below are a couple of options you can use:

* To skip updating all repositories in `__build/_deps`:

    ```sh
    # Assuming that you have previously configured the project
    cmake -S __build -D FETCHCONTENT_UPDATES_DISCONNECTED=ON
    ```
    Repositories that have already been fetched will *not* be updated. Repositories that have not been fetched yet will be fetched as usual.

    To re-enable updates, rerun the above command but replace `ON` with `OFF`.

* To skip updating a single repository in `__build/_deps/<repository>-src`:

    ```sh
    # Assuming that you have previously configured the project
    cmake -S __build -D FETCHCONTENT_UPDATES_DISCONNECTED_<REPOSITORY>=ON
    ```
    replacing `<REPOSITORY>` with the repository's name in *ALL CAPS*.

    !!! example

        `FETCHCONTENT_UPDATES_DISCONNECTED_MBEDTLS=ON` skips updating `__build/_deps/mbedtls-src/`.

    To re-enable updates, rerun the above command but replace `ON` with `OFF`.

## Using your local repository

Although you can just modify the code fetched in `__build/_deps/<REPOSITORY>-src` and skip updates using one of the options above, sometimes it is better to use a copy of the repository that already exists on your computer.

!!! example

    If you have a local copy of `trusted-firmware-m` and have made significant changes there, and you want to try out your changes with your application, then it makes sense to use your existing `trusted-firmware-m` when building your application.

To configure a project to use a local copy of a repository, append `-D FETCHCONTENT_SOURCE_DIR_<REPOSITORY>=<local-path>`, replacing
* `<REPOSITORY>` with the repository's name in *ALL CAPS*
* `<local-path>` with your local path to the repository, which can be an absolute path or a path relative to your current working directory

CMake will use your local repository as it is without trying to update it, so there is no risk of having your changes overwritten. This also means you are responsible in making sure your repository is compatible with what you build.

!!! example

    ```sh
    # UNIX-style path
    cmake -B __build -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake -D FETCHCONTENT_SOURCE_DIR_TRUSTED_FIRMWARE_M="/home/my_username/Documents/trusted-firmware-m"
    ```

    or

    ```
    # Windows-style path
    cmake -B __build -GNinja --toolchain toolchains/toolchain-arm-none-eabi-gcc.cmake -D FETCHCONTENT_SOURCE_DIR_TRUSTED_FIRMWARE_M="C:\Users\my_username\Documents\trusted-firmware-m"
    ```

    enables your build to use your local `trusted-firmware-m` at the specified path.

    !!! tip

        The example above selects GNU Arm Embedded Toolchain. You can pass `--toolchain toolchains/toolchain-armclang.cmake` to use Arm Compiler for Embedded instead.

[sdk-examples]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples
[total-solutions]: https://gitlab.arm.com/iot/open-iot-sdk/examples/total-solutions
[fetchcontent]: https://cmake.org/cmake/help/latest/module/FetchContent.html
