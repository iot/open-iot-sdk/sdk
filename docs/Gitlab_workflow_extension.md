# GitLab Workflow extension

## Overview

This extension integrates GitLab into Visual Studio Code (VS Code). After you set up the extension, you can:

- View GitLab issues and merge requests: view issues, comments, merge requests, and changed files in the sidebar, or build a custom search to meet your needs.
- Create and review merge requests.
- Validate your GitLab CI/CD configuration locally with a command.
- Manage your pipelines: view your pipeline status and open the related merge request. With advanced pipeline actions, you can create, retry, or cancel a pipeline.
- Manage snippets. Create and insert snippets, and apply snippet patches.
- Browse a GitLab repository directly in Visual Studio Code without cloning it.
- Auto-complete GitLab CI/CD variables in your `.gitlab-ci.yml` pipeline file, and any file beginning with `.gitlab-ci `and ending with `.yml` or `.yaml`, like `.gitlab-ci.production.yml`.

## Setup

The [Gitlab workflow extension](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow) is automatically installed in VS Code development environment for Open IoT SDK.

It requires you to create a GitLab personal access token, and assign it to the extension:

1. Create a personal access token with the `api` and `read_user` scopes:
   - If you use GitLab.com, go to the [personal access tokens](https://gitlab.com/-/profile/personal_access_tokens) page.
   - If you use a self-managed GitLab instance, follow the instructions [in the GitLab documentation](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#create-a-personal-access-token).
1. Copy the token. _For security reasons, this value is never displayed again, so you must copy this value now._
1. Open VS Code, then open the command palette by pressing <kbd>F1</kbd>.
1. In the command palette, search for **GitLab: Set GitLab Personal Access Token** and press <kbd>Enter</kbd>.
1. In the **URL to GitLab instance**, paste the full URL to your GitLab instance, including the `http://` or `https://` (<https://git.gitlab.arm.com/iot/open-iot-sdk/sdk.git>). Press <kbd>Enter</kbd> to confirm.
1. Paste in your GitLab personal access token and press <kbd>Enter</kbd>. The token is not displayed, nor is it accessible to others.

The VS Code development environment project contains multiple Gitlab repositories. The user needs to manually match available repositories with Gitlab instance from URL:
1. Go to `Gitlab workflow` tab and check `Issues and merge requests` section. It should contain the names of available repositories in your workspace.
1. Right-click on a selected repository and choose `Manually assign Gitlab project`
1. Choose the right path to the Gitlab repository instance
1. Repeat steps 2 and 3 for all repositories that you want to use

The extension is ready to use.

For more information about Gitlab workflow extension refer to the [official documentation page](https://docs.gitlab.com/ee/user/project/repository/vscode.html).
