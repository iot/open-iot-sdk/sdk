# VS Code development environment for Open IoT SDK

## Overview

This guide describes how to quickly and easily start developing Open IoT SDK with Visual Studio Code [VS Code](https://code.visualstudio.com/) and [Docker](https://docs.docker.com/get-started/overview/).

VS Code is a free IDE that can be used to setup a development environment for Open IoT SDK. It also provides tools for building, running, testing and debugging example applications.

Docker provides the ability to package and run your solution in a loosely isolated environment called a container.
The isolation and security allow you to run many containers simultaneously on a given host.

Containers are lightweight and self contained, to build a container you need a text file named `Dockerfile` which describes what is installed inside the container.

You can version Dockerfile in your repository to keep the environment synced with the codebase.

This repository supports the docker/remote container workflow (*`.devcontainer`*) in VS Code, and has a container environment setup automatically.

You can read more about this workflow [Developing inside a Container](https://code.visualstudio.com/docs/remote/containers).

VS Code's [tasks](https://code.visualstudio.com/docs/editor/tasks) and [debugging](https://code.visualstudio.com/docs/editor/debugging) are used to improve the development user experience of example applications (*`.vscode`*).

## Environment setup

The use of VS Code Dev Containers significantly simplifies the setup process.
All available tools were applied to automate the process as much as possible.

These are the steps to run the Open IoT SDK environment from scratch:

1. _Windows Only_ Enable the Windows Subsystem for Linux (WSL) following
   instructions here:
   <https://docs.microsoft.com/en-us/windows/wsl/install-win10>
1. _Windows Only_ Install Ubuntu from the Windows App Store here:
   <https://www.microsoft.com/en-us/p/ubuntu-1804-lts/9n9tngvndl3q>
1. Install [Git](https://git-scm.com/) for your operating system
1. _Windows Only_ Enable git to use LF instead of CLRF by default:
   `git config --global core.autocrlf false`
1. Install [Docker Engine](https://docs.docker.com/engine/) for your operating system of choice
   from here: <https://docs.docker.com/engine/install/>
   - For _Windows_ users please refer to [Windows installation guide]( https://docs.docker.com/desktop/install/windows-install/) for Docker installation details.
   - For _Linux_ users please refer to [Linux post-installation steps for Docker Engine](https://docs.docker.com/engine/install/linux-postinstall/) to guarantee the right permission for Docker Engine.
1. Install [VS Code](https://code.visualstudio.com/) for your
   operating system of choice here: <https://code.visualstudio.com/Download>
1. Start VS Code  and  install the [Dev Containers](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
   extension for VS Code.
1. Run *`Dev Containers: Clone Repository in Container Volume...`* from the Command Palette <kbd>F1</kbd> or from quick actions Status bar item: *`Open a Remote Window`*.
1. Enter the Open IoT SDK Gitlab URL:
   <https://git.gitlab.arm.com/iot/open-iot-sdk/sdk.git>.
1. The VS Code window will reload, clone the source code (to the passed location), and start building the dev container that has all the components necessary for developing Open IoT SDK. The initial container build will take several minutes to complete and when it completes the source tree will be opened. A progress notification provides status updates.
1. After the build completes, VS Code automatically connects to the container. You can now work with the repository source code in this independent Open IoT SDK environment.
1. _Windows Only_ Update your VS Code settings as documented here:
   https://code.visualstudio.com/docs/editor/integrated-terminal#_configuration
   to use Bash on Ubuntu (on Windows) eg:
   `"terminal.integrated.shell.windows": "C:\\Windows\\System32\\bash.exe"`

!!! tip

    In case of any issues, please see [troubleshooting](#troubleshooting) section.

## Environment configuration

The VS Code development environment for Open IoT SDK can be configured by editing the `.devcontainer/devcontainer.json` file. The default settings provide a fully functional environment quickly. However, the user can adapt them to their own needs.
More settings options for VS Code dev container can be found [here](https://containers.dev/implementors/json_reference/).

**IMPORTANT**

Changes to`.devcontainer/devcontainer.json` only take effect when the container is rebuilt. This can be done by running *`Dev Containers: Rebuild Container`* from the Command Palette <kbd>F1</kbd> or from quick actions Status bar item: *`Open a Remote Window`*.

### Debugging setup

Debugging the Open IoT SDK application running on an `FVP` model requires GDB Remote
Connection Plugin for Fast Model. More details
[GDBRemoteConnection](https://developer.arm.com/documentation/100964/1116/Plug-ins-for-Fast-Models/GDBRemoteConnection).

The Fast Models FVP add-on package can be downloaded from the ARM Developer
website [FM000A Fast models page](https://developer.arm.com/downloads/view/FM000A).
After login in to the `ARM developer` platform, select the `Third Party Add-ons for Fast Models 11.16 (Linux)`.

> :bulb: Ensure that the version number of the add-on matches the version of the FVP you intend to use.

After unpacking the package in the selected location on the host machine, pass the Fast Model package to the Docker container development environment
by mounting it into the container's `/opt/FastModels_ThirdParty_IP_11-16_b16_Linux64` directory.

[Add a volume bound](https://code.visualstudio.com/remote/advancedcontainers/add-local-file-mount) to this directory by editing the `.devcontainer/devcontainer.json` file:

!!! example

    ```
    ...
    "mounts": [
       ...
       "source=/opt/FastModels_ThirdParty_IP_11-16_b16_Linux64,target=/opt/FastModels_ThirdParty_IP_11-16_b16_Linux64,type=bind,consistency=cached"
       ...
    ],
    ...
    ```

If the default mount target of the Fast Model package is changed update the `FAST_MODEL_THIRD_PARTY_DIR` container variable in the `.devcontainer/devcontainer.json` file to the same value.

To define your own Fast Model extension installation directory, edit the the `FAST_MODEL_EXTENSION_DIR` container variable in the `.devcontainer/devcontainer.json` file.

The Fast Model extension is automatically installed during the bootstrapping step when the package is provided to the container.

After installation the GDB Remote Connection Plugin should be visible in
`$FAST_MODEL_EXTENSION_DIR/FastModelsPortfolio_11.16/plugins/Linux64_GCC-9.3` directory.

## Environment usage

[VS Code tasks](https://code.visualstudio.com/docs/editor/tasks) and [VS Code debugging](https://code.visualstudio.com/docs/editor/debugging) are used to simplify developer actions in the Open IoT SDK. This built-in solution allows building, running, testing and debugging each Open IoT SDK example.

### Applying templates changes

Changes made to the Open IoT SDK example templates are automatically applied to the generated code.

The [Run on Save](https://marketplace.visualstudio.com/items?itemName=emeraldwalk.RunOnSave) VS Code extension is used to re-generate examples code each time file from the `templates` directory is saved.

The `.vscode/settings.json` file includes the extension configuration.

### Building example using VS Code task

```
Command Palette (F1)
=> Run Task...
=> Build Open IoT SDK example
=> <debug mode enable>
=> <example name>
=> (example operating system - optional)
```

The build result will be in `__build/example_name/(example_operating_system)/corstone-300` folder.

### Running example using VS Code task

Run Open IoT SDK applications on FVP platform.

```
Command Palette (F1)
=> Run Task...
=> Run Open IoT SDK example
=> <example name>
=> (example operating system - optional)
```

The Open IoT SDK example runs in a terminal session and prints its output directly to it. Use <kbd>CTRL+C</kbd> to terminate the application.

### Testing example using VS Code task

These task allows you to test Open IoT SDK example applications. The [integration test](README.md#integration-tests) is executed with the right configuration.

```
Command Palette (F1)
=> Run Task...
=> Test Open IoT SDK example
=> <example name>
=> (example operating system - optional)
```

The test is terminated automatically.

### Debugging example with VS Code

Debugging components application is a more complex action. It's required to run debug server and connect the client to it.
The [Cortex-debug](https://marketplace.visualstudio.com/items?itemName=marus25.cortex-debug) is used in this VS Code debugging solution. It requires the `ARM GCC Toolchain` and `GDB server` instance but can also works in remote server mode.

The [GDB Remote Connection Plugin](#debugging-setup) is included with running FVP and opens the external GDB server instance.

To run the Open IoT SDK example with GDB server support use this VS Code task:

```
Command Palette (F1)
=> Run Task...
=> Debug Open IoT SDK example
=> <example name>
=> (example operating system - optional)
```

The Open IoT SDK example runs in a terminal session and the GDB server starts (server details are printed in the terminal). Now you can use the preferred GDB client to connect to the node and start debugging. Use <kbd>CTRL+C</kbd> to terminate the example application.

To debug the Open IoT SDK example with VS Code run the debug session using VS Code launch configuration:

```
Run and Debug (Ctrl+Shift+D)
=> Debug Open IoT SDK (TF-M) example
=> Start Debugging (F5)
=> <example name>
=> (example operating system - optional)
```

The VS Code task that runs the application with the GDB server is automatically started in a terminal session.

As soon as a debugging session starts, the `DEBUG CONSOLE` panel is displayed and shows the debugging output.

Use debug controls to debug the current application in the "Run and Debug" section of VSCode.

Use  <kbd>CTRL+C</kbd> to terminate the example application with the GDB server.

!!! note

    The TF-M examples require a different debug configuration therefore they have a seprate launch task.
    In this case, symbols for 3 applications (bootloader, non-secure partition and secure partition) are provided for the debugging process.

Debugging the Open IoT SDK example with the VS Code launch task requires selecting the example settings twice. This is because the Vscode doesn't support passing paramters between tasks.

## Extensions

The VS Code development environment contains some pre-installed extensions.
They provide useful features to develop, enhance or maintain source code inside the environment.

For more information about available extensions visit their page:
   - [Gitlab workflow](Gitlab_workflow_extension.md)

## Limitations

The VS Code development environment for the Open IoT SDK has some limitations on the available development options.

### Supported architectures

The environment is built for the `AMD64` architecture.

### Supported toolchains

Currently only [GNU Arm Embedded version 10.3](https://developer.arm.com/downloads/-/gnu-rm) is supported in the VS Code development environment.

### Supported FVPs

Currently only [Arm Corstone SSE-300 FVP](https://developer.arm.com/Processors/Corstone-300) is supported in the VS Code development environment.

## Troubleshooting

### Cannot connect to the Docker daemon. Is the docker daemon running on this host?

This is an indication that the docker daemon is not started. You can check if it is started by running `systemctl status docker` or `sudo dockerd` in a terminal. If it is not started, run `systemctl start docker` to start the docker daemon.

If docker cannot be started and gives the error `ZONE_CONFLICT: docker0 already bound to a zone`, run the following commands, which reconfigure firewall settings and changes the interface to docker0.
```bash
$ firewall-cmd --get-active-zones
$ sudo firewall-cmd --zone=docker --change-interface=docker0
$ sudo systemctl restart firewalld
```

### DNS resolution when network changes

While the machine is open it can enter and exits several networks: home network, VPN, office network. Every time the network change the file `/etc/resolv.conf` of the host Linux is changed, it contains the address of DNS servers to use. These changes are not reflected in already running docker containers as the file is copied from the host when the container  is started.

To overcome this issue you have a number of options:

 - restart the container
 - update the file `/etc/resolv.conf` in the container with content of this file on the host.
 - set up an auto-updater (see below)

**Setting up your system to auto update the DNS when the network changes**

Create a local version of the `/etc/resolv.conf` file:

```
cp /etc/resolv.conf ~/resolv.conf
```

In your home directory create a file called `dns_change.sh` containing the following :

```bash
#! /bin/sh

diffs=`diff ~/resolv.conf /etc/resolv.conf | wc -l`

if [ $diffs -ne 0 ]; then
    cp /etc/resolv.conf ~/resolv.conf
fi
```

Create a cron job to run this script every 5 mins (or time period of your choosing). To do this run: `crontab -e`  and add the following line to your crontab file:

```
*/5 * * * *  ~/dns_change.sh >/dev/null 2>&1
```

When you run your docker container add the following mount :

```
-v ~/resolv.conf:/etc/resolv.conf
```

Now whenever your network changes, the `/etc/resolv.conf` file should be auto updated within a maximum of 5 minutes.

!!! tip

    For more Docker Engine troubleshoots visit the pages:
    - [Troubleshoot Docker Engine installation](https://docs.docker.com/engine/install/troubleshoot/)
    - [Docker demon troubleshoot](https://docs.docker.com/config/daemon/troubleshoot/)
