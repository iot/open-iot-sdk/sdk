# Copyright (c) 2022 - 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

include(FetchContent)

# Declare all components here, so that application developers can easily see
# what are available to fetch. Before an application fetches and adds the Open
# IoT SDK, it needs to set `IOTSDK_FETCH_LIST` to a list of components to fetch.

# Note: According to CMake's documentation, the patch command is called
# whenever the sources have been updated, but changes introduced by previous
# patch are not discarded. That makes the PATCH_COMMAND itself responsible
# for ensuring that it can be safely re-run after any update.
# If a component is patched, ensure it is reset first.

FetchContent_Declare(
    arm-2d
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics.git
    GIT_TAG         v2023.04
    GIT_SHALLOW     ON
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    aws-iot-device-sdk-embedded-c
    GIT_REPOSITORY https://github.com/aws/aws-iot-device-sdk-embedded-C
    GIT_TAG        02504fe687f4630070020c1a12df9453022d51db
    GIT_SHALLOW    OFF
    GIT_PROGRESS   ON
    # Note: This prevents FetchContent_MakeAvailable() from calling
    # add_subdirectory() on the fetched repository. The built-in CMake support
    # of aws-iot-device-sdk-embedded-C does not work out of the box with the
    # Open IoT SDK, so we do custom CMake support in the SDK.
    SOURCE_SUBDIR  NONE
    # Use latest coreMQTT which includes a fix for when packets are received
    # (https://github.com/FreeRTOS/coreMQTT/commit/d7a478aa9faef81d823c7b8b091a4c4b8a26216e).
    # The version fetched via aws-iot-device-sdk-embedded-c has not been updated
    # for a long time
    PATCH_COMMAND git --git-dir=libraries/standard/coreMQTT/.git --work-tree=libraries/standard/coreMQTT/ checkout 0df6f495e7c6171ca342e832815df8116668cc80
)

FetchContent_Declare(
    azure-iot-sdk-c
    GIT_REPOSITORY  https://github.com/Azure/azure-iot-sdk-c
    GIT_TAG         35914cf7fe9268a05f46fd082dc3c8568a5714bd
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
    # Note: This prevents FetchContent_MakeAvailable() from calling
    # add_subdirectory() on the fetched repository. The built-in CMake support
    # of azure-iot-sdk-c does not work out of the box with the
    # Open IoT SDK, so we do custom CMake support in the SDK.
    SOURCE_SUBDIR   NONE
    PATCH_COMMAND   git --git-dir=c-utility/.git --work-tree=c-utility reset --hard --quiet &&
                    git --git-dir=c-utility/.git --work-tree=c-utility clean --force -dx --quiet &&
                    git apply ${CMAKE_CURRENT_SOURCE_DIR}/components/azure-iot-sdk-c/azure-iot-sdk-c.patch

)

FetchContent_Declare(
    cmsis-5
    GIT_REPOSITORY  https://github.com/ARM-software/CMSIS_5.git
    GIT_TAG         a75f01746df18bb5b929dfb8dc6c9407fac3a0f3
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    cmsis-freertos
    GIT_REPOSITORY  https://github.com/ARM-software/CMSIS-FreeRTOS.git
    GIT_TAG         1a50b000a285acca8fe44433aa433fad843fa496
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    cmsis-pack-utils
    GIT_REPOSITORY  https://github.com/brondani/cmsis-pack-utils.git
    GIT_TAG         6bf0c4e4b3da920c52729d23962184feb395d88d
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
    PATCH_COMMAND   git reset --hard --quiet && git clean --force -dx --quiet && git apply --ignore-whitespace ${CMAKE_CURRENT_SOURCE_DIR}/components/cmsis-pack-utils/0001-processor-trustzone.patch
)

FetchContent_Declare(
    cmsis-sockets-api
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/iot-socket.git
    GIT_TAG         v2023.04
    GIT_SHALLOW     ON
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    coremqtt-agent
    GIT_REPOSITORY https://github.com/FreeRTOS/coreMQTT-Agent
    GIT_TAG        d3668a69bff0487f8964ad1de0fea0799ffe407c
    GIT_PROGRESS   ON
)

FetchContent_Declare(
    create-adu-import-manifest
    URL https://raw.githubusercontent.com/Azure/iot-hub-device-update/1.0.0/tools/AduCmdlets/create-adu-import-manifest.sh
    DOWNLOAD_NO_EXTRACT TRUE
)

FetchContent_Declare(
    littlefs
    GIT_REPOSITORY  https://github.com/littlefs-project/littlefs.git
    GIT_TAG         v2.8.2
    GIT_SHALLOW     ON
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    lwip
    GIT_REPOSITORY  https://github.com/lwip-tcpip/lwip.git
    GIT_TAG         STABLE-2_1_3_RELEASE
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    mbedtls
    GIT_REPOSITORY https://github.com/Mbed-TLS/mbedtls.git
    GIT_TAG        v2.28.6
    GIT_SHALLOW    ON
    GIT_PROGRESS   ON
)

FetchContent_Declare(
    mcu-driver-hal
    GIT_REPOSITORY https://git.gitlab.arm.com/iot/open-iot-sdk/libraries/mcu-driver-hal/mcu-driver-hal.git
    GIT_TAG        v2023.04
    GIT_PROGRESS   ON
)

FetchContent_Declare(
    ml-embedded-evaluation-kit
    GIT_REPOSITORY  https://review.mlplatform.org/ml/ethos-u/ml-embedded-evaluation-kit
    GIT_TAG         23.11
    GIT_SHALLOW     ON
    GIT_PROGRESS    ON
    PATCH_COMMAND   ${ML_PATCH_COMMAND}
    # Note: This prevents FetchContent_MakeAvailable() from calling
    # add_subdirectory() on the fetched repository. ml-embedded-evaluation-kit
    # needs a standalone build because it uses its own toolchain files.
    SOURCE_SUBDIR   NONE
)

FetchContent_Declare(
    netxduo
    GIT_REPOSITORY  https://github.com/azure-rtos/netxduo.git
    GIT_TAG         050f07ba7e09f7c04acf73887f40fef938ae9971
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
    PATCH_COMMAND   git reset --hard --quiet && git clean --force -dx --quiet && git apply ${CMAKE_CURRENT_SOURCE_DIR}/components/azure-rtos/netxduo/netxduo_adu.patch
)

FetchContent_Declare(
    mynewt-nimble
    GIT_REPOSITORY  https://github.com/apache/mynewt-nimble.git
    GIT_TAG         a2c79374d0e93d026d91c3f2ba879edab64802bf
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    pigweed
    GIT_REPOSITORY  https://github.com/google/pigweed.git
    GIT_TAG         7ac9fbde25331b72d7b13c13bf51b2f0714efb21
    PATCH_COMMAND   git reset --hard --quiet && git clean --force -dx --quiet && git apply ${CMAKE_CURRENT_SOURCE_DIR}/components/pigweed/pigweed.patch
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    pyedmgr
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/tools/pyedmgr.git
    GIT_TAG         ba93daffa48edd9b38d582a025150f3283196be3
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    threadx
    GIT_REPOSITORY  https://github.com/azure-rtos/threadx.git
    GIT_TAG         39f3c86c61ec478720bac9fca8f17ccedb8f052b
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    threadx-cdi-port
    GIT_REPOSITORY  https://git.gitlab.arm.com/iot/open-iot-sdk/threadx-cdi-port.git
    GIT_TAG         514c53326be9413fac41b6b3469a98b622231642
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
)

FetchContent_Declare(
    trusted-firmware-m
    GIT_REPOSITORY  https://git.trustedfirmware.org/TF-M/trusted-firmware-m.git
    GIT_TAG         TF-Mv1.8.1
    GIT_SHALLOW     ON
    GIT_PROGRESS    ON
    # Note: This prevents FetchContent_MakeAvailable() from calling
    # add_subdirectory() on the fetched repository. TF-M needs a
    # standalone build because it relies on functions defined in its
    # own toolchain files and contains paths that reference the
    # top-level project instead of its own project.
    SOURCE_SUBDIR   NONE
)

FetchContent_Declare(
    avh
    GIT_REPOSITORY  https://github.com/ARM-software/AVH.git
    GIT_TAG         ab37f6126c94fee7bbd061f77716745dfbb77592
    GIT_SHALLOW     OFF
    GIT_PROGRESS    ON
)
