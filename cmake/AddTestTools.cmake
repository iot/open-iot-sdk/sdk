# Copyright (c) 2022 - 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

# Add libraries useful for unit testing.

include(FetchContent)

if(BUILD_TESTING AND NOT CMAKE_CROSSCOMPILING)
    FetchContent_Declare(
        fff
        GIT_REPOSITORY https://github.com/meekrosoft/fff.git
        GIT_SHALLOW    OFF
        GIT_TAG        a9cb7168183991e95153ca2e76fcb88d2a7c2a77
    )
    FetchContent_GetProperties(fff)
    FetchContent_MakeAvailable(fff)

    include(GoogleTest)

    # Mark GoogleTest gtest library's include directories as SYSTEM, so that clang-tidy
    # will not warn about issues in GoogleTest that do not comply with our
    # clang-tidy configuration.
    get_target_property(gtest_include gtest INTERFACE_INCLUDE_DIRECTORIES)

    function(iotsdk_add_test target)
        target_include_directories(${target} SYSTEM
            PRIVATE
                ${gtest_include}
        )
        target_link_libraries(${target}
            PRIVATE
                gtest
        )

        gtest_discover_tests(${target})
    endfunction()
endif()
