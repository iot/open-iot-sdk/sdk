#!/bin/bash

# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

# Build/run/test Open IoT SDK example.

NAME="$(basename "$0")"
HERE="$(dirname "$0")"
ROOT="$(realpath $HERE/..)"
COMMAND=build
CLEAN=0
SCRATCH=0
DEBUG=false
BUILD_PATH="$ROOT/__build"
TOOLCHAIN=arm-none-eabi-gcc
FVP_CONFIG_FILE="$FM_AGENT_CONFIG/MPS3.conf"
FVP_BIN=FVP_Corstone_SSE-300_Ethos-U55
PLATFORM=corstone-300
GDB_PLUGIN="$FAST_MODEL_PLUGINS_PATH/GDBRemoteConnection.so"
EXAMPLE_OS=""
EXAMPLE=""
TARGET=""
TELNET_CONNECTION_PORT=""

SUPPORTED_EXAMPLE_NAMES=(`ls ${SDK_EXAMPLES_DIR}`)

function show_usage {
    cat <<EOF
Usage: $0 [options] example

Build Open IoT SDK example.

Options:
    -h,--help                       Show this help
    -c,--clean                      Clean target build
    -s,--scratch                    Build from scratch (remove build directory)
    -C,--command    <command>       Action to execute <build-run | run | test | build - default>
    -t,--target     <target_name>   Optional target name that points to the specific target in multiple-target examples.
    -d,--debug      <debug_enable>  Build in debug mode <true | false - default>
    -p,--path       <build_path>    Build path <build_path - default is root_dir/__build>
    -S,--system     <system_name>   Select operating system type for example. Option depends on example <system_name - default is empty which means examples do not support system selection>

Examples:
EOF

    for app in "${SUPPORTED_EXAMPLE_NAMES[@]}"; do
        echo "    $app"
    done

    cat <<EOF
EOF
}

function build_with_cmake {
    if [[ ! -x "$(which cmake)" ]]; then
        echo "Error: CMake is not in PATH" >&2
        exit 1
    fi

    set -e

    mkdir -p $BUILD_PATH

    if [[ $CLEAN -ne 0 ]]; then
        echo "Clean build" >&2
        if compgen -G "$BUILD_PATH/CMake*" >/dev/null; then
            cmake --build "$BUILD_PATH" --target clean
            find "$BUILD_PATH" -name 'CMakeCache.txt' -delete
        fi
    fi

    if [[ $SCRATCH -ne 0 ]]; then
        echo "Remove building directory" >&2
        rm -rf "$BUILD_PATH"
    fi

    BUILD_OPTIONS=(-DFETCHCONTENT_SOURCE_DIR_OPEN-IOT-SDK=$ROOT)
    BUILD_OPTIONS+=(-DCMAKE_PROJECT_TOP_LEVEL_INCLUDES=`realpath $ROOT/components/sanitizers/odr-violation/enable-odr-checker.cmake`)

    if "$DEBUG"; then
        BUILD_OPTIONS+=(-DCMAKE_BUILD_TYPE=Debug)
    else
        BUILD_OPTIONS+=(-DCMAKE_BUILD_TYPE=Release)
    fi

    if [[ -e $EXAMPLE_PATH/example/bootstrap.sh ]]; then
        source $EXAMPLE_PATH/example/bootstrap.sh
    fi

    cmake -G Ninja -S $EXAMPLE_PATH -B $BUILD_PATH --toolchain=$TOOLCHAIN_PATH ${BUILD_OPTIONS[@]}

    [ -e $EXAMPLE_PATH/aws-credentials ] \
        && developer-tools/utils/cloud_helper/aws.sh \
        --out-creds-file $EXAMPLE_PATH/aws-credentials/aws_clientcredential.h \
        --out-keys-file $EXAMPLE_PATH/aws-credentials/aws_clientcredential_keys.h
    [ -e $EXAMPLE_PATH/iothub-config ] \
        && developer-tools/utils/cloud_helper/az.sh \
        --template-type iot \
        --out-file $EXAMPLE_PATH/iothub-config/iothub_credentials.h
    [ -e $EXAMPLE_PATH/sample_azure_iot_credentials.h ] \
        && developer-tools/utils/cloud_helper/az.sh \
        --template-type nx \
        --out-file $EXAMPLE_PATH/sample_azure_iot_credentials.h

    cmake --build $BUILD_PATH --target $EXAMPLE_TARGET_NAME
}

function run_fvp() {
    if [[ "$EXAMPLE" == *"tf-m"* ]]; then
      EXAMPLE_TARGET_NAME="${EXAMPLE_TARGET_NAME}_merged"
    fi
    EXAMPLE_EXE_PATH="$BUILD_PATH/$EXAMPLE_TARGET_NAME.elf"

    # Check if FVP exists
    if [[ ! -x "$(which "$FVP_BIN")" ]]; then
        echo "Error: $FVP_BIN not installed." >&2
        exit 1
    fi

    # Check if executable file exists
    if ! [ -f "$EXAMPLE_EXE_PATH" ]; then
        echo "Error: $EXAMPLE_EXE_PATH does not exist." >&2
        exit 1
    fi

    # Check if FVP GDB plugin file exists
    if $DEBUG && ! [ -f "$GDB_PLUGIN" ]; then
        echo "Error: $GDB_PLUGIN does not exist. Ensure Fast Models FVP are mounted." >&2
        exit 1
    fi

    set -e

    RUN_OPTIONS=(--stat)
    RUN_OPTIONS+=(-C mps3_board.uart0.out_file="-" -C mps3_board.uart0.unbuffered_output=1)
    RUN_OPTIONS+=(--quantum=25)

    if $DEBUG; then
        RUN_OPTIONS+=(--allow-debug-plugin --plugin "$GDB_PLUGIN")
    fi

    echo "Running $EXAMPLE_EXE_PATH with options: ${RUN_OPTIONS[@]}"

    # Run the FVP
    $FVP_BIN ${RUN_OPTIONS[@]} -f $FVP_CONFIG_FILE --application $EXAMPLE_EXE_PATH
}

function run_test {
  # Check if ctest exists
  if [[ ! -x "$(which ctest)" ]]; then
      echo "Error: ctest not installed." >&2
      exit 1
  fi

  ctest --test-dir $BUILD_PATH -R $EXAMPLE_TARGET_NAME -V --no-tests=error
}


SHORT=C:,p:,d:,S:,t:,c,s,h
LONG=command:,path:,debug:,system:,target:,clean,scratch,help
OPTS=$(getopt -n build --options $SHORT --longoptions $LONG -- "$@")

eval set -- "$OPTS"

while :
do
  case "$1" in
    -h|--help )
      show_usage
      exit 0
      ;;
    -c|--clean )
      CLEAN=1
      shift
      ;;
    -s|--scratch )
      SCRATCH=1
      shift
      ;;
    -C | --command)
        COMMAND=$2
        shift 2
        ;;
    -d|--debug )
      DEBUG=$2
      shift 2
      ;;
    -p|--path )
      BUILD_PATH=$ROOT/$2
      shift 2
      ;;
    -S|--system )
      EXAMPLE_OS=$2
      shift 2
      ;;
    -t|--target )
      TARGET=$2
      shift 2
      ;;
    -*|--*)
      shift
      break
      ;;
    *)
      echo "Unexpected option: $1"
      show_usage
      exit 2
      ;;
  esac
done

if [[ $# -lt 1 ]]; then
    show_usage >&2
    exit 1
fi

EXAMPLE=$1

if [[ ! " ${SUPPORTED_EXAMPLE_NAMES[@]} " =~ " ${EXAMPLE} " ]]; then
    echo "Wrong example name"
    show_usage
    exit 2
fi

case "$COMMAND" in
    build | run | test | build-run) ;;
    *)
        echo "Wrong command definition"
        show_usage
        exit 2
        ;;
esac

EXAMPLE_DIR="$EXAMPLE"

if [ -n "$EXAMPLE_OS" ]; then
    EXAMPLE_DIR="$EXAMPLE_DIR/$EXAMPLE_OS"
    if [ ! -d "$SDK_EXAMPLES_DIR/$EXAMPLE_DIR" ]; then
        echo "Wrong example operating system. Check if example supports operating system selection"
        show_usage
        exit 2
    fi
    EXAMPLE_DIR="$EXAMPLE_DIR/$PLATFORM"
else
    EXAMPLE_DIR="$EXAMPLE_DIR/$PLATFORM"
    if [ ! -d "$SDK_EXAMPLES_DIR/$EXAMPLE_DIR" ]; then
        echo "Example operating system selection required"
        show_usage
        exit 2
    fi
fi

TOOLCHAIN_PATH="toolchains/toolchain-$TOOLCHAIN.cmake"
BUILD_PATH="$BUILD_PATH/$EXAMPLE_DIR"
EXAMPLE_PATH="$SDK_EXAMPLES_DIR/$EXAMPLE_DIR"
EXAMPLE_TARGET_NAME="iotsdk-example-$EXAMPLE"
if [ -n "$TARGET" ]; then
  EXAMPLE_TARGET_NAME="$EXAMPLE_TARGET_NAME-$TARGET"
fi

if [[ "$COMMAND" == *"build"* ]]; then
    build_with_cmake
fi

if [[ "$COMMAND" == *"run"* ]]; then
    run_fvp
fi

if [[ "$COMMAND" == *"test"* ]]; then
  run_test
fi
