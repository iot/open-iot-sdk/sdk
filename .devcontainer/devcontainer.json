{
    /*
     Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
     SPDX-License-Identifier: Apache-2.0
    */
    "name": "Open IoT SDK",
    "image": "vscode-dev-sdk-environment:local",
    "runArgs": [
        "--cap-add=SYS_PTRACE",
        "--security-opt",
        "seccomp=unconfined",
        "--network=host",
        "--privileged"
    ],

    "mounts": [
        // Provide a path to your local FastModels_ThirdParty_IP_11-16_b16_Linux64
        //"source=/path/to/your/local/FastModels_ThirdParty_IP_11-16_Linux64,target=/opt/FastModels_ThirdParty_IP_11-16_b16_Linux64,type=bind,consistency=cached"
    ],

    "containerEnv": {
        "FAST_MODEL_THIRD_PARTY_DIR": "/opt/FastModels_ThirdParty_IP_11-16_b16_Linux64",
        "FAST_MODEL_EXTENSION_DIR": "/opt",
        "SDK_CODE_GEN_DIR": "${containerWorkspaceFolder}/__codegen"
    },

    "remoteEnv": {
        "FAST_MODEL_PLUGINS_PATH":
            "${containerEnv:FAST_MODEL_EXTENSION_DIR}/FastModelsPortfolio_11.16/plugins/Linux64_GCC-9.3",
        "SDK_EXAMPLES_DIR": "${containerEnv:SDK_CODE_GEN_DIR}/examples",
        "SDK_TEST_DIR": "${containerEnv:SDK_CODE_GEN_DIR}/test",
        "GIT_PS1_SHOWDIRTYSTATE": "1",
        "GIT_PS1_SHOWSTASHSTATE": "1",
        "GIT_PS1_SHOWCOLORHINTS": "true",
        "PROMPT_COMMAND": "${localEnv:PROMPT_COMMAND}"
    },

    "initializeCommand": ".devcontainer/build.sh",
    "postCreateCommand": "bash .devcontainer/bootstrap.sh ${containerEnv:SDK_CODE_GEN_DIR}",

    "customizations": {
        "vscode": {
            // Add the IDs of extensions you want installed when the container is created in the array below.
            "extensions": [
                "ms-vscode.cpptools",
                "ms-vscode.cmake-tools",
                "twxs.cmake",
                "eamodio.gitlens",
                "knisterpeter.vscode-github",
                "mhutchie.git-graph",
                "jeff-hykin.better-cpp-syntax",
                "aaron-bond.better-comments",
                "esbenp.prettier-vscode",
                "christian-kohler.path-intellisense",
                "marus25.cortex-debug",
                "gitlab.gitlab-workflow",
                "vsciot-vscode.azure-iot-toolkit",
                "ms-python.python",
                "rioj7.command-variable",
                "emeraldwalk.RunOnSave"
            ],
            // Set *default* container specific settings.json values on container create.
            "settings": {
                "terminal.integrated.defaultProfile.linux": "bash",
                "terminal.integrated.profiles.linux": {
                    "bash": {
                        "path": "/bin/bash",
                        "args": [
                            "-l"
                        ]
                    }
                }
            }
        }
    },

    // Comment out connect as root instead. More info: https://aka.ms/vscode-remote/containers/non-root.
    "remoteUser": "vscode"
}
