#!/bin/bash

# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

HERE="$(dirname "$0")"
IMAGE_TAG="vscode-dev-sdk-environment:local"
USER_UID=$UID
USERNAME="vscode"

function show_usage {
    cat <<EOF
Usage: $0

Build VScode Open IoT SDK development environment docker image.

Options:
    -h,--help        Show this help
    -t,--tag         Image tag
    -u,--uid         User UIDa
    -n,--uname       User name
EOF
}

SHORT=t:,u:,n:,h
LONG=tag:,uid:,uname:,help
OPTS=$(getopt -n build --options $SHORT --longoptions $LONG -- "$@")

eval set -- "$OPTS"

while :
do
  case "$1" in
    -h | --help )
      show_usage
      exit 0
      ;;
    -t | --tag )
      IMAGE_TAG=$2
      shift 2
      ;;
    -u | --uid )
      USER_UID=$2
      shift 2
      ;;
    -n | --uname )
      USERNAME=$2
      shift 2
      ;;
    --)
      shift;
      break
      ;;
    *)
      echo "Unexpected option: $1"
      show_usage
      exit 2
      ;;
  esac
done

if [ $USER_UID = "0" ]; then
    USER_UID=1000
fi

docker build \
    -t $IMAGE_TAG \
    --pull \
    --platform linux/amd64 \
    --build-arg USER_UID=$USER_UID \
    --build-arg USERNAME=$USERNAME \
    --network=host \
    $HERE
