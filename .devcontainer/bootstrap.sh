#!/bin/bash

# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

function show_usage {
    cat <<EOF
Usage: $0 example_path

Bootstrap Open IoT SDK VScode environment.

Steps:
 - Git submodules synchronization
 - pre_commit hooks installation
 - Open IoT SDK examples generation in 'example_path' directory
 - required Python packages installation
 - install Fast Model extension if available

EOF
}

if [[ $# -eq 0 ]] ; then
    echo "Error: SDK examples directory not passed." >&2
    show_usage
    exit 1
fi

function sync_submodules {
    echo "Submodules synchronization" >&2
    git submodule update --init --recursive --progress
}

function pre_commit_install {
    echo "Install pre-commit" >&2
    pre-commit install
}

function generate_examples {
    echo "Generate Open IoT SDK examples and tests in $1 directory" >&2
    cmake -S templates -B tmp -DOUTPUT_DIR=$1
    rm -R tmp
}

function install_python_packages {
    pip3 install -r https://git.trustedfirmware.org/TF-M/trusted-firmware-m.git/plain/tools/requirements.txt?h=TF-Mv1.7.0
}

function install_fast_model_extension {
    if [ -d "$FAST_MODEL_THIRD_PARTY_DIR" ]; then
        echo "Install fast model extension" >&2
        cd $FAST_MODEL_THIRD_PARTY_DIR
        sudo ./setup.bin --i-accept-the-license-agreement --basepath $FAST_MODEL_EXTENSION_DIR
        cd -
    fi
}

sync_submodules
pre_commit_install
generate_examples $1
install_python_packages
install_fast_model_extension
