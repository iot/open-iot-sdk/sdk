Unless specifically indicated otherwise in a file, files are licensed under the Apache 2.0 license,
as can be found in: LICENSE-apache-2.0.txt

## Components

Folders containing external components are listed below. Each component should contain its own README file with license specified for its files. The original license text is included in those source files.

```json:table
{
    "fields": [
        "Component",
        "Path",
        "License",
        "Origin",
        "Category",
        "Version",
        "Security risk"
    ],
    "items": [
        {
            "Component": "AWS-IOT-device-sdk-embedded-C",
            "Path": "components/aws-iot-device-sdk-embedded-c",
            "License": "MIT",
            "Origin": "https://github.com/aws/aws-iot-device-sdk-embedded-C",
            "Category": "2",
            "Version": "02504fe687f4630070020c1a12df9453022d51db",
            "Security risk": "low"
        },
        {
            "Component": "Azure-SDK",
            "Path": "components/azure-iot-sdk-c",
            "License": "MIT",
            "Origin": "https://github.com/Azure/azure-iot-sdk-c",
            "Category": "2",
            "Version": "35914cf7fe9268a05f46fd082dc3c8568a5714bd",
            "Security risk": "low"
        },
        {
            "Component": "CMSIS_5",
            "Path": "components/cmsis_5",
            "License": "Apache-2.0",
            "Origin": "https://github.com/ARM-software/CMSIS_5",
            "Category": "2",
            "Version": "a75f01746df18bb5b929dfb8dc6c9407fac3a0f3",
            "Security risk": "low"
        },
        {
            "Component": "CMSIS-Pack-Utils",
            "Path": "components/cmsis-pack-utils",
            "License": "Apache-2.0",
            "Origin": "https://github.com/brondani/cmsis-pack-utils.git",
            "Category": "2",
            "Version": "6bf0c4e4b3da920c52729d23962184feb395d88d",
            "Security risk": "low"
        },
        {
            "Component": "lwip",
            "Path": "components/connectivity/lwip",
            "License": "BSD-3-Clause",
            "Origin": "https://github.com/lwip-tcpip/lwip",
            "Category": "2",
            "Version": "STABLE-2_1_3_RELEASE",
            "Security risk": "low"
        },
        {
            "Component": "fff",
            "Path": "",
            "License": "MIT",
            "Origin": "https://github.com/meekrosoft/fff",
            "Category": "2",
            "Version": "a9cb7168183991e95153ca2e76fcb88d2a7c2a77",
            "Security risk": "low"
        },
        {
            "Component": "FreeRTOS-Kernel",
            "Path": "components/cmsis-freertos",
            "License": "MIT",
            "Origin": "https://github.com/ARM-software/CMSIS-FreeRTOS",
            "Category": "2",
            "Version": "1a50b000a285acca8fe44433aa433fad843fa496",
            "Security risk": "low"
        },
        {
            "Component": "ML-embedded-evaluation-kit",
            "Path": "components/ml-embedded-evaluation-kit",
            "License": "Apache-2.0",
            "Origin": "https://review.mlplatform.org/ml/ethos-u/ml-embedded-evaluation-kit",
            "Category": "2",
            "Version": "23.11",
            "Security risk": "low"
        },
        {
            "Component": "NimBLE",
            "Path": "components/nimble",
            "License": "Apache-2.0",
            "Origin": "https://github.com/apache/mynewt-nimble",
            "Category": "2",
            "Version": "a2c79374d0e93d026d91c3f2ba879edab64802bf",
            "Security risk": "low"
        },
        {
            "Component": "Pigweed",
            "Path": "components/pigweed",
            "License": "Apache-2.0",
            "Origin": "https://github.com/google/pigweed",
            "Category": "2",
            "Version": " 2952d3767e6a4b9511c73d63d2718fb7d1e7aaf7",
            "Security risk": "low"
        },
        {
            "Component": "littlefs",
            "Path": "components/storage/littlefs",
            "License": "BSD-3-Clause",
            "Origin": "https://github.com/littlefs-project/littlefs",
            "Category": "2",
            "Version": "v2.8.2",
            "Security risk": "low"
        },
        {
            "Component": "tf-m",
            "Path": "components/trusted-firmware-m",
            "License": "BSD-3-Clause",
            "Origin": "https://git.trustedfirmware.org/TF-M/trusted-firmware-m.git",
            "Category": "2",
            "Version": "6f995f2dcfed24af274c9d23e3e0366f93110bbf",
            "Security risk": "high"
        },
        {
            "Component": "mbedtls",
            "Path": "components/mbedtls",
            "License": "Apache-2.0",
            "Origin": "https://github.com/ARMmbed/mbedtls",
            "Category": "2",
            "Version": "v2.26.0",
            "Security risk": "high"
        },
        {
            "Component": "Threadx",
            "Path": "components/azure-rtos/threadx",
            "License": "Microsoft Software License Terms for Microsoft Azure RTOS",
            "Origin": "https://github.com/azure-rtos/threadx",
            "Category": "2",
            "Version": "39f3c86c61ec478720bac9fca8f17ccedb8f052b",
            "Security risk": "low"
        },
        {
            "Component": "NetX Duo",
            "Path": "components/azure-rtos/netxduo",
            "License": "Microsoft Software License Terms for Microsoft Azure RTOS",
            "Origin": "https://github.com/azure-rtos/netxduo",
            "Category": "2",
            "Version": "050f07ba7e09f7c04acf73887f40fef938ae9971",
            "Security risk": "low"
        },
        {
            "Component": "NetX Duo Azure Device Update Addon",
            "Path": "components/azure-rtos/netxduo/netxduo_adu.patch",
            "License": "Microsoft Software License Terms for Microsoft Azure RTOS",
            "Origin": "https://github.com/azure-rtos/samples/releases/download/v6.2_rel/Azure_RTOS_6.2_B-U585I-IOT02A_IAR_Samples_2022_12_10.zip",
            "Category": "1",
            "Version": "v6.2_rel",
            "Security risk": "low"
        },
        {
            "Component": "AVH sockets",
            "Path": "components/connectivity/sockets/avh",
            "License": "Apache-2.0",
            "Origin": "https://github.com/ARM-software/AVH.git",
            "Category": "2",
            "Version": "731d50abc6075f70ef80afb18f5896c819466875",
            "Security risk": "low"
        },
        {
            "Component": "Tools for Babblesim",
            "Path": "",
            "License": "Apache-2.0",
            "Origin": "https://git.gitlab.arm.com/iot/open-iot-sdk/libraries/tools-for-babble-sim.git",
            "Category": "2",
            "Version": "",
            "Security risk": "low"
        },
        {
            "Component": "coreMQTT-Agent-Demos",
            "Path": "components/coremqtt-agent",
            "License": "MIT",
            "Origin": "https://github.com/FreeRTOS/coreMQTT-Agent-Demos",
            "Category": "1",
            "Version": "202104.00",
            "Security risk": "low"
        },
        {
            "Component": "coreMQTT-Agent",
            "Path": "components/coremqtt-agent",
            "License": "MIT",
            "Origin": "https://github.com/FreeRTOS/coreMQTT-Agent",
            "Category": "1",
            "Version": "d3668a69bff0487f8964ad1de0fea0799ffe407c",
            "Security risk": "low"
        }
    ]
}
```
