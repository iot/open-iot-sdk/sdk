/*
 * Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "Driver_USART.h"

extern ARM_DRIVER_USART Driver_USART0;

ARM_DRIVER_USART *get_example_serial()
{
    return &Driver_USART0;
}
