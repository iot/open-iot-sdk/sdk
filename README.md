# Open IoT SDK

---

__🚨Disclaimer:__

*_Arm reference solutions are Arm public example software projects that track and pull upstream components, incorporating their respective security fixes published over time._*
*_Users of these solutions are responsible for ensuring that the components they use contain all the required security fixes, if and when they deploy a product derived from Arm reference solutions._*

---

## Overview

The Open IoT SDK is a staging area for integrations of both Arm developed and selected third party software, that we use to showcase the capabilities of newly released Arm Corstone subsystems, before we look to upstream our code to the repos of the key IoT ecosystem projects, mainly FreeRTOS, Azure RTOS.

The SDK also acts as the development repository for the provision of Arm Total Solutions, an integration of hardware and standardized, secure software that provide silicon vendors and software developers with reference examples of common IoT use cases. The examples can be configured via CMake to:

* select a real-time operating system (RTOS) – AWS FreeRTOS, Azure RTOS ThreadX and Keil RTX5 are supported
* select an IP stack, with a high level IoT Socket abstraction that sits above supported IP stacks
* use machine learning models incorporated via the Arm Machine Learning Evaluation Kit
* connect to cloud services from both AWS and Azure
* perform secure firmware update using the PSA-Firmware-Update API integrated with AWS and Azure services

A full list of Total Solutions software examples based on the SDK is detailed in [Total Solutions applications](./docs/total-solutions.md) page.

## Documentation website

The documentation website of the Open IoT SDK: https://iot.sites.arm.com/open-iot-sdk/sdk

## Directory structure


The Open IoT SDK repository is structured as follows:

| Folders                | Contents                                                                           |
| ---------------------- | ---------------------------------------------------------------------------------- |
| `cmake/`               | Contains CMake modules to configure the build system, generate artefacts, etc.     |
| `components/`          | Contains directives for fetching external projects as well as glue code to integrate with said projects. |
| `docs/`                | Contains detailed information on the various procedures to work with the Open IoT SDK.|
| `release-changes/`     | Contains summaries of the changes since the last release version.|
| `templates/`           | Contains templates used to generate example and test applications used to demonstrate the integration of components to the Open IoT SDK. |
| `utils/`               | Contains re-usable code used by Open IoT SDK example and test applications.             |


## Getting started

### Prerequisites

For details about required and optional software, see [Prerequisites.md](./docs/Prerequisites.md).

### Examples

In this Open IoT SDK repository, integration examples are maintained in the form of templates in the
[`templates`](./templates) directory. This helps to avoid duplication when an example has multiple flavors based on
various Real Time Operating Systems (RTOS) and/or target platforms. Those templates are *not* ready to build directly
but need to be converted to full examples which you can built.

#### Officially generated examples

You can get ready-to-use examples generated from the latest Open IoT SDK at https://git.gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples

and follow the `README.md` inside any example you are interested in.

#### Generating examples manually

Alternatively, you can manually convert templates to examples by running the following command:

```bash
cmake -S templates -B tmp
# Note: You can delete tmp/ now as it only contains temporary CMake cache files
```

This is useful for trying out your locally modified or added example templates.

This produces examples in `__codegen/examples` and integration tests in `__codegen/tests`, each with subdirectories
`<EXAMPLE OR TEST NAME>/<RTOS>/<PLATFORM>`, because some examples and tests support multiple RTOSes and platforms. For
example, `__codegen/examples/cmsis-rtos-api/cmsis-rtx/corstone-300` is the CMSIS RTOS API example based on CMSIS RTX and
running on the Corstone-300 platform.

You can change your working directory to an example you are interested in and follow its `README.md` to build and run
the example.

Tip: `__codegen` is the default output directory, but you can specify your own directory by appending
`-D OUTPUT_DIR=<MY_OUTPUT>` to the `cmake` command above, replacing `<MY_OUTPUT>` with an absolute or relative directory
of your choice.

#### Contributing an example to the Open IoT SDK

To contribute an example or a test to the SDK, you need to add it as a template. See
[the documentation for templates](./templates/README.md) for details.

You do _not_ need to use templates for applications outside the SDK repository such as your own application.

### Visual Studio Code development environment

The Open IoT SDK supports the Docker remote container workflow in Visual Studio Code, and has a container environment setup automatically. More details about this workflow can be found [here](docs/vscode_dev_environment.md).

## Security issues reporting

If you find any security vulnerabilities, please do not report it in the GitLab issue tracker. Instead, send an email to the security team at arm-security@arm.com stating that you may have found a security vulnerability in the Open IoT SDK.

More details can be found at [Arm Developer website](https://developer.arm.com/support/arm-security-updates/report-security-vulnerabilities).

## License and contributions

The software is provided under the [Apache-2.0 license](./LICENSE-apache-2.0.txt). All contributions to software and documents are licensed by contributors under the same license model as the software/document itself (ie. inbound == outbound licensing). Open IoT SDK may reuse software already licensed under another license, provided the license is permissive in nature and compatible with Apache v2.0.

Folders containing files under different permissive license than Apache 2.0 are listed in the [LICENSE](./LICENSE.md) file.

To report a security vulnerability, please send an email to the security team at arm-security@arm.com.

For any other contribution, please see [CONTRIBUTING.md](./CONTRIBUTING.md) for more information.
