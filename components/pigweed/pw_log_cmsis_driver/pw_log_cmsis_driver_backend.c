/*
 * Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "Driver_USART.h"
#include "pw_log_cmsis_driver/config.h"
#include "pw_log_cmsis_driver/util.h"

#include <stdarg.h>
#include <stdio.h>
#include <string.h>

// Implementation notes: This first version uses the serial_putc function to
// output the trace. It is inefficient as it blocks the thread until the log
// has been transmitted over serial.
// A first improvement would be to take advantage of the async serial API or
// using a logging thread to output logs.

// Singleton used as the serial output
static ARM_DRIVER_USART *log_serial = NULL;

// Assign the output to the logger
void pw_log_cmsis_driver_init(ARM_DRIVER_USART *serial)
{
    log_serial = serial;
    _pw_log_init_lock();
}

// Output the message from PW_LOG
void _pw_log_cmsis_driver(const char *message, ...)
{
    char buffer[PW_LOG_CMSIS_DRIVER_LINE_LENGTH];

    va_list args;
    va_start(args, message);
    int result = vsnprintf(buffer, sizeof(buffer), message, args);
    va_end(args);

    if (result < 0) {
        return;
    }

    _pw_log_lock();
    log_serial->Send(buffer, strlen(buffer));
    _pw_log_unlock();
}
