/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "mbedtls/pk.h"
#include "fff.h"

DEFINE_FAKE_VOID_FUNC(mbedtls_pk_init, mbedtls_pk_context *);
DEFINE_FAKE_VOID_FUNC(mbedtls_pk_free, mbedtls_pk_context *);
