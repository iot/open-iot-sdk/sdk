/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "fff.h"
#include "mbedtls/ssl.h"

DEFINE_FAKE_VOID_FUNC(mbedtls_ssl_init, mbedtls_ssl_context *);
DEFINE_FAKE_VOID_FUNC(mbedtls_ssl_config_init, mbedtls_ssl_config *);
DEFINE_FAKE_VOID_FUNC(mbedtls_ssl_free, mbedtls_ssl_context *);
DEFINE_FAKE_VOID_FUNC(mbedtls_ssl_config_free, mbedtls_ssl_config *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_session_reset, mbedtls_ssl_context *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_config_defaults, mbedtls_ssl_config *, int, int, int);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_conf_own_cert, mbedtls_ssl_config *, mbedtls_x509_crt *, mbedtls_pk_context *);
DEFINE_FAKE_VOID_FUNC(mbedtls_ssl_conf_authmode, mbedtls_ssl_config *, int);
DEFINE_FAKE_VOID_FUNC(mbedtls_ssl_conf_ca_chain, mbedtls_ssl_config *, mbedtls_x509_crt *, mbedtls_x509_crl *);
DEFINE_FAKE_VOID_FUNC(mbedtls_ssl_conf_rng, mbedtls_ssl_config *, f_rng, void *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_setup, mbedtls_ssl_context *, const mbedtls_ssl_config *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_set_hostname, mbedtls_ssl_context *, const char *);
DEFINE_FAKE_VOID_FUNC(mbedtls_ssl_set_bio,
                      mbedtls_ssl_context *,
                      void *,
                      mbedtls_ssl_send_t *,
                      mbedtls_ssl_recv_t *,
                      mbedtls_ssl_recv_timeout_t *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_handshake, mbedtls_ssl_context *);
DEFINE_FAKE_VALUE_FUNC(uint32_t, mbedtls_ssl_get_verify_result, const mbedtls_ssl_context *);
