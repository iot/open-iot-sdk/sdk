/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "fff.h"
#include "mbedtls/pk.h"

DEFINE_FAKE_VALUE_FUNC(
    int, mbedtls_pk_parse_key, mbedtls_pk_context *, const unsigned char *, size_t, const unsigned char *, size_t);
