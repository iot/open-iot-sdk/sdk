/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "fff.h"
#include "mbedtls/ssl.h"

DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_close_notify, mbedtls_ssl_context *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_write, mbedtls_ssl_context *, const unsigned char *, size_t);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ssl_read, mbedtls_ssl_context *, const unsigned char *, size_t);
