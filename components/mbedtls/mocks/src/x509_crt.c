/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "mbedtls/x509_crt.h"
#include "fff.h"

DEFINE_FAKE_VOID_FUNC(mbedtls_x509_crt_init, mbedtls_x509_crt *);
DEFINE_FAKE_VOID_FUNC(mbedtls_x509_crt_free, mbedtls_x509_crt *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_x509_crt_parse, mbedtls_x509_crt *, const unsigned char *, size_t);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_x509_crt_verify_info, char *, size_t, const char *, uint32_t);
