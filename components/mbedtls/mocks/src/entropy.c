/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "mbedtls/entropy.h"
#include "fff.h"

DEFINE_FAKE_VOID_FUNC(mbedtls_entropy_init, mbedtls_entropy_context *);
DEFINE_FAKE_VOID_FUNC(mbedtls_entropy_free, mbedtls_entropy_context *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_entropy_func, void *, unsigned char *, size_t);
