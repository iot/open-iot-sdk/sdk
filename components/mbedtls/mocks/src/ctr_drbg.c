/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "mbedtls/ctr_drbg.h"
#include "fff.h"

DEFINE_FAKE_VOID_FUNC(mbedtls_ctr_drbg_init, mbedtls_ctr_drbg_context *);
DEFINE_FAKE_VOID_FUNC(mbedtls_ctr_drbg_free, mbedtls_ctr_drbg_context *);
DEFINE_FAKE_VALUE_FUNC(int, mbedtls_ctr_drbg_random, void *, unsigned char *, size_t);
DEFINE_FAKE_VALUE_FUNC(
    int, mbedtls_ctr_drbg_seed, mbedtls_ctr_drbg_context *, f_entropy, void *, const unsigned char *, size_t);
