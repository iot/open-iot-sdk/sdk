
/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef MBEDTLS_ENTROPY_H
#define MBEDTLS_ENTROPY_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fff.h"

typedef int mbedtls_entropy_context;

DECLARE_FAKE_VOID_FUNC(mbedtls_entropy_init, mbedtls_entropy_context *);
DECLARE_FAKE_VOID_FUNC(mbedtls_entropy_free, mbedtls_entropy_context *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_entropy_func, void *, unsigned char *, size_t);

#ifdef __cplusplus
}
#endif

#endif
