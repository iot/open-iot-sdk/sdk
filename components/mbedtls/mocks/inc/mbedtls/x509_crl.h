/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef MBEDTLS_X509_CRL_H
#define MBEDTLS_X509_CRL_H

#ifdef __cplusplus
extern "C" {
#endif

typedef int mbedtls_x509_crl;

#ifdef __cplusplus
}
#endif

#endif
