/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef MBEDTLS_CTR_DRBG_H
#define MBEDTLS_CTR_DRBG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fff.h"

typedef int mbedtls_ctr_drbg_context;
typedef int (*f_entropy)(void *, unsigned char *, size_t);

DECLARE_FAKE_VOID_FUNC(mbedtls_ctr_drbg_init, mbedtls_ctr_drbg_context *);
DECLARE_FAKE_VOID_FUNC(mbedtls_ctr_drbg_free, mbedtls_ctr_drbg_context *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ctr_drbg_random, void *, unsigned char *, size_t);
DECLARE_FAKE_VALUE_FUNC(
    int, mbedtls_ctr_drbg_seed, mbedtls_ctr_drbg_context *, f_entropy, void *, const unsigned char *, size_t);

#ifdef __cplusplus
}
#endif

#endif
