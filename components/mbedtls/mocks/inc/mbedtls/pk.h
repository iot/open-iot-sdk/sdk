/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef MBEDTLS_PK_H
#define MBEDTLS_PK_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fff.h"

typedef int mbedtls_pk_context;

DECLARE_FAKE_VOID_FUNC(mbedtls_pk_init, mbedtls_pk_context *);
DECLARE_FAKE_VOID_FUNC(mbedtls_pk_free, mbedtls_pk_context *);
DECLARE_FAKE_VALUE_FUNC(
    int, mbedtls_pk_parse_key, mbedtls_pk_context *, const unsigned char *, size_t, const unsigned char *, size_t);

#ifdef __cplusplus
}
#endif

#endif
