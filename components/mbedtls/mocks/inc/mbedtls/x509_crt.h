/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef MBEDTLS_X509_CRT_H
#define MBEDTLS_X509_CRT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fff.h"
#include "mbedtls/x509.h"
#include <stdint.h>

typedef int mbedtls_x509_crt;

DECLARE_FAKE_VOID_FUNC(mbedtls_x509_crt_init, mbedtls_x509_crt *);
DECLARE_FAKE_VOID_FUNC(mbedtls_x509_crt_free, mbedtls_x509_crt *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_x509_crt_parse, mbedtls_x509_crt *, const unsigned char *, size_t);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_x509_crt_verify_info, char *, size_t, const char *, uint32_t);

#ifdef __cplusplus
}
#endif

#endif
