/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef MBEDTLS_X509_H
#define MBEDTLS_X509_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mbedtls/pk.h"

#ifdef __cplusplus
}
#endif

#endif
