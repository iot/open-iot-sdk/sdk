/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef MBEDTLS_SSL_H
#define MBEDTLS_SSL_H

#ifdef __cplusplus
extern "C" {
#endif

#include "fff.h"
#include "mbedtls/x509_crl.h"
#include "mbedtls/x509_crt.h"
#include <stdint.h>

typedef int mbedtls_ssl_context;
typedef int mbedtls_ssl_config;
typedef int (*f_rng)(void *, unsigned char *, size_t);
typedef int mbedtls_ssl_send_t(void *ctx, const unsigned char *buf, size_t len);
typedef int mbedtls_ssl_recv_t(void *ctx, unsigned char *buf, size_t len);
typedef int mbedtls_ssl_recv_timeout_t;

#define MBEDTLS_SSL_IS_CLIENT          0
#define MBEDTLS_SSL_TRANSPORT_STREAM   0 /*!< TLS      */
#define MBEDTLS_SSL_PRESET_DEFAULT     0
#define MBEDTLS_SSL_VERIFY_REQUIRED    2
#define MBEDTLS_ERR_SSL_WANT_READ      -0x6900
#define MBEDTLS_ERR_SSL_WANT_WRITE     -0x6880
#define MBEDTLS_ERR_SSL_BAD_INPUT_DATA -0x7100
#define MBEDTLS_ERR_SSL_INTERNAL_ERROR -0x6C00

DECLARE_FAKE_VOID_FUNC(mbedtls_ssl_init, mbedtls_ssl_context *);
DECLARE_FAKE_VOID_FUNC(mbedtls_ssl_config_init, mbedtls_ssl_config *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_close_notify, mbedtls_ssl_context *);
DECLARE_FAKE_VOID_FUNC(mbedtls_ssl_free, mbedtls_ssl_context *);
DECLARE_FAKE_VOID_FUNC(mbedtls_ssl_config_free, mbedtls_ssl_config *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_session_reset, mbedtls_ssl_context *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_config_defaults, mbedtls_ssl_config *, int, int, int);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_conf_own_cert, mbedtls_ssl_config *, mbedtls_x509_crt *, mbedtls_pk_context *);
DECLARE_FAKE_VOID_FUNC(mbedtls_ssl_conf_authmode, mbedtls_ssl_config *, int);
DECLARE_FAKE_VOID_FUNC(mbedtls_ssl_conf_ca_chain, mbedtls_ssl_config *, mbedtls_x509_crt *, mbedtls_x509_crl *);
DECLARE_FAKE_VOID_FUNC(mbedtls_ssl_conf_rng, mbedtls_ssl_config *, f_rng, void *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_setup, mbedtls_ssl_context *, const mbedtls_ssl_config *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_set_hostname, mbedtls_ssl_context *, const char *);
DECLARE_FAKE_VOID_FUNC(mbedtls_ssl_set_bio,
                       mbedtls_ssl_context *,
                       void *,
                       mbedtls_ssl_send_t *,
                       mbedtls_ssl_recv_t *,
                       mbedtls_ssl_recv_timeout_t *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_handshake, mbedtls_ssl_context *);
DECLARE_FAKE_VALUE_FUNC(uint32_t, mbedtls_ssl_get_verify_result, const mbedtls_ssl_context *);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_write, mbedtls_ssl_context *, const unsigned char *, size_t);
DECLARE_FAKE_VALUE_FUNC(int, mbedtls_ssl_read, mbedtls_ssl_context *, const unsigned char *, size_t);

#ifdef __cplusplus
}
#endif

#endif
