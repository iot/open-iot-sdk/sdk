# Mbed Atomic

This is a library of cross-platform atomic memory access functions analogous to the respective C11 and C++11 standard library facilities.

* Loads have acquire semantics,
* stores have release semantics,
* atomic operations are sequentially consistent,
* atomicity is enforced both between threads and interrupt handlers.

> :bulb: The current version of Mbed Atomic is fetched from the mcu-driver-hal
> repository. However, this is likely to change in the future.

## Fetching

In your application's `CMakeLists.txt`, include `mbed-atomic` in the `IOTSDK_FETCH_LIST`
variable, alongside any other components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    mbed-atomic
)
```

> :bulb: This must be done *before* your application's `CMakeLists.txt` adds the Open IoT SDK.

## Linking

In your application's `CMakeLists.txt`, link the application executable against the library named `mbed-atomic` alongside
any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    mbed-atomic
)
```

> :bulb: Replace `my_application` with the actual name of your application.

This not only enables the linking of the `mbed-atomic` interface library, but also makes its API headers' include paths
available to your application.
