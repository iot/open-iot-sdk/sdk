# CMSIS-Pack-Utils

## Overview

[CMSIS-Pack-Utils] is an CMake module aimed at closing the gap between generic CMake based projects and CMSIS-Packs. It
generates csolution.yml and cproject.yml and orchestrates calls to csolution and cpackget to download required packs and
generate build instructions. It parses the generated cbuild.yml for getting source files, include paths and defines and
then it generates CMake library targets.

In short: it let's the user integrate CMSIS pack components to their CMake project.

## Fetching

`CMSIS-Pack-Utils` is fetched unconditionally, and is always available in the Open-IoT-SDK.

This tool is essential for providing the low level support, and is an integral part of the build system.

> :bulb: Please see the [Prerequisites][cmsis-pack-utils-deps] section of the CMSIS-Pack-Utils README file for a list of dependencies that have to be available in your system.

> :bulb: We recommend using *[CMSIS-Toolbox Version 1.5.0][cmsis-toolbox-v150]*, the version we use to test the Open-IoT-SDK.

## Examples

To see the full context of where to put each of the above code snippet in `CMakeLists.txt`, as well as how to call the
API from your application, you are advised to take a look at any of the [example applications][sdk-examples].

## Documentation

For more details of how to use CMSIS-Pack-Utils, see [CMSIS-Pack-Utils] homepage.

[CMSIS-Pack-Utils]: https://github.com/brondani/cmsis-pack-utils
[sdk-examples]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/
[cmsis-pack-utils-deps]: https://github.com/brondani/cmsis-pack-utils/blob/main/README.md#prerequisites
[cmsis-toolbox-v150]: https://github.com/Open-CMSIS-Pack/cmsis-toolbox/releases/tag/1.5.0
