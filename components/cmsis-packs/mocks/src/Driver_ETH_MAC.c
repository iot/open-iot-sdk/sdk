/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "Driver_ETH_MAC.h"
#include "fff.h"

DEFINE_FAKE_VALUE_FUNC(ARM_ETH_MAC_CAPABILITIES, ARM_ETH_MAC_GetCapabilities);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_Initialize, ARM_ETH_MAC_SignalEvent_t);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_PowerControl, ARM_POWER_STATE);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_GetMacAddress, ARM_ETH_MAC_ADDR *);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_SetAddressFilter, const ARM_ETH_MAC_ADDR *, uint32_t);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_SendFrame, const uint8_t *, uint32_t, uint32_t);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_ReadFrame, uint8_t *, uint32_t);
DEFINE_FAKE_VALUE_FUNC(uint32_t, ARM_ETH_MAC_GetRxFrameSize);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_Control, uint32_t, uint32_t);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_PHY_Read, uint8_t, uint8_t, uint16_t *);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_MAC_PHY_Write, uint8_t, uint8_t, uint16_t);

extern ARM_DRIVER_ETH_MAC Driver_ETH_MAC0;
ARM_DRIVER_ETH_MAC Driver_ETH_MAC0 = {
    .GetCapabilities = ARM_ETH_MAC_GetCapabilities,
    .Initialize = ARM_ETH_MAC_Initialize,
    .PowerControl = ARM_ETH_MAC_PowerControl,
    .GetMacAddress = ARM_ETH_MAC_GetMacAddress,
    .SetAddressFilter = ARM_ETH_MAC_SetAddressFilter,
    .SendFrame = ARM_ETH_MAC_SendFrame,
    .ReadFrame = ARM_ETH_MAC_ReadFrame,
    .GetRxFrameSize = ARM_ETH_MAC_GetRxFrameSize,
    .Control = ARM_ETH_MAC_Control,
    .PHY_Read = ARM_ETH_MAC_PHY_Read,
    .PHY_Write = ARM_ETH_MAC_PHY_Write,
};
