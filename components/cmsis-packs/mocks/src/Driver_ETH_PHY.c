/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "Driver_ETH_PHY.h"
#include "fff.h"

DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_PHY_Initialize, ARM_ETH_PHY_Read_t, ARM_ETH_PHY_Write_t);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_PHY_PowerControl, ARM_POWER_STATE);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_PHY_SetInterface, uint32_t);
DEFINE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_PHY_SetMode, uint32_t);
DEFINE_FAKE_VALUE_FUNC(ARM_ETH_LINK_STATE, ARM_ETH_PHY_GetLinkState);
DEFINE_FAKE_VALUE_FUNC(ARM_ETH_LINK_INFO, ARM_ETH_PHY_GetLinkInfo);

extern ARM_DRIVER_ETH_PHY Driver_ETH_PHY0;
ARM_DRIVER_ETH_PHY Driver_ETH_PHY0 = {
    .Initialize = ARM_ETH_PHY_Initialize,
    .PowerControl = ARM_ETH_PHY_PowerControl,
    .SetInterface = ARM_ETH_PHY_SetInterface,
    .SetMode = ARM_ETH_PHY_SetMode,
    .GetLinkState = ARM_ETH_PHY_GetLinkState,
    .GetLinkInfo = ARM_ETH_PHY_GetLinkInfo,
};
