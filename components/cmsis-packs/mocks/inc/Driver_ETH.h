/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef DRIVER_ETH_H_
#define DRIVER_ETH_H_

#include "Driver_Common.h"

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum _ARM_ETH_LINK_STATE {
    ARM_ETH_LINK_DOWN, ///< Link is down
    ARM_ETH_LINK_UP    ///< Link is up
} ARM_ETH_LINK_STATE;

typedef struct _ARM_ETH_LINK_INFO {
    uint32_t speed : 2;  ///< Link speed: 0= 10 MBit, 1= 100 MBit, 2= 1 GBit
    uint32_t duplex : 1; ///< Duplex mode: 0= Half, 1= Full
} ARM_ETH_LINK_INFO;

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* DRIVER_ETH_H_ */
