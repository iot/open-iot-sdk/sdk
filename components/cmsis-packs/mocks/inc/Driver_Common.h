/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef DRIVER_COMMON_H_
#define DRIVER_COMMON_H_

#ifdef __cplusplus
extern "C" {
#endif

#define ARM_DRIVER_OK    0  ///< Operation succeeded
#define ARM_DRIVER_ERROR -1 ///< Unspecified error

typedef enum _ARM_POWER_STATE {
    ARM_POWER_OFF = 0, ///< Power off: no operation possible
    ARM_POWER_FULL = 2 ///< Power on: full operation at maximum performance
} ARM_POWER_STATE;

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* DRIVER_COMMON_H_ */
