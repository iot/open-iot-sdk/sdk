/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef DRIVER_ETH_PHY_H_
#define DRIVER_ETH_PHY_H_

#include "Driver_ETH.h"
#include "Driver_ETH_MAC.h"
#include "fff.h"

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define _ARM_Driver_ETH_PHY_(n) Driver_ETH_PHY##n
#define ARM_Driver_ETH_PHY_(n)  _ARM_Driver_ETH_PHY_(n)

#define ARM_ETH_PHY_AUTO_NEGOTIATE (1UL << 3) ///< Auto Negotiation mode

typedef int32_t (*ARM_ETH_PHY_Read_t)(
    uint8_t phy_addr,
    uint8_t reg_addr,
    uint16_t *data); ///< Pointer to \ref ARM_ETH_MAC_PHY_Read : Read Ethernet PHY Register.
typedef int32_t (*ARM_ETH_PHY_Write_t)(
    uint8_t phy_addr,
    uint8_t reg_addr,
    uint16_t data); ///< Pointer to \ref ARM_ETH_MAC_PHY_Write : Write Ethernet PHY Register.

typedef struct _ARM_DRIVER_ETH_PHY {
    int32_t (*Initialize)(
        ARM_ETH_PHY_Read_t fn_read,
        ARM_ETH_PHY_Write_t fn_write); ///< Pointer to \ref ARM_ETH_PHY_Initialize : Initialize PHY Device.
    int32_t (*PowerControl)(
        ARM_POWER_STATE state); ///< Pointer to \ref ARM_ETH_PHY_PowerControl : Control PHY Device Power.
    int32_t (*SetInterface)(
        uint32_t interface); ///< Pointer to \ref ARM_ETH_PHY_SetInterface : Set Ethernet Media Interface.
    int32_t (*SetMode)(
        uint32_t mode); ///< Pointer to \ref ARM_ETH_PHY_SetMode : Set Ethernet PHY Device Operation mode.
    ARM_ETH_LINK_STATE(*GetLinkState)
    (void); ///< Pointer to \ref ARM_ETH_PHY_GetLinkState : Get Ethernet PHY Device Link state.
    ARM_ETH_LINK_INFO(*GetLinkInfo)
    (void); ///< Pointer to \ref ARM_ETH_PHY_GetLinkInfo : Get Ethernet PHY Device Link information.
} const ARM_DRIVER_ETH_PHY;

DECLARE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_PHY_Initialize, ARM_ETH_PHY_Read_t, ARM_ETH_PHY_Write_t);
DECLARE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_PHY_PowerControl, ARM_POWER_STATE);
DECLARE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_PHY_SetInterface, uint32_t);
DECLARE_FAKE_VALUE_FUNC(int32_t, ARM_ETH_PHY_SetMode, uint32_t);
DECLARE_FAKE_VALUE_FUNC(ARM_ETH_LINK_STATE, ARM_ETH_PHY_GetLinkState);
DECLARE_FAKE_VALUE_FUNC(ARM_ETH_LINK_INFO, ARM_ETH_PHY_GetLinkInfo);

#ifdef __cplusplus
} // extern "C"
#endif

#endif /* DRIVER_ETH_PHY_H_ */
