# pyedmgr

`pyedmgr` is a Python library for discovery, control & flashing of boards and FVPs.

* You can learn how to use `pyedmgr` to write integration tests [here](https://iot.sites.arm.com/open-iot-sdk/sdk/integration_tests.html)
* You can find the full documentation of `pyedmgr` including further tutorials and API reference [here](https://iot.sites.arm.com/open-iot-sdk/tools/pyedmgr/)
