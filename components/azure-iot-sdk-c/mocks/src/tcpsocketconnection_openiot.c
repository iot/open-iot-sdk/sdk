/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "azure_c_shared_utility/tcpsocketconnection_c.h"
#include "fff.h"

DEFINE_FAKE_VOID_FUNC(tcpsocketconnection_close, TCPSOCKETCONNECTION_HANDLE);
DEFINE_FAKE_VOID_FUNC(tcpsocketconnection_destroy, TCPSOCKETCONNECTION_HANDLE);
DEFINE_FAKE_VALUE_FUNC(int, tcpsocketconnection_receive, TCPSOCKETCONNECTION_HANDLE, char *, int);
DEFINE_FAKE_VALUE_FUNC(int, tcpsocketconnection_send, TCPSOCKETCONNECTION_HANDLE, const char *, int);
DEFINE_FAKE_VALUE_FUNC(TCPSOCKETCONNECTION_HANDLE, tcpsocketconnection_create);
DEFINE_FAKE_VALUE_FUNC(int, tcpsocketconnection_connect, TCPSOCKETCONNECTION_HANDLE, const char *, const int);
DEFINE_FAKE_VOID_FUNC(tcpsocketconnection_set_blocking, TCPSOCKETCONNECTION_HANDLE, bool, unsigned int);
