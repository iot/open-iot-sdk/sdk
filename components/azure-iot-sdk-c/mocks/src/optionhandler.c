/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "azure_c_shared_utility/optionhandler.h"
#include "fff.h"

DEFINE_FAKE_VALUE_FUNC(OPTIONHANDLER_HANDLE, OptionHandler_Create, pfCloneOption, pfDestroyOption, pfSetOption);
