/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "azure_c_shared_utility/singlylinkedlist.h"
#include "fff.h"

DEFINE_FAKE_VALUE_FUNC(LIST_ITEM_HANDLE, singlylinkedlist_add, SINGLYLINKEDLIST_HANDLE, const void *);
DEFINE_FAKE_VALUE_FUNC(LIST_ITEM_HANDLE, singlylinkedlist_get_head_item, SINGLYLINKEDLIST_HANDLE);
DEFINE_FAKE_VALUE_FUNC(int, singlylinkedlist_remove, SINGLYLINKEDLIST_HANDLE, LIST_ITEM_HANDLE);
DEFINE_FAKE_VALUE_FUNC(const void *, singlylinkedlist_item_get_value, LIST_ITEM_HANDLE);
DEFINE_FAKE_VALUE_FUNC(SINGLYLINKEDLIST_HANDLE, singlylinkedlist_create);
DEFINE_FAKE_VOID_FUNC(singlylinkedlist_destroy, SINGLYLINKEDLIST_HANDLE);
