# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

add_library(azure-iot-sdk-c-mocks
    src/iot_ntp_client.c
    src/iot_socket.c
    src/iotsdk_alloc.c
    src/ip_network_api.c
    src/optionhandler.c
    src/singlylinkedlist.c
    src/strings.c
    src/tcpsocketconnection_openiot.c
    src/tlsio_mbedtls.c
    src/xlogging.c
)

target_include_directories(azure-iot-sdk-c-mocks
    PUBLIC
        inc
)

target_link_libraries(azure-iot-sdk-c-mocks
    PUBLIC
        fff
        cmsis-rtos-api-mock
)
