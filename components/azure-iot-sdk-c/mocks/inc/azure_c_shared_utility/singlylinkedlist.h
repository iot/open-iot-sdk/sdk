/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef SINGLYLINKEDLIST_H
#define SINGLYLINKEDLIST_H

#include "fff.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef int *SINGLYLINKEDLIST_HANDLE;
typedef void *LIST_ITEM_HANDLE;

DECLARE_FAKE_VALUE_FUNC(LIST_ITEM_HANDLE, singlylinkedlist_add, SINGLYLINKEDLIST_HANDLE, const void *);
DECLARE_FAKE_VALUE_FUNC(LIST_ITEM_HANDLE, singlylinkedlist_get_head_item, SINGLYLINKEDLIST_HANDLE);
DECLARE_FAKE_VALUE_FUNC(int, singlylinkedlist_remove, SINGLYLINKEDLIST_HANDLE, LIST_ITEM_HANDLE);
DECLARE_FAKE_VALUE_FUNC(const void *, singlylinkedlist_item_get_value, LIST_ITEM_HANDLE);
DECLARE_FAKE_VALUE_FUNC(SINGLYLINKEDLIST_HANDLE, singlylinkedlist_create);
DECLARE_FAKE_VOID_FUNC(singlylinkedlist_destroy, SINGLYLINKEDLIST_HANDLE);

#ifdef __cplusplus
}
#endif

#endif // SINGLYLINKEDLIST_H
