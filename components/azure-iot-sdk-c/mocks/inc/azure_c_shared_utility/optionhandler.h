/*
 * Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef OPTIONHANDLER_H
#define OPTIONHANDLER_H

#include "fff.h"

typedef struct OPTIONHANDLER_HANDLE_DATA_TAG *OPTIONHANDLER_HANDLE;
typedef void *(*pfCloneOption)(const char *name, const void *value);
typedef void (*pfDestroyOption)(const char *name, const void *value);
typedef int (*pfSetOption)(void *handle, const char *name, const void *value);

#define OPTIONHANDLER_OK 0

DECLARE_FAKE_VALUE_FUNC(OPTIONHANDLER_HANDLE, OptionHandler_Create, pfCloneOption, pfDestroyOption, pfSetOption);

#endif // OPTIONHANDLER_H
