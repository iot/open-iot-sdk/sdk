/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef SOCKETIO_H
#define SOCKETIO_H

#include "azure_c_shared_utility/xio.h"

typedef struct SYSTEMIO_CONFIG_TAG {
    const char *hostname;
    int port;
} SOCKETIO_CONFIG;

#define XIO_RECEIVE_BUFFER_SIZE 64

DECLARE_FAKE_VALUE_FUNC(CONCRETE_IO_HANDLE, socketio_create, void *);
DECLARE_FAKE_VOID_FUNC(socketio_destroy, CONCRETE_IO_HANDLE);
DECLARE_FAKE_VALUE_FUNC(int,
                        socketio_open,
                        CONCRETE_IO_HANDLE,
                        ON_IO_OPEN_COMPLETE,
                        void *,
                        ON_BYTES_RECEIVED,
                        void *,
                        ON_IO_ERROR,
                        void *);
DECLARE_FAKE_VALUE_FUNC(int, socketio_close, CONCRETE_IO_HANDLE, ON_IO_CLOSE_COMPLETE, void *);
DECLARE_FAKE_VALUE_FUNC(int, socketio_send, CONCRETE_IO_HANDLE, const void *, size_t, ON_SEND_COMPLETE, void *);
DECLARE_FAKE_VOID_FUNC(socketio_dowork, CONCRETE_IO_HANDLE);
DECLARE_FAKE_VALUE_FUNC(int, socketio_setoption, CONCRETE_IO_HANDLE, const char *, const void *);
DECLARE_FAKE_VALUE_FUNC(const IO_INTERFACE_DESCRIPTION *, socketio_get_interface_description);

#endif // SOCKETIO_H
