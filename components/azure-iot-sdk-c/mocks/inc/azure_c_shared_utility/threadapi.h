/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef THREADAPI_H
#define THREADAPI_H

#include "fff.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void *THREAD_HANDLE;
typedef int (*THREAD_START_FUNC)(void *);
typedef enum {
    THREADAPI_OK,
    THREADAPI_INVALID_ARG,
    THREADAPI_NO_MEMORY,
    THREADAPI_ERROR,

} THREADAPI_RESULT;

DECLARE_FAKE_VALUE_FUNC(THREADAPI_RESULT, ThreadAPI_Create, THREAD_HANDLE *, THREAD_START_FUNC, void *);
DECLARE_FAKE_VALUE_FUNC(THREADAPI_RESULT, ThreadAPI_Join, THREAD_HANDLE, int *);
DECLARE_FAKE_VOID_FUNC(ThreadAPI_Exit, int);
DECLARE_FAKE_VOID_FUNC(ThreadAPI_Sleep, unsigned);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // THREADAPI_H
