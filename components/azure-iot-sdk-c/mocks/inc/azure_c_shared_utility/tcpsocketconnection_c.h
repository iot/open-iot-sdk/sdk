/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef TCPSOCKETCONNECTION_C_H
#define TCPSOCKETCONNECTION_C_H

#include "fff.h"

#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef void *TCPSOCKETCONNECTION_HANDLE;

DECLARE_FAKE_VOID_FUNC(tcpsocketconnection_close, TCPSOCKETCONNECTION_HANDLE);
DECLARE_FAKE_VOID_FUNC(tcpsocketconnection_destroy, TCPSOCKETCONNECTION_HANDLE);
DECLARE_FAKE_VALUE_FUNC(int, tcpsocketconnection_receive, TCPSOCKETCONNECTION_HANDLE, char *, int);
DECLARE_FAKE_VALUE_FUNC(int, tcpsocketconnection_send, TCPSOCKETCONNECTION_HANDLE, const char *, int);
DECLARE_FAKE_VALUE_FUNC(TCPSOCKETCONNECTION_HANDLE, tcpsocketconnection_create);
DECLARE_FAKE_VALUE_FUNC(int, tcpsocketconnection_connect, TCPSOCKETCONNECTION_HANDLE, const char *, const int);
DECLARE_FAKE_VOID_FUNC(tcpsocketconnection_set_blocking, TCPSOCKETCONNECTION_HANDLE, bool, unsigned int);
DECLARE_FAKE_VALUE_FUNC(bool, tcpsocketconnection_is_connected, TCPSOCKETCONNECTION_HANDLE);
DECLARE_FAKE_VALUE_FUNC(int, tcpsocketconnection_send_all, TCPSOCKETCONNECTION_HANDLE, const char *, int);
DECLARE_FAKE_VALUE_FUNC(int, tcpsocketconnection_receive_all, TCPSOCKETCONNECTION_HANDLE, char *, int);

#ifdef __cplusplus
}
#endif

#endif // TCPSOCKETCONNECTION_C_H
