/*
 * Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef IOTSDK_SOCKET_H_
#define IOTSDK_SOCKET_H_

#include "fff.h"

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define IOT_SOCKET_AF_INET     1    ///< IPv4
#define IOT_SOCKET_SOCK_STREAM 1    ///< Stream socket
#define IOT_SOCKET_IPPROTO_TCP 1    ///< TCP
#define IOT_SOCKET_ESOCK       (-2) ///< Invalid socket
#define IOT_SOCKET_EINVAL      (-3) ///< Invalid argument
#define IOT_SOCKET_EAGAIN      (-6) ///< Operation would block or timed out
#define IOT_SOCKET_ETIMEDOUT   (-8) ///< Operation timed out
#define IOT_SOCKET_IO_FIONBIO \
    1 ///< Non-blocking I/O (Set only, default = 0); opt_val = &nbio, opt_len = sizeof(nbio), nbio (integer):
      ///< 0=blocking, non-blocking otherwise
#define IOT_SOCKET_SO_RCVTIMEO 2 ///< Receive timeout in ms (default = 0); opt_val = &timeout, opt_len = sizeof(timeout)
#define IOT_SOCKET_SO_SNDTIMEO 3 ///< Send timeout in ms (default = 0); opt_val = &timeout, opt_len = sizeof(timeout)
#define IOT_SOCKET_SOCK_DGRAM  2 ///< Datagram socket
#define IOT_SOCKET_IPPROTO_UDP 2 ///< UDP

DECLARE_FAKE_VALUE_FUNC(int32_t, iotSocketCreate, int32_t, int32_t, int32_t);
DECLARE_FAKE_VALUE_FUNC(int32_t, iotSocketSetOpt, int32_t, int32_t, const void *, uint32_t);
DECLARE_FAKE_VALUE_FUNC(int32_t, iotSocketClose, int32_t);
DECLARE_FAKE_VALUE_FUNC(int32_t, iotSocketGetHostByName, const char *, int32_t, uint8_t *, uint32_t *);
DECLARE_FAKE_VALUE_FUNC(int32_t, iotSocketConnect, int32_t, const uint8_t *, uint32_t, uint16_t);
DECLARE_FAKE_VALUE_FUNC(int32_t, iotSocketSend, int32_t, const void *, uint32_t);
DECLARE_FAKE_VALUE_FUNC(int32_t, iotSocketRecv, int32_t, void *, uint32_t);

#ifdef __cplusplus
}
#endif

#endif /* IOTSDK_SOCKET_H_ */
