/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#include "backoff_algorithm.h"

DEFINE_FAKE_VOID_FUNC(BackoffAlgorithm_InitializeParams, BackoffAlgorithmContext_t *, uint16_t, uint16_t, uint32_t);
DEFINE_FAKE_VALUE_FUNC(
    BackoffAlgorithmStatus_t, BackoffAlgorithm_GetNextBackoff, BackoffAlgorithmContext_t *, uint32_t, uint16_t *);
