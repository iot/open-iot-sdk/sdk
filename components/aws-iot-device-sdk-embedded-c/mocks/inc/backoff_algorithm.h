/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef BACKOFF_ALGORITHM_H_
#define BACKOFF_ALGORITHM_H_

#include "fff.h"
#include <stdint.h>

typedef int BackoffAlgorithmContext_t;
typedef enum BackoffAlgorithmStatus {
    BackoffAlgorithmSuccess = 0,     /**< @brief The function successfully calculated the next back-off value. */
    BackoffAlgorithmRetriesExhausted /**< @brief The function exhausted all retry attempts. */
} BackoffAlgorithmStatus_t;

DECLARE_FAKE_VOID_FUNC(BackoffAlgorithm_InitializeParams, BackoffAlgorithmContext_t *, uint16_t, uint16_t, uint32_t);
DECLARE_FAKE_VALUE_FUNC(
    BackoffAlgorithmStatus_t, BackoffAlgorithm_GetNextBackoff, BackoffAlgorithmContext_t *, uint32_t, uint16_t *);

#endif
