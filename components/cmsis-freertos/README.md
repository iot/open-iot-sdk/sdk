# FreeRTOS Kernel and CMSIS-FreeRTOS

The FreeRTOS Kernel is a real-time operating system (RTOS) kernel for embedded devices. It is part of the Amazon FreeRTOS project which provides a collection of libraries for Internet of Things (IoT) applications.

CMSIS-FreeRTOS is an implementation of the CMSIS-RTOS v2 API running on top of the FreeRTOS Kernel. By enabling CMSIS-FreeRTOS, a single application gains the flexibility of using software components that are based on both the FreeRTOS API and the CMSIS-RTOS v2 API.

The sections below explain how to use each of the two options.

> :bulb: If you are new to CMake and the Open IoT SDK, you are advised to familiarized yourself with [How to create an application and use the Open IoT SDK][creating-application] before continuing.

## FreeRTOS Kernel

### Fetching

In your application's `CMakeLists.txt`, include `cmsis-5` and `cmsis-freertos` in the `IOTSDK_FETCH_LIST` variable, alongside any other components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    cmsis-5
    cmsis-freertos
)
```

> :bulb: The `cmsis-freertos` repository contains a full copy of FreeRTOS Kernel, so the Open IoT SDK fetches this single repository to provide both the FreeRTOS Kernel and CMSIS-FreeRTOS.

### Configuration

Download the sample FreeRTOS Kernel configuration header, [`FreeRTOSConfig.h`][freertos-config-h].

In your application's directory, create a `freertos-config` directory and move the downloaded `FreeRTOSConfig.h` into `freertos-config`. You can change or add any configuration macros in `FreeRTOSConfig.h` to cater your application's needs. Refer to the [documentation for `FreeRTOSConfig.h`][freertos-config-doc].

In your application's `CMakeLists.txt`, add `freertos-config` (which contains `FreeRTOSConfig.h`) to the include directories of the CMake target `freertos-config`:

```cmake
target_include_directories(freertos-config INTERFACE freertos-config)
```

### Linking

FreeRTOS Kernel comes with five heap implementations, `heap_1` through `heap_5`, which are detailed in the [Memory Management][freertos-memory-management] page. In the Open IoT SDK, the five heap impementations are available as libraries named `freertos-kernel-heap-1` through `freertos-kernel-heap-5`.

Link your application to the library named `freertos-kernel` and the heap implementation of your choice, alongside any other libraries you need. For example, the following enables FreeRTOS Kernel with `heap_4`:

```cmake
target_link_libraries(my_application
    ...
    freertos-kernel
    freertos-kernel-heap-4
)
```

### Examples

To see the full context of where to put each of the above code snippet in `CMakeLists.txt`, as well as how to call the API from your application, you are advised to take a look at the [examples showing how to use the FreeRTOS Kernel][sdk-examples-freertos-kernel].

### Documentation

For more details of the API usage and the available configuration options, see the [FreeRTOS Kernel documentation][freertos-kernel-docs].

## CMSIS-FreeRTOS

### Fetching

In your application's `CMakeLists.txt`, include `cmsis-5` and `cmsis-freertos` in the `IOTSDK_FETCH_LIST` variable, alongside any other components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    cmsis-5
    cmsis-freertos
)
```

### Configuration

CMSIS-FreeRTOS requires two configuration headers:

* `FreeRTOSConfig.h` for the underlying FreeRTOS Kernel
* `RTE_Components.h` for the CMSIS-RTOS v2 implementation layer

Download the sample FreeRTOS Kernel configuration header *for CMSIS-FreeRTOS*, [`FreeRTOSConfig.h`][freertos-config-h].

> :bulb: This header contains definitions specifically needed for the CMSIS-RTOS v2 port, and it is different from the one with the same name mentioned in the section [FreeRTOS Kernel](#freertos-kernel) above.

In your application's directory, create a `freertos-config` directory and move the downloaded `FreeRTOSConfig.h` into `freertos-config`. You can change or add any configuration macros in `FreeRTOSConfig.h` to cater your application's needs. Refer to the [documentation for `FreeRTOSConfig.h`][freertos-config-doc].

In your application's `CMakeLists.txt`, add `freertos-config` (which contains `FreeRTOSConfig.h`) to the include directories of the CMake target `freertos-config`:

```cmake
target_include_directories(freertos-config INTERFACE freertos-config)
```

> :bulb: The CMake target `freertos-config` will make `FreeRTOSConfig.h` available to CMSIS-FreeRTOS and its underlying FreeRTOS Kernel, both of which require this configuration header.

In your application's directory, create a `cmsis-config/RTE_Components.h` file. You can leave the content of `RTE_Components.h` empty if CMSIS-FreeRTOS is the only CMSIS component you use.

In your application's `CMakeLists.txt`, add `cmsis-config` (which contains `RTE_Components.h`) to the include directories of the CMake target `cmsis-config`:

```cmake
target_include_directories(cmsis-config INTERFACE cmsis-config)
```

> :bulb: `RTE_Components.h` is the user configuration header for all CMSIS components, and it must exist (if your application uses any CMSIS components) but can be empty.

### Linking

CMSIS-FreeRTOS's underlying FreeRTOS Kernel comes with five heap implementations, `heap_1` through `heap_5`, which are detailed in the [Memory Management][freertos-memory-management] page. In the Open IoT SDK, the five heap impementations are available as libraries named `freertos-kernel-heap-1` through `freertos-kernel-heap-5`.

Link your `cmsis-rtos-api` to the library named `freertos-cmsis-rtos` and the heap implementation of your choice, alongside any other libraries you need. For example, the following enables FreeRTOS Kernel with `heap_4`:

```cmake
target_link_libraries(cmsis-rtos-api
    PUBLIC
        freertos-cmsis-rtos
        freertos-kernel-heap-4
)
```

> Note: `cmsis-rtos-api` provides the API declaration only, whereas `freertos-cmsis-rtos` (and the heap implementation) is a concrete implementation (definition). Without this `target_link_libraries()` directive, any libraries that depend on `cmsis-rtos-api` may fail to pick up the definition and produce `undefined reference` errors.

Link your application to the library `cmsis-rtos-api`, alongside any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    cmsis-rtos-api
)
```

### Examples

To see the full context of where to put each of the above code snippet in `CMakeLists.txt`, as well as how to call the API from your application, you are advised to take a look at the [examples showing how to use CMSIS-FreeRTOS][sdk-examples-cmsis-freertos].

### Documentation

For more details of the API usage and the available configuration options, see the [CMSIS-FreeRTOS documentation][cmsis-freertos-docs].

[cmsis-freertos-docs]: https://arm-software.github.io/CMSIS-FreeRTOS/latest/
[creating-application]: https://iot.sites.arm.com/open-iot-sdk/sdk/creating-application.html
[freertos-config-h]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/blob/main/examples/freertos-api/freertos/corstone-300/freertos-config/FreeRTOSConfig.h
[freertos-config-h-cmsis]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/blob/main/examples/cmsis-rtos-api/cmsis-freertos/corstone-300/freertos-config/FreeRTOSConfig.h
[freertos-config-doc]: https://www.freertos.org/a00110.html
[freertos-kernel-docs]: https://www.freertos.org/RTOS.html
[freertos-memory-management]: https://www.freertos.org/a00111.html
[sdk-examples-cmsis-freertos]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/cmsis-rtos-api/cmsis-freertos
[sdk-examples-freertos-kernel]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/freertos-api/freertos
