# An implementation of cmsis-sockets-api using the AVH interface

This provides an implementation of the `cmsis-sockets-api` library by providing
the `avh-sockets` CMake target. This implementation is specific for use when the target device is ran in an AVH instance. It allows the device to communicate out of the AVH

The API is provided by IoT Socket interface from https://github.com/MDK-Packs/IoT_Socket,
in [iot_socket.h](https://github.com/MDK-Packs/IoT_Socket/blob/develop/include/iot_socket.h).

## License

Files are licensed under the Apache 2.0 license.
