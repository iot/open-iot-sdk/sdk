# Mbed Critical

This is a library of cross-platform utilities for disabling and enabling interrupt handling.
- The interrupts are disabled when execution reaches the start of a critical section.
- The interrupts are re-enabled when the execution reaches the end of a a critical section.

> :bulb: The current version of Mbed Critical is fetched from the mcu-driver-hal
> repository.

## Fetching

In your application's `CMakeLists.txt`, include `mbed-critical` in the
`IOTSDK_FETCH_LIST` variable, alongside any other components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    mbed-critical
)
```

> :bulb: This must be done *before* your application's `CMakeLists.txt` adds the Open IoT SDK.

## Linking

In your application's `CMakeLists.txt`, link the application executable against the library named `mbed-critical` alongside
any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    mbed-critical
)
```

> :bulb: Replace `my_application` with the actual name of your application.

This not only enables the linking of the `mbed-critical` static library, but also makes its API headers' include paths
available to your application.
