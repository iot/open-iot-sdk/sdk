# Create Azure Device Update (ADU) import manifest

Bash script for creating ADU update import manifest.

## Official Documentation

For more information on this component, please refer to [Device Update for IoT Hub - Scripts][official-readme].

## Fetching

In your application's `CMakeLists.txt`, include `create-adu-import-manifest` in
the `IOTSDK_FETCH_LIST` variable, alongside any other components you need to
fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    create-adu-import-manifest
)
```

> :bulb: This must be done *before* your application's `CMakeLists.txt` adds the Open IoT SDK.

## Executing

The script is available as
`${create-adu-import-manifest_SOURCE_DIR}/create-adu-import-manifest.sh`.

In your application's `CMakeLists.txt`, add a custom command to your executable
target. For reference, see [the Azure Device Update example based on NetX Duo and PSA Firmware Update][azure-netxduo-device-update-example-cmakelists].

[official-readme]: https://github.com/Azure/iot-hub-device-update/blob/main/tools/AduCmdlets/README.md#using-create-adu-import-manifestsh-bash
[azure-netxduo-device-update-example-cmakelists]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/blob/main/examples/azure-netxduo-device-update/corstone-300/CMakeLists.txt
