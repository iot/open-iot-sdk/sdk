# littlefs

[littlefs] is a little fail-safe filesystem designed for microcontrollers.

## Fetching

In your application's `CMakeLists.txt`, include `littlefs` in the `IOTSDK_FETCH_LIST` variable, alongside any other
components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    littlefs
)
```

> :bulb: This must be done *before* your application's `CMakeLists.txt` adds the Open IoT SDK.

## Linking

In your application's `CMakeLists.txt`, link the application executable against the library named `littlefs` alongside
any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    littlefs
)
```

> :bulb: Replace `my_application` with the actual name of your application.

This not only enables the linking of the `littlefs` static library, but also makes its API headers' include path
available to your application.

## Examples

To see the full context of the information in the sections above, as well as an example of how to use littlefs with the
Open IoT SDK, you are advised to take a look at the [Open IoT SDK example for littlefs].

In particular, in order for littlefs to access the underlying storage device, you need to implement callback functions
for read, program, erase and sync operations. The example's [main.c] implements those callbacks as

* `lfs_bd_read()`
* `lfs_bd_prog()`
* `lfs_bd_erase()`
* `lfs_bd_sync()`

and registers the callbacks into the example's `struct lfs_config` instance:

```c
static struct lfs_config lfs_cfg;
lfs_cfg.read = lfs_bd_read;
lfs_cfg.prog = lfs_bd_prog;
lfs_cfg.erase = lfs_bd_erase;
lfs_cfg.sync = lfs_bd_sync;
```

See the full example code for more details.

## Documentation

For more details of how to use the littlefs API in your application and how littlefs works, see the Markdown (`.md`)
files in the [littlefs] repository.

[littlefs]: https://github.com/littlefs-project/littlefs
[Open IoT SDK example for littlefs]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/littlefs
[main.c]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/blob/main/examples/littlefs/corstone-300/main.c
