# Azure RTOS

## Overview

[Microsoft Azure RTOS](https://github.com/azure-rtos/) is a collection of
repositories for connecting deeply embedded IoT devices.

The Open IoT SDK currently adds the following Microsoft Azure RTOS repositories:

* `threadx` - [Azure RTOS ThreadX][threadx] real-time operating system for
              deeply embedded devices.
* `netxduo` - [Azure RTOS NetX Duo][netxduo] TCP/IP network stack for IoT
              applications running on Azure RTOS ThreadX.

It is not required to use all Azure RTOS components at the same time. However,
each components have required and optional (depending on the enabled feature)
dependencies.

See the table below for dependencies of the added components:

| Component | Required dependency| Optional dependency|
|-----------|--------------------|--------------------|
| `threadx` | none               | none               |
| `netxduo` | `threadx`          | `filex` (not supported by the Open IoT SDK)|

Azure RTOS projects require knowledge of the toolchain and MCU core
architecture in order to build subdirectory paths to corresponding files that
provide ports support. The Open IoT SDK sets the CMake variables
`THREADX_TOOLCHAIN` and `THREADX_ARCH` to that effect in
[in the CMakeLists.txt for all Azure RTOS components](./CMakeLists.txt) and
requires no further application developer actions.

## ThreadX and CMSIS-RTOS v2 Adaptation Layer for ThreadX

Azure RTOS ThreadX is a real-time operating system (RTOS) highly optimized for
deeply embedded systems. Additionally, the Open IoT SDK adds the [CMSIS-RTOS v2 Adaptation Layer for Azure RTOS ThreadX][threadx-cdi-port] which implements the CMSIS-RTOS v2 API on top of Azure RTOS ThreadX.

To learn about how to enable them, see [Azure RTOS ThreadX and CMSIS-RTOS v2 Adaptation Layer for Azure RTOS ThreadX](./threadx/README.md).

## NetX Duo

Azure RTOS NetX Duo is a TCP/IP stack for embedded systems. It also comes with
an addon named Azure IoT Middleware for Azure RTOS which enables the use of
Azure IoT Hub services.

To learn more about Azure RTOS NetX Duo and how to enable it, see [its dedicated
page](./netxduo/README.md).

## License
Azure RTOS components are licensed under the Microsoft Software License Terms
for Microsoft Azure RTOS.
The [full license text is available at https://aka.ms/AzureRTOS_EULA](https://aka.ms/AzureRTOS_EULA).

CMSIS-RTOS v2 Adaptation Layer for Azure RTOS ThreadX is licensed under the Apache 2.0 license.

[netxduo]: https://github.com/azure-rtos/netxduo
[threadx]: https://github.com/azure-rtos/threadx
[threadx-cdi-port]: https://git.gitlab.arm.com/iot/open-iot-sdk/libraries/threadx-cdi-port.git
