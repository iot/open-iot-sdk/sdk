# Azure RTOS ThreadX and ThreadX CDI Port

The Open IoT SDK gives your application the options to use

* the Azure RTOS ThreadX API
* the CMSIS-RTOS v2 API, through a wrapper (named ThreadX CDI Port) on top of Azure RTOS ThreadX

A single application has the flexibility of using software components that are based on the two different RTOS APIs.

The sections below explain how to use each of the two options.

> :bulb: If you are new to CMake and the Open IoT SDK, you are advised to familiarized yourself with [How to create an application and use the Open IoT SDK][creating-application] before continuing.

## Azure RTOS ThreadX

### Fetching

In your application's `CMakeLists.txt`, include `threadx` in the `IOTSDK_FETCH_LIST` variable, alongside any other components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    threadx
)
```

### Configuration

> Note: This is optional. If you do not intend to override the default configuration, you can skip this section and jump to [Linking](#linking).

In your application's directory, create a `threadx-config/tx_user.h` file for the Azure RTOS ThreadX configuration. Define any ThreadX configuration macros you want to override in the `tx_user.h` you have created. You can refer to the [list of configuration macros][threadx-config-macros].

Then, in your application's `CMakeLists.txt`, set `TX_USER_FILE` to the full path to the `tx_user.h` file you have created:

```cmake
set(TX_USER_FILE "${CMAKE_CURRENT_LIST_DIR}/threadx-config/tx_user.h")
```

### Linking

Link your application to the library named `threadx`, alongside any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    threadx
)
```

### Examples

To see the full context of where to put each of the above code snippet in `CMakeLists.txt`, as well as how to call the API from your application, you are advised to take a look at the [examples showing how to use Azure RTOS ThreadX][sdk-examples-threadx].

### Documentation

For more details, see the [Azure RTOS ThreadX documentation][threadx-docs] website.

## ThreadX CDI Port

### Fetching

Include `threadx-cdi-port` in the `IOTSDK_FETCH_LIST` variable, alongside any other components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    threadx-cdi-port
)
```

### Configuration

In your application's directory, create a `threadx-config/tx_user.h` file for the Azure RTOS ThreadX configuration. At minimum, it *must* contain the following definitions:

```c
#define TX_PORT_SPECIFIC_PRE_SCHEDULER_INITIALIZATION return;
#define TX_THREAD_USER_EXTENSION VOID *tx_cmsis_extension;
```

You can also define additional macros for both the [ThreadX CDI Port configuration][threadx-cdi-port-docs] and the underlying [ThreadX configuration][threadx-config-macros].

In your application's `CMakeLists.txt`, set `TX_USER_FILE` to the full path to the `tx_user.h` file you have created:

```cmake
set(TX_USER_FILE "${CMAKE_CURRENT_LIST_DIR}/threadx-config/tx_user.h")
```

### Linking

Link the library `cmsis-rtos-api` to the library `threadx-cdi-port`:

```cmake
target_link_libraries(cmsis-rtos-api PUBLIC threadx-cdi-port)
```

> :bulb: `cmsis-rtos-api` provides the API declaration only, whereas `threadx-cdi-port` is a concrete implementation (definition). Without this `target_link_libraries()` directive, any libraries that depend on `cmsis-rtos-api` may fail to pick up the definition and produce `undefined reference` errors.

Finally, link your application to the library named `cmsis-rtos-api`, alongside any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    cmsis-rtos-api
)
```

### Examples

To see the full context of where to put each of the above code snippet in `CMakeLists.txt`, as well as how to call the API from your application, you are advised to take a look at the [examples showing how to use the ThreadX CDI Port][sdk-examples-threadx-cdi-port].

### Documentation

For additional configuration options, see the [documentation for ThreadX CDI Port][threadx-cdi-port-docs].

To learn how to use the CMSIS-RTOS v2 API, see the API's [documentation][cmsis-rtos-v2-docs].

## Mixed usage of the two APIs

In your application's `CMakeLists.txt`, [enabling the ThreadX CDI Port](#threadx-cdi-port) also enables the underlying Azure RTOS ThreadX API, so no additional CMake configuration is needed.

If your application and/or libraries use *both* of the RTOS APIs, the RTOS kernel *must* be initialized and started via the CMSIS-RTOS v2 API *only*, by calling `osKernelInitialize()` and `osKernelStart()`. Once the kernel has started, both the CMSIS-RTOS v2 API and the Azure RTOS ThreadX API become available for use.

[cmsis-rtos-v2-docs]: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/index.html
[creating-application]: https://iot.sites.arm.com/open-iot-sdk/sdk/creating-application.html
[sdk-examples-threadx]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/threadx-api
[sdk-examples-threadx-cdi-port]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/cmsis-rtos-api/cmsis-threadx
[threadx-cdi-port-docs]: https://gitlab.arm.com/iot/open-iot-sdk/libraries/threadx-cdi-port/-/blob/main/README.md
[threadx-docs]: https://learn.microsoft.com/en-us/azure/rtos/threadx/
[threadx-config-macros]: https://github.com/azure-rtos/threadx/blob/master/common/inc/tx_user_sample.h
