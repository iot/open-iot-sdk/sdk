# Azure RTOS NetX Duo

[Azure RTOS NetX Duo] is a real time implementation of the TCP/IP standards that works exclusively with
[Azure RTOS ThreadX] as its Real-Time Operating System.

Additionally, [Azure IoT Middleware for Azure RTOS] is available as an addon to provide platform specific binding code
between the Azure RTOS and the [Azure SDK for Embedded C] which is made available through NetX Duo. This addon enables
applications to connect to and use services of [Azure IoT Hub].

> :bulb: In its default configuration, Azure RTOS NetX Duo comes with Azure RTOS FileX enabled to support server
> protocol handlers. However, this has been turned off as Azure RTOS FileX is not provided by the Open IoT SDK.

## Fetching

In your application's `CMakeLists.txt`, include `netxduo` and its dependency, `threadx`, in the `IOTSDK_FETCH_LIST`
variable, alongside any other components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    netxduo
    threadx
)
```

> :bulb: This must be done *before* your application's `CMakeLists.txt` adds the Open IoT SDK.

## Configuration

Available configuration macros are documented in [`nx_user_sample.h`][nx_user_sample.h] from the Azure RTOS NetX Duo
repository. This `nx_user_sample.h` is also used as the default configuration file, unless you override any macros.

If you would like to override any default configuration macros:

1. In your application's directory, create a `netxduo-config/nx_user.h` file.
1. In the `nx_user.h` file you have just created, define any macros to override.
1. In you application's `CMakeLists.txt`, set `NX_USER_FILE` to the full path to your `nx_user.h` file:

    ```cmake
    set(NX_USER_FILE "${CMAKE_CURRENT_LIST_DIR}/netxduo-config/nx_user.h")
    ```

    > :bulb: `CMAKE_CURRENT_LIST_DIR` is a CMake built-in variable containing the full path to the directory containing
    > the current `CMakeLists.txt`.

    > :bulb: This must be done *before* your application's `CMakeLists.txt` adds the Open IoT SDK.

The default configuration only enables the TCP/IP stack. If you would also like to enable Azure IoT Middleware for Azure
RTOS, *in addition to the steps above*, you also need to do the following:

1. In you application's `CMakeLists.txt`, set `NXD_ENABLE_AZURE_IOT` to `ON`:

    ```cmake
    set(NXD_ENABLE_AZURE_IOT ON CACHE BOOL "")
    ```

    > :bulb: Notice that the way we set `NXD_ENABLE_AZURE_IOT` is different from `NX_USER_FILE` earlier. This is because
    > Azure RTOS NetX Duo's CMake support internally declares `NXD_ENABLE_AZURE_IOT` as a `CACHE` variable, and it also
    > specifies a very old version of CMake which requires this way of overriding `CACHE` variables.

    > :bulb: This must be done *before* your application's `CMakeLists.txt` adds the Open IoT SDK.

1. Add the following definitions to the `nx_user.h` file you have created earlier:

    ```c
    // Enable Azure IoT Middleware for Azure RTOS
    #define NX_ENABLE_EXTENDED_NOTIFY_SUPPORT
    #define NX_SECURE_ENABLE
    #define NXD_MQTT_CLOUD_ENABLE

    // Enable Azure Defender for IoT security module
    #define NX_ENABLE_IP_PACKET_FILTER
    ```

## Linking

In your application's `CMakeLists.txt`, link the application executable against the library named `netxduo` alongside
any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    netxduo
)
```

> :bulb: Replace `my_application` with the actual name of your application.

This not only enables the linking of the `netxduo` static library, but also makes its API headers' include paths
available to your application.

## Examples

To see the full context of the information in the sections above, you are advised to take a look at

* the [Open IoT SDK example for NetX Duo] showing the use of TCP/IP features
* the [Open IoT SDK example for Azure IoT Middleware] showing the connection to Azure IoT Hub

## Documentation

For more details of how to use the APIs in your application code, see the official documentations for

* [Azure RTOS NetX Duo][netxduo-doc]
* [Azure IoT Middleware for Azure RTOS][netxduo-iot-doc]

[Azure RTOS NetX Duo]: https://github.com/azure-rtos/netxduo
[Azure RTOS ThreadX]: https://github.com/azure-rtos/threadx
[Azure IoT Middleware for Azure RTOS]: https://github.com/azure-rtos/netxduo/tree/master/addons/azure_iot
[Azure SDK for Embedded C]: https://github.com/Azure/azure-sdk-for-c
[Azure IoT Hub]: https://azure.microsoft.com/en-gb/products/iot-hub
[nx_user_sample.h]: https://github.com/azure-rtos/netxduo/blob/master/common/inc/nx_user_sample.h
[Open IoT SDK example for NetX Duo]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/azure-netxduo
[Open IoT SDK example for Azure IoT Middleware]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/azure-netxduo-iot
[netxduo-doc]: https://github.com/azure-rtos/netxduo/blob/master/README.md
[netxduo-iot-doc]: https://github.com/azure-rtos/netxduo/blob/master/addons/azure_iot/README.md
