/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef IOTSDK_SANITIZERS_ASAN_H
#define IOTSDK_SANITIZERS_ASAN_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>

#include "iotsdk_sanitizers_common.h"

/** Wrapper for malloc. */
void *IOTSDK_ASAN_WRAP_MALLOC(size_t size);

/** Wrapper for free. */
void IOTSDK_ASAN_WRAP_FREE(void *p);

#ifdef __cplusplus
}
#endif

#endif /* ! IOTSDK_SANITIZERS_ASAN_H */
