# AddressSanitizer

The AddressSanitizer (asan) checks for memory bugs such as overflow and use-after-free.

* The IoT SDK implementation checks the stack and the heap.
* It uses "shadow memory" (1 byte of reserved memory per 8 bytes of allocated memory) to track application memory. A special region outside of the bounds of the protected sections should be defined in the linker script. Its should be 1/8 the combined size of the stack and heap.

## Configuration

The `iotsdk_sanitizers_config.h` file must define the following preprocessor symbols:

* `IOTSDK_ASAN_SYM_HEAP_START` and `IOTSDK_ASAN_SYM_HEAP_END` - set to the names of symbols defined in the linker script so asan can find the bounds of the heap.
* `IOTSDK_ASAN_SYM_STACK_START` and `IOTSDK_ASAN_SYM_STACK_END` - set to the names of symbols defined in the linker script so asan can find the bounds of the stack.
* `IOTSDK_ASAN_SHADOW_START` and `IOTSDK_ASAN_SHADOW_END` - set to the names of symbols defined in the linker script so asan can find the bounds of the shadow region.

The following preprocessor symbols can also be defined:

* `IOTSDK_ASAN_REDZONE_SIZE` - size in bytes of red-zone memory which is placed either side of an allocation and allows checking for out-of-bounds errors. Defaults to 8 bytes.
* `IOTSDK_ASAN_QUARANTINE_SIZE` - number of freed blocks to remember for double-free detection. Defaults to 16.

## Building & Usage

1. Provide the location of the config file to the `iotsdk-sanitizers-asan` target using `target_include_directories(iotsdk-sanitizers-asan PUBLIC ...)`.
2. Link the targets to be instrumented to the `iotsdk-sanitizers-asan` library.
3. Asan will initialise itself before main(), so all user code can be instrumented.

## Notes & Limitations

* Currently only the `arm-none-eabi-gcc` toolchain is supported.
* Stack memory bugs which occur in an RTOS thread will be reported as heap bugs because the thread stack is allocated on the heap.
