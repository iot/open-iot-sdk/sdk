/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef IOTSDK_SANITIZERS_COMMON_H
#define IOTSDK_SANITIZERS_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>

#include "iotsdk_sanitizers_config.h"

/**
 * @brief Called when sanitizer needs to report an error. User-implemented. May be replaced by a macro.
 *
 * @param fmt Format string.
 * @param ... Format arguments.
 */
#ifndef iotsdk_sanitizers_error_cb
void iotsdk_sanitizers_error_cb(const char *fmt, ...);
#endif

#ifdef __cplusplus
}
#endif

#endif /* ! IOTSDK_SANITIZERS_COMMON_H */
