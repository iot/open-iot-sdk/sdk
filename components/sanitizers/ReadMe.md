# Iot SDK Sanitizers

Sanitizers for Open IoT SDK.

## Common

### Configuration

The `iotsdk_sanitizers_config.h` should exist and its location provided to the `iotsdk-sanitizers-common` target via `target_include_directories`.

### User-supplied callbacks

`iotsdk_sanitizers_error_cb` (see [`iotsdk_sanitizers_common.h`](common/include/iotsdk_sanitizers_common.h) for signature) must be implemented as a function or macro.

## Sanitizers

* [AddressSanitizer (asan)](./asan/ReadMe.md)
* [UndefinedBehaviorSanitzer (ubsan)](./ubsan/ReadMe.md)
* [ODR Checker](./odr-violation/ReadMe.md)
