# Copyright (c) 2023 Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

if(NOT DEFINED ODR_SUPPRESSIONS)
    set(ODR_SUPPRESSIONS "100,101,102,103")
endif()

set(ODR_CHECKER "${CMAKE_CURRENT_LIST_DIR}/odr-checker.py;-s ${ODR_SUPPRESSIONS}")

set(CMAKE_ASM_LINKER_LAUNCHER   "${ODR_CHECKER}")
set(CMAKE_C_LINKER_LAUNCHER     "${ODR_CHECKER}")
set(CMAKE_CXX_LINKER_LAUNCHER   "${ODR_CHECKER}")
