# UndefinedBehaviorSanitizer

The UndefinedBehaviorSanitizer (ubsan) checks for undefined behaviour.

Checks supported:

* `alignment` - use of misaligned pointer
* `bool` - loading a bool which is neither true or false
* `bounds` - out of bounds indexing
* `enum` - loading an enum with an invalid value
* `float-cast-overflow` - cast from float to int outside of the integer's range
* `float-divide-by-zero` - floating point division by zero
* `integer-divide-by-zero` - integer division by zero
* `nonnull-attribute` - passing `NULL` to an argument marked `[[non-null]]`
* `null` - dereferencing a null pointer
* `object-size` - using bytes that are not occupied by an object
* `pointer-overflow` - overflowing a pointer through pointer arithmetic
* `return` - reaching the end of a non-void function without returning a value
* `returns-nonnull-attribute` - returning `NULL` from a `[[non-null]]` function
* `shift` - shifting by more than the width of the type, shifting by a negative value, or shifting a negative value
* `signed-integer-overflow` - overflowing a signed integer by addition, subtraction, multiplication or division/modulo
* `unreachable` - unreachable code reached
* `vla-bound` - variable-length array with a zero or negative bound

See [the Clang documentation for UBSan](https://clang.llvm.org/docs/UndefinedBehaviorSanitizer.html#id4) for further details.

## Building & Usage

Both `arm-none-eabi-gcc` and `armclang` are supported.

Specific checks can be enabled by linking the code to be instrumented to the target `iotsdk-sanitizers-ubsan-${CHECK}`. Alternatively all of the checks can be enabled by linking to the target `iotsdk-sanitizers-ubsan`. Otherwise, build as normal with `CMAKE_BUILD_TYPE=Debug`.
