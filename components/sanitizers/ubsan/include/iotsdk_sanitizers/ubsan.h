/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */
#ifndef IOTSDK_SANITIZERS_UBSAN_H
#define IOTSDK_SANITIZERS_UBSAN_H

#include "iotsdk_sanitizers_common.h"

#endif /* ! IOTSDK_SANITIZERS_UBSAN_H */
