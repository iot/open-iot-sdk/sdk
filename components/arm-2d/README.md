# Arm-2d
## Description
Arm-2D provides a library to enable low-level 2D image processing services for embedded display systems.

The Arm-2D component used within the Open IoT SDK is a fork, called [TS-PixelGraphics], of the upstream [Arm-2D] repository on GitHub. The [TS-PixelGraphics] fork adds CMake support for use with the Open IoT SDK. For more information see [TS-PixelGraphics-README].

## CMake target used within Open IoT SDK
To ensure Arm-2D is fetched for use, add `arm-2d` to the `IOTSDK_FETCH_LIST` within CMake.

Arm-2D has two CMake libraries available within the Open IoT SDK. They are:
1. `arm-2d` - provides the Arm-2D image processing services. This is mandatory to be able to use Arm-2D in your application and build graphical renders.
2. `arm-2d-helper` - provides helper services that can assist in building more complex graphics. This is an optional library that you can use.

## How To Configure
To configure Arm-2D:
1. Create an `arm-2d-config` directory for your application.
2. Inside the `arm-2d-config` directory, create an `arm_2d_cfg.h` file and define any configuration macros of your choice in this file. An example can be found [here][arm-2d-test-config-example].
3. Add `arm-2d-config` as an `INTERFACE` include directory of the CMake target `arm-2d-config`.

For information on deploying the Arm-2D library in your application, see [here][how-to-deploy-arm-2d-library].

## Official Documentation

For more information on this component, please refer to the [Arm 2D official documentation][Arm-2D-Official-Documentation].
[Official Examples][TS-PixelGraphics-Examples] are available that utilize the [TS-PixelGraphics] fork.

[Arm-2D]: https://github.com/ARM-software/Arm-2D
[Arm-2D-Official-Documentation]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics/-/tree/totalsolutions/documentation
[TS-PixelGraphics]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics
[TS-PixelGraphics-README]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics#readme-mainpage
[TS-PixelGraphics-Examples]: https://gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics/-/tree/totalsolutions/examples
[how-to-deploy-arm-2d-library]: https://gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics/-/blob/totalsolutions/documentation/how_to_deploy_the_arm_2d_library.md
[arm-2d-test-config-example]: https://git.gitlab.arm.com/iot/open-iot-sdk/examples/ts-pixelgraphics/-/blob/totalsolutions/Library/Include/template/arm_2d_cfg.h
