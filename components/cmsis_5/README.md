# CMSIS Version 5

## Overview

The Common Microcontroller Software Interface Standard (CMSIS) is a set of tools, APIs, frameworks, and workflows that help to simplify software re-use, reduce the learning curve for microcontroller developers, speed-up project build and debug, and thus reduce the time to market for new applications. (Quoted from the [documentation of CMSIS][cmsis-docs] where you can find more details.)

The Open IoT SDK adds the [CMSIS Version 5][cmsis-5] repository which contains a large collection of software components.

## Keil RTX v5

[Keil RTX v5][rtx-v5] is a real-time operating system (RTOS). It is the reference implementation of the [CMSIS-RTOS v2][cmsis-rtos-v2] API.

To learn about how to enable Keil RTX v5, see [its dedicated page](./keil_rtx_v5.md).

[cmsis-5]: https://github.com/ARM-software/CMSIS_5
[cmsis-docs]: https://arm-software.github.io/CMSIS_5/latest/General/html/index.html
[cmsis-rtos-v2]: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/index.html
[rtx-v5]: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/rtx5_impl.html
