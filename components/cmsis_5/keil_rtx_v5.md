# Keil RTX v5

[Keil RTX v5][rtx-v5] is a real-time operating system (RTOS). It is the reference implementation of the [CMSIS-RTOS v2][cmsis-rtos-v2] API.

> :bulb: If you are new to CMake and the Open IoT SDK, you are advised to familiarized yourself with [How to create an application and use the Open IoT SDK][creating-application] before continuing.

## Fetching

In your application's `CMakeLists.txt`, include `cmsis-5` in the `IOTSDK_FETCH_LIST` variable, alongside any other components you need to fetch:

```cmake
set(IOTSDK_FETCH_LIST
    ...
    cmsis-5
)
```

### Configuration

In your application's directory, create a `cmsis-config/RTE_Components.h` file for the Keil RTX v5 configuration. You can either leave the `RTE_Components.h` file empty, or define any configuration macros you want to override in the file. Refer to the [list of configuration macros][rtx-v5-config].

In your application's `CMakeLists.txt`, add `cmsis-config` (which contains `RTE_Components.h`) to the include directories of the target named `cmsis-config`:

```cmake
target_include_directories(cmsis-config INTERFACE cmsis-config)
```

> :bulb: `RTE_Components.h` is the user configuration header for all CMSIS components including Keil RTX v5, and it must exist (if your application uses any CMSIS components) but can be empty.

## Linking

Link the library `cmsis-rtos-api` to the library `cmsis-rtx`:

```cmake
target_link_libraries(cmsis-rtos-api PRIVATE cmsis-rtx)
```

> :bulb: `cmsis-rtos-api` provides the API declaration only, whereas `cmsis-rtx` is a concrete implementation (definition) of the API. Without this `target_link_libraries()` directive, libraries that depend on `cmsis-rtos-api` may fail to pick up the definition and cause `undefined reference` errors.

Finally, link your application to the library `cmsis-rtos-api`, alongside any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    cmsis-rtos-api
)
```

## Examples

To see the full context of where to put each of the above code snippet in `CMakeLists.txt`, as well as how to call the API from your application, you are advised to take a look at the [examples showing how to use Keil RTX v5][sdk-examples-rtx].

## Documentation

For more details of the API usage and the available configuration options, see [CMSIS-RTOS v2 Documentation][cmsis-rtos-v2] and [RTX v5 Implementation][rtx-v5].

[cmsis-5]: https://github.com/ARM-software/CMSIS_5
[cmsis-docs]: https://arm-software.github.io/CMSIS_5/latest/General/html/index.html
[cmsis-rtos-v2]: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/index.html
[creating-application]: https://iot.sites.arm.com/open-iot-sdk/sdk/creating-application.html
[rtx-v5]: https://www.keil.com/pack/doc/CMSIS/RTOS2/html/rtx5_impl.html
[rtx-v5-config]: https://github.com/ARM-software/CMSIS_5/blob/develop/CMSIS/RTOS2/RTX/Config/RTX_Config.h
[sdk-examples-rtx]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/cmsis-rtos-api/cmsis-rtx
