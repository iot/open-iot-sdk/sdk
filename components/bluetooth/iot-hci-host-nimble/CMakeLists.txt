# Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

project(iot-hci-host-nimble LANGUAGES C)

add_library(iot-hci-host-nimble)

target_sources(iot-hci-host-nimble
    PRIVATE
        src/iot_hci_host_nimble.c
)

target_link_libraries(iot-hci-host-nimble
    PRIVATE
        iot-hci
        cmsis-rtos-api
        mynewt-nimble
)
