# Implementation of HCI interface from Host side for Nimble

This component provides the driver for nimble to talk over HCI to the controller.
It uses the Open IoT SDK HCI interface.

## How to use

To use it you must provide it as a dependency to your port
of Nimble host.

If you're using the Open IoT SDK provided port cmsis-nimble-port you can do this by adding this to your cmake:

```cmake
target_link_libraries(cmsis-nimble-port
    PUBLIC
        iot-hci-host-nimble
)
```

You must also provide the controller driver as the dependency since the host needs that to register its callback.

If you want to use the provided loopback controller you would need to add this to your application cmake:

```cmake
target_link_libraries(iot-hci-host-nimble
    PUBLIC
        iot-hci-controller-ip-socket
)
```

## License

Files are licensed under the Apache 2.0 license.
