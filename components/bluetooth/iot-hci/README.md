# IoT HCI interface

This is the interface to connect the Bluetooth Host and Controller in Open IoT SDK.

The controller must implement the `iotHciTransportInit(iot_hci_host_rx_callback callback)` function.
The callback parameter is called to transfer data received from the controller to the host stack.

It must also implement the `iotHciSendToController` that the host will use to send data to the controller.
Host will always send complete HCI packets.

The host must implement the callback it passes to the controller using the `iotHciTransportInit` function.
Host must handle the controller sending fragmented packets.

Please read the header file comments for implementation requirement details.

## License

Files are licensed under the Apache 2.0 license.
