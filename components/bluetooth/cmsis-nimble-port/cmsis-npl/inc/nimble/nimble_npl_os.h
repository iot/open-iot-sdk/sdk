/* Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef NIMBLE_NPL_NPL_OS_H_
#define NIMBLE_NPL_NPL_OS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "cmsis_os2.h"

#define BLE_NPL_OS_ALIGNMENT 4
#define BLE_NPL_TIME_FOREVER osWaitForever

typedef uint32_t ble_npl_time_t;
typedef int32_t ble_npl_stime_t;

struct ble_npl_event {
    bool queued;
    ble_npl_event_fn *callback;
    void *context;
};

struct ble_npl_eventq {
    osMessageQueueId_t id;
};

struct ble_npl_callout {
    osTimerId_t id;
    struct ble_npl_eventq *event_queue;
    struct ble_npl_event event;
    uint32_t period;
    uint32_t expiry;
};

struct ble_npl_mutex {
    osMutexId_t id;
};

struct ble_npl_sem {
    osSemaphoreId_t id;
};

#ifdef __cplusplus
}
#endif

#endif /* NIMBLE_NPL_NPL_OS_H_ */
