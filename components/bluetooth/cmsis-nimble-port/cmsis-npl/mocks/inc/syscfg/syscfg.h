/* Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef NIMBLE_NPL_SYSCFG_H_
#define NIMBLE_NPL_SYSCFG_H_

#define MYNEWT_VAL(_name) MYNEWT_VAL_##_name

#ifndef MYNEWT_VAL_BLE_PERIODIC_ADV
#define MYNEWT_VAL_BLE_PERIODIC_ADV (0)
#endif

#endif /* NIMBLE_NPL_SYSCFG_H_ */
