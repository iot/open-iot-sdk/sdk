# Copyright (c) 2021-2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

if(BUILD_TESTING AND NOT CMAKE_CROSSCOMPILING)
    add_subdirectory(tests)
endif()

FetchContent_GetProperties(mynewt-nimble)


# Interface to be populated by the user to set a non default config.
# See cmsis-nimble-port's README for more info on how to do it.
add_library(nimble-user-config INTERFACE)


add_library(cmsis-npl)

target_sources(cmsis-npl
    PRIVATE
        src/nimble_npl_os.c
)

target_include_directories(cmsis-npl
    PRIVATE
        ${mynewt-nimble_SOURCE_DIR}/nimble/include
        ${mynewt-nimble_SOURCE_DIR}/nimble/host/include
    PUBLIC
        # compilation relies on order of include paths, we override only select files
        inc
        ${mynewt-nimble_SOURCE_DIR}/porting/nimble/include
)

target_link_libraries(cmsis-npl
    PUBLIC
        cmsis-rtos-api
        nimble-user-config
)
