# NimBLE CMSIS port

This component provides an implementation of the Nimble Porting Layer for CMSIS RTOS and a set of functions to
initialise and run the stack.

## How to use

The port works as a host only BLE stack and requires a HCI driver implementation to work. To use the provided
implementation that implements the IoT HCI interface add this to your application cmake:

```cmake
target_link_libraries(cmsis-nimble-port
    PUBLIC
        iot-hci-host-nimble
)
```

This port can handle initialisation and running of the stack by exposing two helper functions.

To use them inside your application you must call the `ble_init()` function first.
After this you may add any services you require.
Once that is done you must run `ble_start()` to start servicing the NimBLE event queue.
This function returns normally and handles the events on its own thread.

The stack size of the thread handling the event queue can be adjusted by setting the value of
`BLE_NIMBLE_EVENT_THREAD_STACK_SIZE`:

```cmake
target_compile_definitions(cmsis-nimble-port
    PRIVATE
        BLE_NIMBLE_EVENT_THREAD_STACK_SIZE=2048
)
```

Otherwise you may initialise and run Nimble using their API calls. Refer to their own documentation on how to do this.

### Configuring NimBLE

NimBLE offers many configuration options you may use to disable some functionality and shrink the resulting binary for instance.
You can find the default settings in [syscfg.h](cmsis-npl/inc/syscfg/syscfg.h).

To override the default settings, create a file (eg: `nimble-user-config.h`), add the `#defines` of the options you want to override, and add the following to your application's `CMakeLists.txt`:

```cmake
target_sources(nimble-user-config INTERFACE "nimble-user-config.h")
target_compile_definitions(nimble-user-config INTERFACE NIMBLE_USER_CONFIG_PATH="nimble-user-config.h")
target_include_directories(nimble-user-config INTERFACE <path_to_dir_containing_nimble-user-config.h>)
```

#### Finding the options description

There are no easy-to-access the documentation for the NimBLE configuration options.
You will have to look into NimBLE's source files (not just the sdk port) for the correct syscfg.yml.

For instance, if you want to know more about `#define MYNEWT_VAL_BLE_PERIODIC_ADV (0)`, discard `MYNEWT_VAL_` and grep for `BLE_PERIODIC_ADV:` (the semicolon will help narrow the search)
```sh
grep -ri "BLE_PERIODIC_ADV:" --exclude-dir=apps --exclude-dir=repos
```
You'll obtain a few files where this config is set and some of those might have a description.

#### Re-generating the default config

Updating NimBLE to a newer version may generate build errors with the configuration file. In that case, you may need to re-generate the default config. To do so, you'll need to build NimBLE with the `newt` tool to build one of the NimBLE's example. Then overwrite the sdk's `syscfg.h` with the generated `syscfg.h`.
Refer to [Apache's doc](https://mynewt.apache.org/latest/get_started/index.html) on how to use the `newt` tool.

### Logging module

By default NimBLE will log through printfs. If you want to use another logging channel, you can link another logging backend against cmsis-npl by adding the following to your application's CMakeLists:
```cmake
target_link_libraries(cmsis-npl
    PUBLIC
        <logging_interface_lib>
)
```

We provide a Pigweed implementation under the library name `nimble-pw-logging`.

#### Creating a new logging module

Follow these steps to create your own logging library:
 - Create a logging target
 - Add your header
 - Set the define `NIMBLE_LOG_BACKEND_PATH` to point to your header
 - Link your target to `cmsis-npl`

You may also refer to [modlog_pw_impl.h](pw_logging/inc/modlog_pw_impl.h) and to [the pw_logging CMakeLists](pw_logging/CMakeLists.txt) as examples.

## License

Files are licensed under the Apache 2.0 license.
