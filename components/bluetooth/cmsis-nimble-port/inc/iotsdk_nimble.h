/* Copyright (c) 2022-2023, Arm Limited and Contributors. All rights reserved.
 * SPDX-License-Identifier: Apache-2.0
 */

#ifndef IOTSDK_NIMBLE_H_
#define IOTSDK_NIMBLE_H_

#ifdef __cplusplus
extern "C" {
#endif

/** Initialises the nimble host stack and transport.
 *  Must be called prior to adding any services.
 */
void ble_init(void);

/** Starts servicing the internal nimble queue.
 *  Call this after calling ble_init and adding your own services needed by your application.
 *  This returns immediately and events are handled on a separate thread.
 */
void ble_start(void);

#ifdef __cplusplus
}
#endif

#endif /* IOTSDK_NIMBLE_H_ */
