# NimBLE

Apache [NimBLE](https://github.com/apache/mynewt-nimble) is an open-source Bluetooth 5.1 BLE stack (both Host & Controller).

This component offers the host only version of NimBLE.

## How to use

To use this component the application must specify the port which needs to at minimum provide provide the
Nimble Porting Layer implementation.

To use the Open IoT SDK provided port for CMSIS rtos you must add this to your application cmake file:

```cmake
target_link_libraries(mynewt-nimble
    PUBLIC
        cmsis-nimble-port
)
```

Please refer to the README of cmsis-nimble-port for other requirements.

Nimble depends on tinycrypt. A version of tinycrypt is available as mynewt-nimble-tinycrypt but has been
left out of dependencies to allow you to supply your own version of tinycrypt. If you want to use the
supplied version, add this to your application cmake:

```cmake
target_link_libraries(mynewt-nimble
    PRIVATE
        mynewt-nimble-tinycrypt
)
```

Otherwise specify there the alternate library you wish to use to supply tinycrypt.

## License

NimBLE is licenced under the Apache 2 licence. The full licence is available [here](https://github.com/apache/mynewt-nimble/blob/master/LICENSE)
