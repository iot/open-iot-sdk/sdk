# Bluetooth

Open IoT SDK offers a BLE host stack solution using [Apache NimBLE](https://github.com/apache/mynewt-nimble).

Elements of the Open IoT SDK BLE solution:

- cmsis-nimble-port - Port of Nimble for Open IoT SDK using CMSIS rtos, providing the Host stack only
- iot-hci - HCI interface
- iot-hci-controller-ip-socket - controller driver implementing the HCI interface from the controller side
                                using ip loopback socket to connect to a controller running on a system
                                hosting the FVP running the host part
- iot-hci-host-nimble - host driver implementing the HCI interface from the host side
- mynewt-nimble - BLE host stack provided by Apache Nimble

Please read the READMEs in individual components to learn how to use them.

Note that to use the provided Nimble implementation you must connect it to a controller as it only
provides the host part of BLE.

## License

Files are licensed under the Apache 2.0 license.
