# Implementation of HCI interface from Controller side using an IP socket

This component provides a driver that will attempt to connect to a controller over an IP connection.
It will open a socket that it will use to connect to the specified port and IP address where it expects
to find a relay that will carry HCI traffic to and from the controller.

By default the driver will attempt to connect to the host running the model using the loopback address and port 51051.

## How to use

The component itself relies on a working socket implementation, which can be provided by `avh-sockets`.
This must be specified in your application cmake:

```cmake
target_link_libraries(iot-hci-controller-ip-socket
    PRIVATE
        avh-sockets
)
```

### Running the controller

For the component to work in a meaningful way you must open and listen on a socket from the side of the host system
that will provide the controller. A relay must route the traffic from this socket to the system's HCI interface.

You may change the target port and IP address octets by defining compile time definitions for the library:

```cmake
target_compile_definitions(iot-hci-controller-ip-socket
    PRIVATE
        BLE_CONTROLLER_TRANSPORT_SOCKET_IP_PORT=51051
        BLE_CONTROLLER_TRANSPORT_SOCKET_IP_OCTET_1=127
        BLE_CONTROLLER_TRANSPORT_SOCKET_IP_OCTET_2=0
        BLE_CONTROLLER_TRANSPORT_SOCKET_IP_OCTET_3=0
        BLE_CONTROLLER_TRANSPORT_SOCKET_IP_OCTET_4=1
)
```

The stack size of the thread handling the data coming in from the controller can be adjusted by setting the value of
`BLE_CONTROLLER_HCI_RX_THREAD_STACK_SIZE`:

```cmake
target_compile_definitions(iot-hci-controller-ip-socket
    PRIVATE
        BLE_CONTROLLER_HCI_RX_THREAD_STACK_SIZE=1024
)
```

## License

Files are licensed under the Apache 2.0 license.
