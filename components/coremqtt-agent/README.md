# coreMQTT-Agent


## Overview

The coreMQTT Agent library is an API that adds thread safety to the [coreMQTT library][coreMQTT-library]. It allows for a simpler use in multi-threaded environments. For more information, see [here][coreMQTT-Agent].

> :bulb: The coreMQTT Agent library brings the coreMQTT library as a dependent component using Git submodules.

## Fetching
In your application's `CMakeLists.txt`, include `coremqtt-agent` in the `IOTSDK_FETCH_LIST` variable, alongside any other components you need to fetch.
```cmake
set(IOTSDK_FETCH_LIST
    ...
    coremqtt-agent
)
```

> :bulb: This must be done *before* your application's `CMakeLists.txt` adds the Open IoT SDK.

## Configuration

Available configuration macros are documented in [`core_mqtt_agent_config_defaults.h`][core_mqtt_agent_config_defaults.h] from the coreMQTT Agent library repository.
This `core_mqtt_agent_config_defaults.h` is also used as the default configuration file, unless you override any macros.

If you would like to override any default configuration macros:

1. In your application's directory, create a `core_mqtt_config.h` file.
1. In the `core_mqtt_config.h` file you have just created, define any macros to override.
1. In your applications's `CMakeLists.txt`, include the newly created configuration file's parent directory path to the include directory for the `coremqtt-agent-config` CMake target:
```cmake
target_include_directories(coremqtt-agent-config INTERFACE /path/to/config/header/parent/directory)
```
1. In your applications's `CMakeLists.txt`, include the `MQTT_AGENT_DO_NOT_USE_CUSTOM_CONFIG` compile definition to the the `coremqtt-agent-config` CMake target:
```cmake
target_compile_definitions(coremqtt-agent-config INTERFACE MQTT_AGENT_DO_NOT_USE_CUSTOM_CONFIG)
```


## Linking

In your application's `CMakeLists.txt`, link the application executable against the library named `coremqtt-agent` alongside any other libraries you need:

```cmake
target_link_libraries(my_application
    ...
    coremqtt-agent
)
```
> :bulb: The `coremqtt-agent-config` CMake target library exists to allow user applications to customize the configuration of the coreMQTT Agent library.

## Interface for CMSIS-RTOS

The Open IoT SDK AWS provides the coreMQTT Agent library with CMSIS-RTOS C API as the RTOS interface. The CMSIS-RTOS interface is provided via the `coremqtt-agent-interface-cmsis-rtos` CMake target library.
`coremqtt-agent-interface-cmsis-rtos` is a dependency of `coremqtt-agent`.

> :bulb: If you wish to use an alternative RTOS API, you will need to implement your own version of the interface to use with the coreMQTT Agent library and add it as a dependency of the `coremqtt-agent` CMake library.

## Subscription Manager for coreMQTT-Agent
One of the dependencies for building the coreMQTT Agent library is a subscription manager. A global subscription list is defined when initializing the MQTT Agent and can have elements added and removed using the functions provided by the Subscription Manager as part of the callback when the command completes.


> :bulb: The Subscription Manager is provided as part of the `coremqtt-agent` library within CMake, there is no need to link a separate library.

## Examples

+To see the full context of the information in the sections above, you are advised to take a look at the [Open IoT SDK example for AWS Client][aws-client-example] which shows the use of coreMQTT Agent.

## Documentation

For more information, please refer to the [coreMQTT Agent library official Documentation][coreMQTT-Agent-docs].

[coreMQTT-library]: https://github.com/FreeRTOS/coreMQTT
[coreMQTT-Agent]: https://github.com/FreeRTOS/coreMQTT-Agent
[coreMQTT-Agent-config-defaults]: https://github.com/FreeRTOS/coreMQTT-Agent/blob/main/source/include/core_mqtt_agent_config_defaults.h
[coreMQTT-Agent-docs]: https://freertos.org/Documentation/api-ref/coreMQTT-Agent/docs/doxygen/output/html/index.html
[aws-client-example]: https://gitlab.arm.com/iot/open-iot-sdk/examples/sdk-examples/-/tree/main/examples/aws-client
[core_mqtt_agent_config_defaults.h]: https://github.com/FreeRTOS/coreMQTT-Agent/blob/main/source/include/core_mqtt_agent_config_defaults.h
