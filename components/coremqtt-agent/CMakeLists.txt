# Copyright (c) 2023, Arm Limited and Contributors. All rights reserved.
# SPDX-License-Identifier: Apache-2.0

include(FetchContent)

if("coremqtt-agent" IN_LIST IOTSDK_FETCH_LIST)
    FetchContent_MakeAvailable(coremqtt-agent)
endif()

if (coremqtt-agent_POPULATED)
    include(${coremqtt-agent_SOURCE_DIR}/mqttAgentFilePaths.cmake)

    add_library(coremqtt-agent
        ${MQTT_AGENT_SOURCES}
    )

    target_include_directories(coremqtt-agent
        PUBLIC
            ${MQTT_AGENT_INCLUDE_PUBLIC_DIRS}
    )

    add_library(coremqtt-agent-config INTERFACE)

    target_link_libraries(coremqtt-agent
        PUBLIC
            coreMQTT
            cmsis-rtos-api
            coremqtt-agent-config
            coremqtt-agent-interface-cmsis-rtos
            coremqtt-agent-subscription-manager
    )

    add_subdirectory(interface/rtos/cmsis-rtos)

    add_subdirectory(subscription_manager)
endif()
